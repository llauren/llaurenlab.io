---
title: "50"
description: "Tack! Kiitos! Thank you!"
menu: more
weight: 50
date: 2020-05-06T23:00:23+02:00
---

Nu har jag fyllt femtio och haft mottagning på distans. Det måste ha varit
femtio personer närvarande och vi höll på i sju timmar! Tack till alla som
medverkade. Ni är underbara, allihop!

Nyt olen täyttänyt viisikymmentä ja pitänyt etävastaanoton. Paikalla oli
viitisenkymmentä ihmistä ja hommaa jatkettiin seitsemän tuntia! Kiitos kaikille
osallistuneille. Olette ihania, kaikki!

And so i've turned fifty and had my remote reception. There must have been fifty
people on line, and the set went on for seven hours! Thank you everyone. You are
indeed wonderful!

 - Robin, 50 ツ
