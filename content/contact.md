---
title: "Contact"
languageCode: sv
#menu: main
weight: 90
---

You can hail me on any of the social networks you'll find me, but be aware that i'm
not the most active on any of them and i might be a bit untrusting if i receive
an unexpected unsolicited message. Be creative. For best results, send
PGP-signed mail to me. That should catch my attention. My key IDs are `ED759C09`
and `FADDF7CD`.

All else failing, use the form below:

% form-contact action="https://formspree.io/qxpsm93my7@liamekaens.com" %

Sent using [Formspree](https://formspree.io). Please use other means of
transmission if you are paranoid.
