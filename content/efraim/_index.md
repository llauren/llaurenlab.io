---
title: "Kalla mej Efraim"
featured_image: '/sheriffen.jpeg'
#description: "Kalla mej Efraim"
languageCode: sv
menu: main
weight: 50
omit_header_text: true
---
**Vad ont har jag gjort?**

Robin Laurén, aspirerande amatörskådespelare. Spelar vanligen byxlösa män och
inkompetenta men välvilliga fäder. Sjunger gärna och inte helt dåligt. Dansar
som en träbent panda.

 * Bill Lesley, [Fallåker 2022-2023](/tags/paniken) &middot;
 * Gepetto/Sergei, [Finns 2022](/tags/pinocchio) &middot;
 * Evald Erikson Blom, [Fallåker 2020-2021](/tags/stulen-kärlek)  &middot;
 * Sheriffen av Nottingham, [Fallåker 2019-2020](/tags/robin-hood)  &middot;
 * Efraim Långstrump, [Finns 2019](/tags/pippi-långstrump)  

(och _naturligtvis_ författare+regi: Leonardo, eller Vem Måla Lisa? 1997 och regi: Jeppe på berget 1989 :)

<!--
Statist i Sorjonen: Muraalimurhat   &middot;
En replik i Under svavelgula himlen.
-->
