---
title: "Artikel Till EBUF"
type: post
date: 2019-08-18T14:38:56+03:00
draft: false
tags:
 - Finns
 - Pippi Långstrump

---

Historierna går lite i sär när man försöker reda ut hur jag hamnade på audition
till Finns Sommarteater, men där var jag i varje fall. Omgiven av barn och
ungdomar. Vi kan inte ha varit mer än en handfull vuxna och flera hörde till
produktionsteamet. En del av ungarna kände varandra från förr. Jag kände mej
lite malplacerad, men beslöt att det skulle inte hinra mej. Här skulle det köras
fullt ut. Dansa kan jag inte. Skådespelat har jag aldrig gjort. Men försöka kan
jag, så det gjorde jag. Fullt ut.

Det var snö ute när Finns-ensemblen samlades för sin första repetition. Jonna,
vår producent (och Prusiluska) höll ett kort anförande att ett halvår går i ett
huj och snart ska vi skiljas och det är slut. Inget kändes mer alvägset. Det är
ju snö ute! Det är en evighet tills vi ska spela. Vi känner knappt varandra ...
och hur ska jag lära mej alla dessa repliker, alla koreografier och alla de här
underliga sångerna? Har jag tagit vatten över huvudet?

Repetition är kunskapens moder, heter det, så vi repeterade. 54 gånger, enligt
vårt repschema. Först en gång i veckan. Och småningom oftare. Sakta började det
bli en grupp av enseblen. Folk började stoja och fånas med varandra. Trivas,
liksom. Och med tid och övning började stegen kännas bekantare, låtarna
normalare och replikerna naturligare. Ibland behövde man inte ens titta i
manuset för att veta vad man skulle säja till nästa.

Och det var repetitioner och övningar och talko och yrsel och fnissande och
vansinne, kostymer och mikrofoner och nervositet och borttapade grejer, repliker
och, ibland, skådepelare. Men vi lärde oss, hur osannolikt det ändå kändes
ibland. Och så var det premiär. Koncentrationen var på topp. Och allt bara
fixade sej! Publiken trivdes, skrattade på de rätta ställena och applåderade vid
scenbytena! Applåderade in oss efter bugningarna! Vilken härlig känsla! Och så
slut man kände sej när vi stod och tackade och hejade på publiken på väg hem
igen!

Vi spelade igenom hela juni, flera gånger i veckan. Dagarna vi inte spelade
kändes helt tomma! Två lediga dagar efter varandra kändes som en evighet. Att
ses igen efter två dagar var som att komma tillbaka till familjen.

Koncentrationen byttes så småningom till rutin och säkerhet. Vi kunde börja
finessa med karaktärerna och replikerna. Regnade det, och det gjorde det inte
ofta, lade vi i ett extra kol och spelade allt vildare.

Och så var det mitt i allt sista föreställningen. Ja, två sista föreställningar,
eftersom vi hade två Pippin. Och så var det slut. Precis som Jonna hade sagt,
evigheter sedan. Men tänk vad rätt hon hade, ändå. Det var skönt med semester,
men väldigt, väldigt tyst.

Skulle jag göra om det? Absolut. Visst tog det tid och energi, men min sommar på
scen är nåt som gav mej så mycket glädje. Det kändes fint att jag genom mitt
grejande på scen kunde skapa glädje och underhållning för de som kommit för att
se oss. För vår publik. Ge och få.

Det var en bra och jämställd atmosfär i gruppen. Trots att vi hade skådespelare
i åldern under skolåldern till nästan 50, var alla lika värda. Vi hade alla
repliker, sånger och koreografi att lära oss, kostym, mickar, rekvisita att
hålla reda på, varandra att hålla reda på. Vi ville göra det bra, och faktiskt
tycker jag att vi gjorde det. Hoppas ni tyckte det med :)
