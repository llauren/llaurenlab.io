---
title: "Att våga vara arg"
type: post
date: 2020-10-01T22:26:20+03:00
tags:
  - Stulen kärlek
  - känslor
---

Spoiler. I ett skede av _Stulen kärlek_ grälas det. Det var det vi övade på
igår. Och jag måste säja att det är inte lätt att stå där och skrika åt folk och
vara ursinnig. Det är mycket lättare att bara växla repliker och göra
lustigheter, men att vara arg kräver på nåt sätt att man är _närmare_ varandra,
att man känner varandra och att litar på varandra, som människor och som
skådespelare.

Här måste säjas att Matilda gör en alldeles fenomenal arg Berit. Hon är genuint
(teatergenuint) arg och giftig där det gäller och efter ganska mycket skrattande
känns det lite som en chock att vara med om ett sånt gräl på scen. Men det
fungerar. I jämförelse har jag mycket lättare att skrika och vara hotfull mot
Thomas, som jag har grälat med som Sheriffen av Nottingham.

Och bara vi övat lite mer, kommer det nog att kännas hur naturligt som helst att
stå och skrika med frun på scen. Fast det är ju nog lite mer spännande såhär.
