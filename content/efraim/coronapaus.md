---
title: "Coronapaus"
type: post
date: 2020-12-01T17:49:05+02:00
draft: false
categories:
  - Stulen kärlek
---

Vi har lagts på coronapaus och livet är lite tommare. Inga träningar på ett
okänt antal veckor. Och ingen aning om vi kan ha premiär i början av nästa år.
Det vet bara coronan, och coronan gör så gott den kan för att slänga käppar i
hjulet.

På vår sista övning spelade vi in hela skådespelet så vi har nåt material att
öva från. Så mycket blev i alla fall klart att det inte är så lätt att filma
skådespel ... och att vi inte riktigt är premiärklara. Men helt ruttet ser det
ju ändå inte ut. Nu ska jag bara klippa materialet och dela med mej åt mina
kollegor. Och sen kan jag klippa ihop vår trailer. Inte för att vi behöver den
än. Men i nåt skede ska det flygas.
