---
title: "Cyklingstips"
type: post
date: 2019-10-23T22:44:03+03:00
tags:
 - Robin Hood
 - cykling
 - Fallåker
draft: false
---

{% instagram B3-C2asBokb %}

Dagens tips: om ni cyklar från tåget till träningen, cykla inte igenom pölarna.
De är längre, bredare och framför allt djupare än de ser ut. Dina fötter kommer
att hamna under vattnet där du cyklar. Riktigt ordentligt. Och när ni väl cyklar
i pölen kan ni inte vända om.[^vatten]

Om ni trots det cyklar igenom pölarna, se åtminstone till att ni har bytesbyxor,
-strumpor och -skor för kvällen och hemvägen. Eller träna barfota och förbered
er på att det blir oskönt att cykla hem igen i era våta skor.[^summa]

[^vatten]: Alternativet att cykla tillbaka igen och ta andra vägen är
  naturligtvis inget alternativ, eftersom du då kommer att anlända en kvart
  försenad till övningen, där dina kollegor ändå sitter och pratar strunt de
  första femton minuterna.

[^summa]: Summa summarum, inget att lära sej här. Det blir fel vad än du gör.
  Starta tidigare nästa gång och ta den längre vägen.
