---
title: "Dansar Som en Panda"
date: 2019-01-30T15:30:36+02:00
tags:
  - Pippi Långstrup
  - koreografi
draft: false
author: Robin Laurén
---

Är det nåt jag inte är bra på, så är det att dansa. Faktum är att jag i min ansökan
för auditionen skrev _"En panda dansar bättre än jag"_ -- vilket jag vidhåller. Och
på söndagens övning var temat "Dans och drama".

Nu skulle man kanske ha trott att _dans_ är nåt jämförbart med rytmiska rörelser till
musik men sidu nähe. Övningen började med uppvärmning, muskelövningar och stretching,
vilka är tre saker till jag heller inte är bra på. Jag är vig som ett järnspett och
har en lite småfet datanörds muskulatur. Så vad jag trodde skulle vara illa, var värre.

Vår skådespelartropp består till stor del av flexibla personer. Yogaposer är inget
problem för dem. Dessutom har vi fler än en showdansare i gruppen, så jag kan enkelt
sätta mej lägst ner på listan av kapabla dansare.

Men. Har ni sett filmen _Kung-fu panda_? Där har jag nån att ta till mej. Pandan Po
är en drummelpetter, driven av ambition och mat. Men han jobbar på och genom gediget
och målmedvetet tränande blir de ändå en kungfuka av honom. Och det är vad jag får göra.

Dansen i övrigt gick som man kan tänka sej. Jag är fortfarande en panda på dansgolvet,
men åtminstone tänker jag försöka.
