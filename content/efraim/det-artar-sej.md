---
title: "Det artar sej (trots allt)"
type: post
date: 2022-05-15
tags:
  - Finns
  - Pinocchio
  - trailer
  - skägg
---
Vi hade första övningen ute på scenen idag -- äntligen! Vi spelade in
videomaterialet för en trailer för Pinocchio, mest scener med musik och
koreografi. I ärlighetens namn måste säjas att jag har varit lite oroad för vad
det ska bli av alltsammans, men idag var jag imponerad av alla mina
teaterkollegor. Det verkar som alla (andra) kan sina repliker och sin koreografi
-- så nu är det bara jag som behöver öva mina! Visst har vi en lång väg att gå
före vi är klara för premiär, men jag har förhoppningar att vi fixar det.

I slutet av pjäsen ska Gepetto se gammal och sliten ut, vilket betyder att mitt
skägg blivit grått och mitt ansikte skrynkligt. Vi provade med teaterfärg i
skägget och visst blev jag gammal och grå i ansiktet. Jag undrar vad färgen gör
åt skägget om det ska sprejas tre tyra gånger i veckan tills juli eftersom det
kändes lite lite strävt när jag tvättade det där hemma. Jag får visst skaffa mej
en flaska skäggolja om jag inte vill att min haka ska täckas av en filt före
midsommar.

Fick just höra något extra saftigt på repetitionen, angående musiken. Jag låter
producenen gå ut med nyheten, men ta det här som en cliffhanger :)

Ja, och så är det ju premiär om tre och en halv vecka. **VA?! TRE OCH EN HALV
VECKA??!!** ([har ni köpt biljetter](https://www.netticket.fi/pinocchio) ren?)
