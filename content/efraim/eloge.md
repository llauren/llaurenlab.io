---
title: "En eloge till regissör och koreograf"
type: post
date: 2019-12-06T11:56:58+02:00
draft: false
tags:
  - regi
  - koreografi
  
---

Jag vill här ge mitt djupaste hattlyft till regissörer som behöver handha
oerfarna amatörskådespelare. Jag som entusiastisk nybörjare, full av spontana
idéer (och lustigheter) jag inte kan hålla tyst med, en som ifrågasätter eller
ställer frågor om saker som inte vi (dvs regisssören) ännu ens hunnit tänka på
... _kan_ ju inte vara lätt att handla med.

Samma gäller koreografen. Jag är hopplöst långsam att lära mej steg, fullkomligt
okoordinerad med mina två vänsterben, och har så himla lätt att ljudligt
protestera att det här går ju aldrig!!

Jag menar inte det. Jag menar att det är svårt för mej, men inte att jag vill
underveärdera ert arbete. Inte protestera mot era idéer. Ni har antagligen bara
en idé jag inte förstått ännu. Jag borde fatta att bara ta emot _tills_ jag
fattat vad ni tänkt. Och sen kan jag protestera, eller åtminstone med nån form
av respekt föreslå vad jag tycker. Men sist och slut är ni den som formar vår
grej till något enhetligt och vi är de som formas till det.

Så tack och förlåt. Kommer jag någonsin att vara med i ett skådespel till så ska
jag lyssna först, tänka sen, och först sen komma fram med mina egna idéer. De
kanske passar in, eller så går de inte alls ihop med regissörens vision. Eller
så har regissören inte ännu riktigt utformat en vision men mina och alla andra
storkäftade skådespelares eviga skval av protester och åsikter stör arbetet och
gör att det blir nåt helt annat än vad det var tänkt.

Samma gäller antagligen i verkliga livet oxå.

Roligt att märka att jag ännu inte är för gammal för att lära mej nåt nytt.
