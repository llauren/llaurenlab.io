---
title: "Epilog"
date: 2019-07-08T13:51:19+03:00
tags:
  - Pippi Långstrup
  - tystnad
  - slut
---

> _And then it was over and we took our applause.  
We passed the peace pipe and thought no more  
About the scenes that were missing, about the lines we had crossed.  
And we smiled at each other and knew that the moment was lost._

Det är en vecka sen det tog slut. Jag har varit på aktivsemester i östra
Lappland (eller egentligen var det strax söder om Lappland, men det var renar på
vägen, mygg och fjäll, och utanför Ring III, så det var nog Lappland i mina
böcker) så jag har inte haft tid att tänka så mycket på vad det blev av
alltsammans.

Men nu sitter jag ensam i en soffa här hemma och det känns mitt i allt rätt så
tomt. Inga träningar att ta sej till. Ingen föreställning. Inget _gänget._ Det
är som att ha åkt ner för en lång hoppbacke och nu svävar man där i luften på
sina skidor och undrar om man ska komma ner eller bara hänga kvar här utan nåt
under sej. Det är ganska konstigt.

> _I let you snuff out the candles, I let you blow out the flame,  
And I knew that this time it would never be the same.  
The smiles you had wavered, tears welled in your eyes  
And I looked and I knew that this magic it was only a sign_

Sista föreställningen var nog den bästa vi gjort. Jag fick in alla mina
påskägg[^1]. Publiken var underbar, vi blev in-applåderade en extra gång, och
tillochmed jag fick blommor :). Och det var kalas. Fotograferanden. Grillfest.
Kramar. Skumpa och vin (i måttliga mängder, naturligtvis). Mygg. Karaoke till
morgonen. Cykla hem med kameror, stativ, kläder, grejer, och blomster. Och efter
tre timmars sömn, iväg till Lappland. Nejlikan klarade sej vid liv och hälsade
oss när vi var hemma igen!

Det var, överraskande nog, just inga tårar eller ledsna miner när vi skiljdes.
Varma kramar och _"vi ses om men månad!"_ för vi ska nämligen på Raseborgs
"Jorden runt" tillsammans. Sista föreställningen. Och vi kommer att ha
"filmafton" och se inspelningen av Pippi, om jag bara får materialet klippt och
skuret. Och en del av ensemblen kommer att dyka in på Nyländs Afton, antingen
längs bordet eller i bästa fall retav på scenen. Jag är åtminstone med om det
händer!

> _You never really thought it out but I hoped that one time  
That you'd come back for more._

Var det här det? Kommer vi att göra teater igen tillsammans? Ingen aning. Skoj
skulle det vara. En del kanske söker in till nästa års produktion, och en del
inte. Nån från i år kanske söker men kommer inte in. Det beror helt på hurdan
pjäs det är, vad regissören tycker och vilka som helt enkelt passar in i de
roller som finns till buds. Själv har jag kommit med i Fallåker teater som
Sheriffen av Nottingham. Teatern sätter alltså upp Robin Hood -- med partitur av
Sås och Kopp! -- i höst, så teatrandet tar inte slut för min del! Men eftersom
Robin Hood fortsätter till in på våren så lär jag inte kunna vara med i Finns
nästa år. Om inte i en _riktigt_ liten roll... Nå, vi får se :)

> _You started to smile  
Is this really the end?  
Is this really the end?_



_"Sunsets on Empire"_ lyrics by Derek William Dick

[^1]: Om dem har jag skrivit [här](../jox))
