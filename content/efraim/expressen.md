---
title: "Finns-Expressen"
type: post
date: 2022-06-14T08:14:37+03:00
featured_image: "/img/PXL_20220609_174039359.jpg"
image_name: "Skål!"
image_by: "Robin Laurén"
tags:
  - Finns
  - Pinocchio
  - Sås & Kopp
---

Mitt liv är ett tåg just nu. Dunkar fram på rälsen utan tid att stanna upp för
att ens se vad stationerna heter. Framåt, framåt. Repetition. Repetition.
Repetition. Repetition nästan varje dag i en veckas tid -- och så premiär! (jo,
den stationen stannade vi vid ett tag)

Jag måste erkänna att jag ljög lite i [mitt senaste inlägg](/efraim/skrivet/).
Jag var nog egentligen _lite_ orolig hur det skulle gå. Om vi skulle klara av
att lära oss att spela tillsammans, spela teater och inte bara prata fram våra
utantill lärda repliker och sånger, svepa vår koreografi enligt inlärda steg.
Eller om att vi skulle ens lära oss våra repliker och rörelser så vi kunde
upprepa dem utantill.

Det finns en ekonomisk modell som kallas "hockey stick"-modellen[^stick] vilken
i korthet beskriver det att under en lång tid verkar nästan inget hända i form
av framsteg -- och så plötsligt vänder kurvan brant uppåt och hej vad det går
fram! Det är precis vad som hände med oss på Finns. Sen övningarnas begynnelse
har vi i maklig takt lärt oss lite ditt och datt här och där, men inte något som
ens med bästa vilja kan kallas ett premiärklart skådespel. Och jag vet att jag
upprepar mej men den här gången stämmer det: dagen före genrep började det
faktiskt kännas som om det nog blir något, på riktigt. Och visst gick ju
genrepet riktigt bra.

[Morgonen därpå var vi i radion](https://arenan.yle.fi/audio/1-62788121) och
myste av (välspelad) självsäkerhet.

Under premiärdagen hade jag lite svårt att koncentrera mej på mina
arbetsuppgifter. Tack till mina förstående kollegor på jobbet.

Det finns en myt att om generalrepetitionen går (för) bra så går premiären åt
pipan. Pah, säjer jag. Vår premiär gick utmärkt! Vi var förvånande bra. Så bra
att regissören Ylva var rent överraskad och i sitt tal till ensemblen sa nåt i
stil med att det här gick ju bättre än det borde ha gjort. Visst var det ju
fjärilar i magen men de förvandlades snabbt till adrenalin och entusiasm när
overtyren satte igång.

Jag tycker alla på scen gjorde ett fenomenalt arbete. Visst var det små misstag
här och där, men inget som fick tåget att köra av rälsen, och det viktigaste i
att spela teater inför publik, tycker jag, är inte hur rätt och enligt manus och
regi det går, utan hurdant flyt man uppnår och hur man kan komma igen efter att
någon gjort nåt litet misstag. Det är teamwork och när någon står och säjer sin
replik är det inte bara den som talar som har en aktiv roll. Jag ska skriva om
det där med flyt och sammarbete i en framtida postning.

{{< figure src=/img/pinocchio-gepetto-saus-o-kopp.jpeg >}}

Vet ni vem vi hade i publiken då? Jo, herrarna Pasi och Peik, det vill säja
självaste Sås och Kopp, som skrivit manuset och musiken till Pinocchio! Det var
mej en glädje och ära att få säja tack till herrarna och hade jag vetat att de
suttit i publiken hade de fått en hemlig hälsning i början av skådespelet. Ja,
ni som sett det kanske vet vad jag talar om. Men marionetterna i första scenen
har inte samma namn i varje föreställning :)

Efterpå var det kalas och det förvånar väl ingen att det dansades vilt till Sås
och Kopps musik.

Det var torsdagen. På fredagen hade vi ledigt, vilket i mitt fall betydde att
jag skulle sjunga i firmans för sommarkalaset hopskramlade rockorkester (femton
låtar, tre övningar sammanlagt varav en där alla närvarat mer eller mindre
samtidigt; jag hade deltagit i två). Under dagen greps jag av pollenallergi, så
jag tappade en oktav i övre registret, vilket blev instant kris då större delen
av mina låtar gick i höga toner. Tack tack och åter tack till Thilia, Finns
sångcoach, som agerade kriskonsult och hjälpte mej att få tonerna tillbaka, med
ånga, honung-och-citron-i-hett-vatten (romtoddy utan rom, alltså) och försiktig
uppvärming. Precis som på Finns gick det inte helt enligt manus, men vi rockade
som älgar och kom vanligtvis i mål samtidigt.

Premiärveckoslutet fortsatte med spelningar på lördag och söndag. Folk var
fortfarande ganska pirriga. Misstag gjordes och lappades på löpande band och
både publik och skådespelare hade det gott. På söndag hade jag tillochmed
studiebekanta i publiken, som kommit dit mestadels utan barn. Tack till er
speciellt, som dykt in helt "frivilligt"!

Nu sitter jag i friden på landet och skriver det här vid min morgonmacka. Det
börjar bli dags att ta bilen mot civilisationen för i kväll ska vi spela igen --
och *du* ska vara välkommen! Det börjar äntligen kännas lite som om man ur tåget
har tid att titta ut ur fönstret och se vad vi kör förbi ändå :)

[^stick]: Lätkästaga eller ishockeymaila; klubba för er vattusvenskar :)
