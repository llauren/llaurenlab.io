---
title: "Faderstyper i böcker av Astrid Lindgren"
date: 2019-02-02T15:14:58+02:00
tags:
  - Pippi Långstrup
draft: false
---

Jag har ofta känt en dragning till faderstyperna i Astrid Lindgrens böcker. Sen
jag själv blev pappa (och det var ju några år sen), har papporna och
fadersfigurerna i bakgrunden fungerat som nåt form av ideal, men oxå som en
spegel att alla pappor inte är perfekta. Det har säkert skrivits avhandligar om
faderstyperna i Astrid Lindgrens böcker och antagligen skulle jag må bra av att
läsa en.

Min favoritfar är den bullriga, lite vildvuxna pappan som älskar sina barn men
kanske inte riktigt vet vad man ska göra med dem ibland.[^1] Litar på att de
klarar av att ta hand om sej själva men känner sej ändå melankolisk då barnet
växer upp, kan ta sina egna val och börjar klara av sej på egen hand. De här
papporna finns i Pippi Långstrumps Efraim och Ronja Rövardotters Mattis.

En spännande vinkling är de böckerna där pappan _inte_ är närvarande men där
fadern är ersatt med en annan person. Jag tänker främst på Rasmus på luffens
fadersfigur Paradis-Oskar och "farfar" Mattias från Bröderna Lejonhjärta, men
oxå Emil i Lönnebergas lite dysfunktionella fader Anton som fått ge vika som
fadersfigur till förmån för drängen Alfred. I boken Mio min Mio längtar Bosse
efter en far som kompisen Benke men finner sin far som kung i Landet i fjärran.
Dessvärre tycker jag att kungen oxå är lite fjärran i boken, nog mild och
kärleksfull men ändå mera ledsen och avlägsen. Och liksom utan greppyta och
mänskliga svagheter. Inte riktigt av samma kaliber som papporna i Pippi- eller
Ronjaböckerna alltså.

Madickens pappa Jonas är en annorlunda karaktär. Han ser inte så mycket efter
sina döttrar för på den tiden skötte mor och hembiträde barnens uppväxt. Däremot
har han ett modernt, för tiden radikalt, synsätt på social orättvisa, hur
orättvist en del vuxna beter sej mot bar och framför allt hur samhället
behandlar sina mindre bemedlade medborgare och denna livssyn smittar ju han av
på sina döttrar. En förebild för sin tid, ändå. Även grannens välvillige
fyllbult Emil "E P" Nilson och hans tonårsson Abbe fyller sina delar av
mansrollen i boken.

En helt annan pappa hittar vi i Saltkråkans Melker Melkerson. Han är en
tragikomisk figur som vill så väl men har tummen mitt i handen när det gäller
att ta hand om sin familj och deras sommarstuga. Mycket av modersansvaret häller
han, antagligen oförstående, på sin dotter Malin. Han känner att han behöver
berättiga sin existens som försörjande och försvarande familjefar, men får i sin
inkompetens allt oftare agera clown för sin omgivning. Ändå har han ett
ömsesidigt kärleksfullt förhållande till sina barn.

Nu finns det en massa Astrid Lindgren jag aldrig läst, eller åtminstone inte
läst med samma omsorg som böckerna ovan. Mästerdetektiven Kalle Blomkvist läste
jag aldrig, så hans eventuella pappa har jag inget att skriva om. Samma gäller
Kajsa Kavat, Kati, och alla de där småhistorierna i vad det nu var för en bok
och så var det Nils Karlsson Pyssling och Lotta på Bråkmakargatan och jag vet
inte vad. Lillebror i Karlsson på Taket hade faktiskt både mor och far, men de
blev alltid lite bleka för mej, vilket kanske var meningen med boken för varför
annars skulle en filur som Karlsson dyka in? (fast i ärlighetens namn tyckte jag
alltid att Karlsson var lite väl skrällig och självupptagen för att jag genuint
skulle tycka om honom, så kanske jag bara har förträngt familjedynamiken med
resten av historien). Alla barn i Bullerbyn hade mest att göra med varandra och
i den grad jag uppfattade så var föräldrarna närmast statister.

Men en fluffig far som Efraim gör jag gärna. Som ni kanske kan läsa i det här så
är det den lite bullriga pappan jag helst identifierar mej med. Samtidigt känner
jag igen Efraims och Mattis svagheter. Visst, de ger sina respektive dötrar
utrymme och frihet att växa och leva sitt eget liv, men samtidigt är de ofta
frånvarande från sina ungars liv, och närvaro är ju något man gärna vill ge sina
barn och till skillnad från Mattis och Efraim så känner jag dåligt samvete när
jag inte är med mina barn (trots att de antagligen klarar sej bra utan mej
största delen av tiden). Men varken Ronja eller Pippi skrivet ur faderns
perspektiv utan ur dotterns. Dottern har det bra när hon kan växa upp, men Ronja
hade sin far när hon behövde honom. Pippi däremot hade det mindre lätt; hon
kunde bara leva i sin övertygelse att att hennes far inte drunknat i en storm
utan att han skulle återvända och hämta henne.

Hur det går i skådespelet får ni kolla i sommar :)

[^1]: Jo, jag vet att just i dessa bägge fall har pappan i fråga _ett_ barn, men
som tvåbarnspappa skulle det bli så konstig satskonstruktion ... Nåja, spelar
kanske ingen större roll :)
