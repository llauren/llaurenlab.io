---
title: "The Återuppståndelse of Fallåker Teater"
type: post
date: 2020-05-26T08:50:14+03:00
tags:
  - Fallåker
---

Jag vet inte hur mycket mer jag kan berätta här, men om coronaläget tillåter, så
blir det teater på [Fallåker](https://www.fallaker.fi/) i höst och då stiger jag
upp på scen igen!

Fallåker kommer i höst att sätta upp en, i jämförelse med förra säsongens _Robin
Hood_, liten produktion med ungefär hälften så många skådespelare som senast.
Jag tänker inte berätta vad pjäsen är, det reserverar jag producenten, men det
ska bli kul att få medverka! Om virusläget tillåter, blir det spelplan ungefär
som senast, dvs premiär i november, några spelningar före jul, och efter nyår
kör vi sen för fulla hus i några månader :)

Huruvida det blir sång och koreografi vet inte ens jag!
