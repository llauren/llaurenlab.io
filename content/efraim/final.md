---
title: "Final"
date: 2019-06-30T10:07:56+03:00
tags:
  - Pippi Långstrup
  - Finns
  - slut
---

Idag är det sista föreställningen för Pippi Långstrump. Det är slutsålt och
vädret kommer att bli just perfekt. Alla kan sina roller. Gemenskapen är på
topp. Och sen är det slut.

Det är ju inte så länge sen vi började, känns det, fast vi hållit på i över ett
halvår. I augusti 2018 kontaktade jag Jonnna, vår producent och Prusiluska (som
det senare skulle visa sej) att min son kunde vara intresserad att delta och det
var hon som ursprungligen frågade mej om kanske jag oxå var intresserad av en
plats på scen. I januari var det audition. Det lär ha varit 70 personer
närvarande där och knappt 20 fick plats på scen. Tjugo är egentligen ganska
många och jag tycker det är jätteroligt med skådespel som har många i ensemblen!

Vi började öva redan en vecka efter auditionen. Först var det genomläsning och
så småningom blev det dans och sång (och dans-och-sång!) och ganska snart
började vi repetera scener. I april började vi repetera ute. Då hade snön precis
smultit (och borstats) från scenen. Och med tiden kom kostym och mikrofoner i
spel.

En vecka före premiär hade vi genomgång av hela skådespelet och jag trodde att
det aldrig skulle bli nåt alls av det! Folk vimsade, glömde sina repliker (jag,
åtminstone!), rekvisita fanns lite här och där ... det var inte helt olika
_Farlig midsommar_ fast vi hade inte två halva lejon förstås. Men hur det nu
ändå gick, så blev alla _klara_ till premiär.

Premiären var ju pirrig. Alla koncentrerade sej till det yttersta för att få
föreställningen igenom. Det gick på koncentration och adrenalin och känslan vid
Slutsången är ju nåt som bara kan upplevas. Vi gjorde det! Vi klarade av det här
tillsammans och publiken gillade det!

I och med att juni gått och vi haft föreställning nästan varje dag (förutom en
alldeles för lång paus för midsommar!) har vi blivit tajtare, avslappnade,
lekfullare och säkrare. Vi har, tycker jag, aldrig varit så bra som vi är nu.
Och lite som en mandala känns det både fint och lite vemodigt att det ska ta
slut idag. För att fortsätta muminanalogin, så blir det ju förstås fest idag
oxå.

_Vi ska alla dö en dag. Men först ska vi leva._ (okänd)

Jag vill för min del utfärda ett hejrungande **TACK** till alla i ensemblen
(härliga "kollegor"), alla i maskineriet (teknik, utstyrsel, biljetter,
byrokrati, café, parkering och jag vet inte vad!), våra underbara sång- och
danslärare och inte minst, vår underbara publik! Och ett specialtack till Jonna,
som lurade med mej till detta vansinne! Det här har varit min första gång på en
amatörteaterscen och jag kan redan nu berätta att det inte blir den sista. Men
mer om det nån annan dag. För nu ska här samlas kraft till en final varken vi
eller ni sett maken till. I kväll brakar det loss och vi ska ha vår bästa
föreställning nånsin!

Så det, ni!
