---
title: "Framtiden är oskriven"
type: post
date: 2020-11-23T10:08:28+02:00
tags:
  - Stulen kärlek
  - Fallåker
---

Det känns lite konstigt att öva teater under dessa coronatider. Vem är det vi
övar för? En publik som kanske aldrig blir till? Oss själva?

Enligt ursprungligt schema skulle vi ha haft premiär nu i veckoslutet. Det hade
vi inte, men vi drack ett glas skumpa under repetitionen igår till minnet av
premiären som inte blev av. Vi har ju ingen aning om vi kan ha premiär i januari
heller, men vi rättar oss efter verkligheten och spelar sen när det är möjligt.
Jag spekulerar här, men jag tror inte det blir aktuellt att spela för en publik
på 20 personer i en sal som drar 180. Men i vart fall vill vi jättegärna att det
ska _bli_ premiär och det ska bli alldeles jätteroligt att få spela för en
levande publik.

Det är å andra sidan ganska skönt att inte ha premiär bakom knuten. I det tempo
vi övat nu, två gånger i veckan --varav första halvtimmen av varje repetition
gått till kaffe och prat-- skulle vi förstås inte varit klara till en premiär i
november. Men vi börjar nu vara i det skedet att vi repeterar utan manus och
enseblen börjar sakta få koll på sina repliker. Frank Skog jobbar hårt med sin
dubbelroll som både regissör och sufflör. Det är både lite pirrigt att stå på
scen utan papper i hand, men samtidigt väldigt befriande. Man kan så småningom
börja _skådespela_ och inte bara läsa upp. Man kan så på scen i sin kostym (för
min teaterkostym är de facto en kostym -- väldigt meta) och _vara_ sin karaktär.

Så nej, jag sakanar inte premiärstressen, det är roligt att öva tillsammans.
Bara det sen blir premiär alls.



([Dagens referens](https://www.youtube.com/watch?v=ibmKgiVv1No))
