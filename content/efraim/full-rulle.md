---
title: "Nu är det full rulle"
type: post
date: 2021-10-10T22:42:48+03:00
draft: false
tags:
  - Stulen Kärlek
  - Fallåker
---

Nu är det full rulle på Fallåker! Faktiskt så full att vi bara har fyra
spelningar kvar; tre nästa veckoslut och sista showen lördagen därefter.

Det har varit härligt att spela för riktig publik. Förra veckoslutet hade vi
över 100 åskådare på båda föreställningarna och jag tror vi hade 150 idag. Så
inte bara full rulle, men faktiskt så fullt hus som coronan tillåter. Inkommande
lördag är det slutsålt på åtminstone ena föreställningen, så det är ju bra att
vi har två :)

Det positiva i att det varit en så lång "träningsperiod", dvs att vi inte fått
spela men nog haft möjligheten att öva, är att vi är ganska bra på vad vi gör.
En massa småord, miner och handviftningar har dykt in som aldrig kunnat stå i
manuset. Och vi har helt enkelt kunnat joxa lite med rollerna så att de känns
mer personliga.

OK, alltid går det inte så bra. Igår tappade jag repliken två gånger. En av
gångerna blev det full blackout ett tag, men i stället för att viska lite tyst
att jag tappat bort mej sade jag fullt ut nåt med att nu är jag helt borta,
vilket passade helt in i situationen och Matilda kunde rädda mej med att säja
att jag skulle kunna sköta hushållsuppgifterna. I ett annat skede är jag fullt
säker på att jag sa "Jag bor här", men antagligen tänkte jag det bara, för det
blev lite konstigt i replikskiftet efter det. Men eftersom det ändå var i ett
lite råddigt ställe i skådespelet var det kanske ändå bara våra mest hängivna
fans, nåja, bara vi själva då, som märkte det.

Idag koncentrerade jag mej till den grad att alla repplikerna kom med, i rätt
ordning och allt. Hoppas jag spelade hyfsat, eftersom den delen fick gå lite på
autopilot medan jag gjorde mitt bästa att säja rätta saker. Men efterpå kändes
det mindre nervöst.

Konstigt att vi bara har två veckoslut till i Fallåker med den här produktionen.
Det har gått snabbt när det väl kom igång.
