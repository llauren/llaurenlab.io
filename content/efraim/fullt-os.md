---
title: "Fullt ös"
date: 2019-05-28T11:23:42+03:00
tags:
  - Pippi Långstrup
  - Finns
  - repetition
  - regn
---

Det är fullt ös i Pippi-produktionen. Så mycket händer att jag knappt hinner sitta
ner och reflektera! Vi har övning nästan varje dag nu, tre eller fyra timmar i
kvällen och mer på veckosluten. Och det är premiär OM EN VECKA! (och några dagar)
Ännu har inte premiärpaniken kommit, men det är väl bara för att jag inte riktigt
insett verklighetens allvar ännu.

Vi har övat ute på scenen i säkert en månad nu. Vi kör med kostym (vilket är
[teaterspråk](https://teaterforbundet.se/yrkesavdelningar/scenografer-kostymdesigner/lathund/ordlista/)
för sånt man har på sej), rekvisita (sånt som man har med sej) och mikrofoner.
Mikrofonerna tejpar man på kinden, bakom örat och ner bakom nacken. Och så ska man
dra tejpen av sej efter att man spelat. Har man tejpat sej i det tunna håret i
nacken eller bakom örat så märker man det senast då. I mikrofonen sitter en
sändare man sänker ner under skjortan och av nån orsak är den alltid jättekall!

Härom dagen var det uselt väder och de flesta i gruppen var på lika surt humör som
vädret. Jag själv greps väl av stundens vansinne så jag hoppade omkring och hojtade
med näven i vädret att nu ska vi fröjda bort ovädret om vi så ska dö på kuppen!
(ingen dog eller ens halkade på scenen, så det är ju bra .. och regnet avtog under
eftermiddagen)

Numera tränar vi långa sjok, en halv eller hel akts genomdrag (genomgång), för att
få känsla för hur alltsammans hänger ihop. Själv lyckades jag totalförstöra min
känsliga scen i slutet av skådespelet genom att säja min replik från _nästa_ scen
och med det så höll vi nästan på att skippa alltsammans som kommer däremellan
(inga spoilers här, men det finns faktiskt ett par scener som hoppeligen kramar om
hjärtat på publiken lite). Bra att regissören Oskar var på alerten, men jag
tappade ju konceptet för resten av pjäsen och kunde helt enkelt inte komma ihåg
vad jag skulle säja därnäst. Det var ganska beklämmande.

Och igår fick vi läktaren installerad. Visst tränade vi ju inför en tom läktare,
men ändå kändes det lite mer "riktigt" igen. Och det är ju helt rätt!

I övrigt tycker jag att vi börjar få nåt som faktiskt påminner om ett skådespel vi
kan visa upp, så småningom. Och om precis en vecka har vi generalrepetition inför
levande publik -- och SEN ÄR DET PREMIÄR! AAAAAGGGHHH!
