---
title: "Saker jag undrar över"
date: 2019-02-10T12:30:37+02:00
tags:
  - Pippi Långstrup
---

Varför är Pippi anarkist? Är det bara för att hon växt upp med pirater (med en
piratkapten till far) och då får man bete sej lite hur man vill? Eller är hon
bara en motpol till det Organiserade Samhället där man Borde Bete Sej på vissa
sätt?

Hur hittade Pippi till den lilla byn där hon slog sej ner?

Var Pippi med på Hoppetossa då hennes pappa sköljdes av från skeppet en storm
i Söderhavet? Om hon stack före det, hur kunde hon veta att pappa hade hamnat i
havet? Om hon stack mycket efter det borde hon väl nästan ha varit där då de
hittade Efraim igen. Så hon måste väl ha stuckit precis efter ovädret. Fast hur
hon hittat till "Sverige" (eller Esbo) i värsta oväder är ändå ett mysterium.

Är Villa Villekulla i pippihistorien det första fasta ställe hon bott på eller
finns det andra Villa Villekullor före det? Finns det anda _Tommykor_ [^tommyka]
från hennes tidigare liv? Pippi är ju en social person, så hon borde väl
rimligen bekantat sej med folk förr, om inte annars, så på vägen från Söderhavet
till historien här.

Seglade Pippis mamma oxå på Hoppetossa? Eller var hon nån som Efraim träffade i
en hamn nånstanns?[^mamma] Är Pippis mamma verkligen död? Hur gick det så?

Hur fick Pippi sej en häst? Det etableras (spoiler!) att Lilla Gubben inte
gillar att vara ombord på ett skepp, så det här måste ha hänt efter att hon gett
sej av. Och så kanske hon köpte honom för gudlpengar.

Varför är Pippi stark?

Varför är det socialt acceptabelt att vara pirat? Hur har inte Interpol lagt ut
en efterlysning på Efraim Långstrump och kunde inte Barnavårdsnämnden ta hand om
Pippi helt enkelt baserat på att Pippi _har_ en pappa, som är kriminell?

Varifrån härstammar alla guldpengarna? Rimligtvis är de stulna. Är det helt okej
om Pippi använder stulna pengar? Och om "de vuxna" medvetet tar emot stulna
pengar som betalningsmedel, bryter de inte oxå mot lagen?

Pippi är en reaktion mot det auktoritativa samhället som inte vill se saker ur
barns perspektiv. Pippi är snäll med de hon kan identifiera sej med. De flesta
vuxna är bara konstiga.

Pippi vet inte om hon kan läsa (eftersom hon aldrig har prövat) men ändå har hon
en skylt _"Villa Villekulla"_ som hon kan läsa. Vem som skrivit skylten vet man
inte. Det är ju möjligt att Pippi kan skriva trots att hon inte kan läsa.

Är Pippi egentligen bara en fantasi? Vems fantasi i så fall?

Pippi Långstrump är en kombination av Karlsson på taket och Emil i Lönneberga.
Pippi är en välvillig, socialt inkompetent anarkist. Karlsson på Taket är mest
bara en socialt inkompetent narcisst; Lillebror är i princip allt som Karlsson
inte är (var det Lillebror som de facto skapade Karlsson? Det är åtminstone hur
hans föräldrar såg saken, dvs helt i ton med de vuxna i Pippi Långstrump). Emil
är välvillig och påhittig; det Alva i hennes annaler kallar "hyss" är egentligen
inte onda påhitt sådär som jag förstår mej på hyss, utan närmast påhittigheter
utan försikt eller tanke på eventuella konsekvenser.

Jag borde antagligen läsa om Karlsson och ge honom en ny chans. Nåt bra måste
det väl va med honom. Han har trots allt en propeller i ryggen och är
förepråkare för att man inte ska ta saker på så stort allvar här i världen. Och
dessutom var ju signaturmelodin i teveserien alldeles oerhört _funkig!_

Flear fungeringar en annan gång. Och kanske några svar.

[^tommyka]: Tommy och Annika

[^mamma]: [I verkliga livet](https://slakthistoria.se/artiklar/pippis-pappa-i-verkligheten) var Efraims hustru faktiskt dotter till hövdingen på söderhavsön [Simberi](https://goo.gl/maps/z6MvpQcunf72), norr om Papua Nya Guinea. Efraim Långstrup hette för övrigt Carl Petterson. Pippi Petterson låter kanske lite konstigt, men inte helt omöjligt för historia av Astrid Lindgren.
