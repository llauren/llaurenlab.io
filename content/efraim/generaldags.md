---
title: "Generalen knackar på dörrn"
type: post
date: 2019-11-19T16:44:21+02:00
draft: false
tags:
  - Robin Hood
  - Fallåker
  - repetition
---

Så har vi tränat, repeterat och övat och i kväll är det Generalrepetition 1.
Inför testpublik. Inte för att det finns så mycket vi kan ändra på om publiken
är missnöjd, men det är kanske främst så att vi får testa att spela för en
levande samling människor.

Repliker, scenbyten och koreografier börjar kännas, om inte säkra, så åtminstone
bekanta. Jag har tillochmed mejkat mej runt ögonen en gång och idag ska jag göra
det igen. Det känns som om det här kunde bli både klart och roligt, för både
ensemble och publik.

Under veckoslutet hade vi två maratonövningar. På lördag var det sång och
koreografi i sex timmar, på söndag var det två genomgångar av skådespelet, plus
återkoppling, i likaså sex timmar. Det var ganska pirrigt och nog så intensivt.
Bastun där hemma har sällan känts så schöööön.

På onsdag är det Generalrep 2 och lite beroende på hur det går då så blir eller
uteblir Generalrep 3 på torsdag. Och på fredagen är det **PREMIÄR!!**

Om det finns en osåld biljett DU inte ännu köpt, gör slag i saken!
