---
title: "Generalrep Ett"
type: post
date: 2021-02-11T19:08:04+02:00
category: Stulen Kärlek
tags:
  - Stulen kärlek
  - Fallåker
  - repetition
---

{% instagram CLKNMZpB-mk %}

Så var det ett nånslags genrep för Stulen Kärlek, för en massiv publik på fem
personer. Jädra corona, minns ni?

Det började ... ja, först satte inte musiken igång, men sen började det riktigt
fint. Ända tills jag blackade ut just när jag skulle säja min femte replik,
vilket var både fasansfullt och jättekonstigt -- allt bara försvann framför mej
-- men lyckligtvis var vår regissör Frank på hugget, så jag kunde haka på utan
att tappa känslan. Och resten av min scen gick riktigt bra.

Nu på lördagen är det alltså internpremiär för de närmast sörjande
(finansiärerna alltså) och eftersom det påstås att om genrepet går dåligt går
premiären bra. Eller omskrivet, jag kommer nog att minnas alla mina repliker,
men känslan kommer att vara som en disktrasa :)

Åtminstone ska det vara premiärkalas!
