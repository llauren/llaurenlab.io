---
title: "Genomgång, akt ett"
type: post
date: 2022-04-10T23:27:47+03:00
tags:
  - Pinocchio
  - Finns
  - COVID
featured_image: "/img/pinocchio-genomg-1x.jpg"

---

Igår hade vi genomgång av Pinocchio, första akten. Själv har jag varit på
arbetsresa och i (därpåföljande) COVID de senaste två veckors övningar så det
kändes lite speciellt att vara tillsammans med gänget igen. Lite ovant men
mycket skoj att se alla bekanta fejs :)

Genomgång är en form av övning där man går igenom skådespelet eller en del av
det, lite på samma sätt som om det vore föreställning. Det betyder att man kör
repliker, sång och koreografi som det står i manus och att avbryter så lite som
möjligt. På så sätt får man både som skådespelare och regissör (och koreograf
och scenograf!) en helhetsbild av hur det hela kommer att bli. Efteråt ger sen
regissören feedback om vad som fungerade och vad som inte gjorde det, varför,
och vad man borde göra i framtiden.

Nu på första genomgången hade vi ju inga klädbyten, bakgrundsmusik eller ens en
egentlig scen, men att köra ett helt och mestadels oavbrutet sjok av skådespelet
gav ändå en känsla av "på riktigt".

En del artister kunde ren en hel del av sina repliker, vilket jag ger ett
hattlyft för. Klart bättre än jag, och jag har ändå inte många repliker i
jämförelse med till exempel Benjamin Syrsa (som klarade merparten av sina rader
utan manuskript -- strongt!). Själv var jag nog ganska rostig, både i lederna,
hjärnan och rösten. Det får gå om i takt med coronan.

Ett litet tillägg har det blivit medan jag var borta. Johan Finne kommer nu att
agera hantlangare för mej som Sergei, vilket kommer att bli kul. Jag har ju
aldrig egentligen skådespelat med Johan annat än i stora scener i Saltkråkan,
som dessutom aldrig blev av. Det blir bara en liten snutt, men ska bli kul å få
göra nåt smått med den gamle räven!
