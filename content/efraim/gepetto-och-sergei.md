---
title: "Gepetto och Sergei"
type: post
omit_header_text: true
date: 2022-03-07T21:46:16+02:00
featured_image: "https://live.staticflickr.com/65535/51928050563_732d7351ff_c_d.jpg"
image_name: "[Finns där en sommarteater?](https://flic.kr/p/2n7GQhB)"
image_by: "[Robin Laurén](https://www.flickr.com/photos/llauren/)"
tags:
  - Finns
  - Pinocchio
  - repetition
  - typecasting
  - reflektion
  - cykling
  - backstage
---

Inkommande sommar kommer jag att stå på en bekant scen. I juni 2022 kommer ni
att kunna se mej som Pinocchios far Gepetto, samt i en biroll som den onda
Sergei. Dessutom kan ni se min skönsjungande dotter som den goda fén!

<!--more-->

Hittills har vi haft två repetitioner och en provsjungning med Finnsarna. Rep 0
var hej på er alla och genomläsning av pjäsen och i söndags på första egentliga
repetitionen gick vi igenom de första par scenerna. Och så i igår provsjöng en
del av oss sina låtar så att Thilia, vår snitsiga insjungare, kunde kolla vilken
tonart som inte skrämmer bort publiken. I mitt fall blir mitt öppningsnummer
antagligen i originaltonart och Sergeis _Åsnor ska ni va_ tar vi upp ett tonsteg
(vilket ännu lämnar utrymmte till lite falsettgnäll om strupen godkänner).

En piffig detalj är att Fröken och jag har många gemensamma scener, vilket är
lite praktiskt med tanke på transporten. Snart blir det ju dessutom cykelsäsong,
vilket långtida läsare av denna blogg[^hah] kanske minns är något jag
uppskattar, speciellt i kombination med teater. Och har jag inte skrivit om det
så är sammandraget det att det är riktigt skönt att trampa hem i sommarkvällen
efter avslutad föreställning (och eventuellt en liten snackis och en mestadels
alkoholfri bubbeldryck med såna i teamet som heller inte har fullt så bråttom
hem). Ja, jag ser fram emot sommaren. Och teatern. Och publiken. Det ska bli så
gött.

En detalj jag inte kan låta bli att märka är att jag på Finns spelar väl menande
men inkompetenta fäder, med biroll som gruffiga sluskar (på Fallåker spelar jag
män utan byxor). Ett klart exempel på typecasting. Både Efraim och Gepetto
slarvade på nåt sätt bort sitt respektive barn och borde med facit på hand
kanske aldrig fått faderslicens. Å andra sidan var deras barn självständiga, om
så inte i båda fallen fullt lika kompetenta att hålla reda på sej själva.

På vägen hem från repetitionen åkte vi en sväng via Finns scen. Den nya läktaren
känns gedigen (och snöig; dito scenen är täckt med en halvmeter snö). Högst
uppen är där en teknik-koppi. Men den största överraskningen var att
backstage-konttin var försvunnen. Enligt producent Malin hade den blivit till
den grad murken att den behövde pensioneras. Var vi ska dricka kaffe nästa
sommar är än ett mysterium.

[^hah]: ...säjer han med en god gnutta självironi
