---
title: "Halva Pjäsens Skådespelare"
type: post
date: 2021-10-10T22:55:09+03:00
tags:
  - Stulen Kärlek
  - reflektion
  - Fallåker
---

Jag har egentligen en lite konstig roll i _Stulen kärlek._ Jag är med i första
aktens första scen och sen droppar jag in och ut under andra akten några gånger.
Det betyder alltså att jag ska köra 100% vid start men sen kommer jag aldrig
riktigt igång eftersom det är nästan 50 minuter tills jag tittar in igen. Och
det bara i tio sekunder. Så när resten av ensemblen är eld och lågor till pausen
har jag knappt börjat.

Att komma in i farten då är inte helt enkelt; det är lite som att hoppa på ett
tåg i rörelse.  Noll till hundra, hundra till noll, och sen till hundra igen.
Visst har det gått bra, men som sagt, känslan är lite konstig. Publiken är ju
redan ombord och det är resten av ensemblen med; det är bara jag som behöver
haka på med ett ryck för att det inte ska verka konstigt.

En annan underlig grej är att jag är den enda i skådespelet som egentligen inte
är en _rolig_ person. Jag är barsk, arg, stressad, upprörd och ganska otrevlig.
Jo, de andra har sina brister (egentligen finns det inte en enda "normal" person
i pjäsen) men även sina soligare sidor. Min uppgift i pjäsen är att de andra ska
få vara skojiga i jämförelse. I det ljuset känns det lite konstigt att locka
fram några skratt ur publiken när man är (spelar) ett sånt arsel. Det är liksom
inte helt säkert om det är förtjänat.

Inte lätt att vara svår ibland.
