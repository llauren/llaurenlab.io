---
title: "Hur Gick Det Sen?"
date: 2019-07-20T12:36:43+03:00
tags:
  - Pippi Långstrump
  - slut
  - Finns
  - Robin Hood
  - Fallåker
---

Så var det, i det stora hela, slut på Pippi Långstrump. Ja, det blir lite
revival ännu i och med att vi ska till Raseborg tillsammans och kolla på _Jorden
Runt på 80 Dagar_ och så kommer vi att ha en filmkväll ännu (om jag bara får
filmerna klippta). Men det egentliga arbetet är ganska långt klart.

Vad tror ni att det hände sen?

Jo, det hände sej lite lustigt att en herreman från Yleradion var och
intervjuade oss för Pippi under en av de sista övningarna före premiären. Och så
blev han och snattrade efteråt och det visade sej att han oxå sysslar med
teater, närmare bestämt skulle han vara med i Fallåkers uppsättning av Robin
Hood (med partitur av Sås och Kopp!) och att de just haft genomläsning.

Oj, sa jag, utan eftertanke. Ända sen jag såg Robin Hood på Raseborg en hel
massa år sen har jag drömt att en dag få spela Broder Tuck -- vilket var tryggt
för mej att säja för uppenbarligen hade de ren en ensemble och dessutom jobbade
ju jag för Finns.

Nå, dagen efter ringer producenten för _Robin Hood_ till mej och hör sej för om
jag eventuellt kunde tänkas vara intresserad av att spela Sheriffen av
Nottingham i Fallåker? Jag bad om 24 timmars tänketid och kollade med familjen.
Och så blev det ett ja.

Nu är ju frågan om jag ska blogga om det oxå? Och under rubriken _Kalla mej
Efraim?_ Kanske det är helt OK att tänka sej att "Efraim" står för en rollfigur,
oberoende vad han (hon, den, det, de, osv) heter på scen. Sheriffen av
Nottingham i Robin Hood-världen har förresten inget etablerat namn, och det är
oklart om han existerade i verkligheten heller. Kanske han oxå hette Efraim? :)

Det blir nog att blogga ändå. Jag känner klart att jag behöver skriva lite mera.

PS: Jag försöker sätta på kommentarer i denna blogg, så kanske jag får lite
interaktivitet hit oxå :)
