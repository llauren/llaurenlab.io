---
title: "I sol och i rusk"
type: post
date: 2022-06-19T22:01:50+03:00
featured_image: /img/PXL_20220618_124604820.jpg
omit_header_text: true
tags:
  - väder
  - regn
  - Finns
  - reflektion
  - Skott
  - Tenala
---
Vädret är ett oundvikligt element av sommarteater som spelas ute. Ett halvt år
före premiär, när spelplanen läggs upp, går det omöjligen att säja om det ska
bli sol eller regn, vind eller åska, hagel eller halvmulet. Man får väl vara
glad om det alls blir något väder och man kan hoppas på det bästa.

Sannolikt är att det kommer att vara sommarväder, med flera soliga dagar och
åtminstone en föreställning i regn.

Igår hade vi två.
<!--more-->

Det är något speciellt med föreställning i regnväder (åtminstone de jag varit
med om). Ensemblen kämpar på lite extra, både för att hålla eget humör uppe och
för att den den undebara publiken ska få allt den är värd för att den valde att
dyka in till oss, vädret till trots. Och kanske publiken tänker lite i samma
banor: har vi nu kommit hit ska vi trivas och så får vädret göra som det tycker.

Tar regnet slut under första scenen känns det som en seger.

Är det dessutom så att en annan teater kommer på besök just då, blir det något
magiskt.

Ni kanske inte som publik vet hur mycket ert stöd betyder för oss just under
regniga föreställningar. Jag tackar er ödmjukt för era applåder, ert skratt och
ert stöd. Ni gör att vi klämmer på lite extra, drar ut lite mer i svängarna,
hejar och hoppar lite mer än under en solig föreställning. Jag tackar speciellt
Tenala teaterförening [Skott](https://www.skottrf.fi/) som själva förevisade
Pinocchio några år sen och som nu satt i publiken och sjöng med i varje låt,
skrattade, klappade, njöt och hade skoj! Ni har varit med om precis samma som vi
och vad ni gav oss med er lilla men ack så intensiva delegation kan inte mätas i
pengar. Jag lovar att vi kommer att ha minst lika roligt när vi besöker er
senare i sommar.

Vad som kändes som en oundviklig flopp blev till slut den bästa föreställning vi
gjort. Jo, lördagens andra föreställing gick oxå bra och jag kan bara tacka
publiken. Dessutom regnade det ännu lite mindre och det var ju skönt för alla.

Idag var det strålande solsken från en just lagom molnig himmel. Det var inte
för varmt och inte för kallt. Tekniskt sett gick föreställningen kanske ännu
bättre än lördagens dubbel, men det är nog ändå nåt extra med föreställning i
regn. Fast i ärlighets namn räcker det nog med _en_ regnföreställning; nu har vi
upplevt den här sortens magi och resten av magin hoppas vi kunna framföra torra!

{{<
  figure src=/img/PXL_20220618_164109715.jpg
         title="Stromboli (Tenala) och Stromboli (Finns)"
>}}
