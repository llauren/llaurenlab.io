---
title: "Idag händer ingenting"
type: post
date: 2022-06-17T23:52:05+03:00
featured_image: /img/PXL_20220617_204631382.jpg
image_name: "Bloggen backstage"
image_by: "Robin Laurén"
tags:
  - Finns
  - Pinocchio
  - reflektion
---

Idag händer ingenting. Åtminstone känns det så. Eller, idag var jag på jobbet
och där hände jobbsaker som det gör när det är vanlig vardag. Jag träffade
kollegor, åt extra mumsig fredagslunch och tog mej en liten glass på
eftermiddagen, men just nu är det så tomt att det känns som världen stannat.

Vi har nämligen ingen föreställning idag.

Sedan femte juni har vi haft två dagar då det inte varit teater. Idag är den
tredje och det är som att ha blivit slängd av karusellen. Det är faktiskt inte
mer än en vecka sen vi hade premiär och det känns som om vi spelat hundra gånger
under ingen tid alls. Dessutom är hela familjen utflugen. Frun har ledigt så hon
är med hunden på stugan. Ungarna har diverse skoj på egen ort. Jag har legat på
soffan och försökt samla mitt huvud, grillat lite för mej själv och bankat
staket på bakgården. Det är som en vardag ute i rymden och jag vet inte hur jag
ska behandla det. Jag antar att jag helt enkelt saknar mänskorna jag brukar ha
omkring mej, intensiteten, surret, spänningen, uppträdandet, publiken.

Ensemblen visar en tidigare oskådad nivå av fnattighet. Det är nästan hysteriskt
bakom scen före föreställning. En del skådespelare har hittat på att göra en
konst av ringningarna före föreställningen och då pausen börjar ta slut. Folk
har burit på varandra, jagat varandra, blivit slagna i huvudet med en yxa (av
skumgummi). Det är som en hel minishow och det är en njutning att se hur de
hittar på och får göra tokigheter helt _själv._ När regissören är borta dansar
skådespelarna runt scenen.

Men fnattigheterna till trots har det varit nog så professionellt på scen. Det
är egentligen spännande att tänka på att ingen enstaka person _styr_ maskinen
som är vår ensemble. Vi kollar tillsammans med skådespelarna, tekniken och
biljettkiosken (som har kontakt med parkeringen) att vi kan börja. Folk håller
reda på sin rekvisita mer eller mindre själv. Jo, det finns arbetsroller. Jonas
kör tekniken. Jag har koll på tiden[^tiden] och radiokontakten och resten av
ensemblen att att vi kan börja spela. Ellen ("Mamma") manar slarvigare
skådespelare med varm beslutsamhet att bära sina grejer till "Konttin"
(backstage-containern) så att hon kan låsa den efter att vi är färdiga. Men
ingen är boss över någon annan. Det är samarbete. Folk hjälper och stöder
varandra. Maskinen sköter sej själv eftersom alla egentligen vet vad de ska
göra. Det är imponerande, med tanke på att det faktiskt bara finns tre i
ensemblen som (till sin ålder) inte är minderåriga.

Sånt här lär man sej inte i skolan. Att joba tillsammans, att ta ansvar, att
göra sin grej både för varandra och för publiken. Det är därför teater är så
härligt.

[^tiden]: vilket är lite spännande för är det något jag inte är bra på
  (förutom dans) så är det att hålla tiden
