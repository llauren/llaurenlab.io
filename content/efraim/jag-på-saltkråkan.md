---
title: "Jag på Saltkråkan"
type: post
date: 2020-01-16T23:53:53+02:00
draft: false
tags:
  - Saltkråkan
  - Finns
---

Då jag började med Finns Sommarteater som Efraim bara ett år tillbaka,
postulerade jag en tes att efter tre produktioner kan man (eller jag åtminstone)
befogat titulera sej amatörskådespelare. Nå, tidigare i veckan tilldelades jag
min tredje roll, så om jag med hedern i behåll klarar mej igenom _Vi på
Saltkråkan_ denna sommar, kan jag skriva in en ny titel på CV:n.

Även i denna Finns-produktion har jag två roller, morfar Söderman och direktör
Karlberg (även "Carlberg"). Rollen som Söderman är så vitt jag förstår klart
mindre än både Efraim och Sheriffen, och Carlberg är åtminstone i boken närmast
en "plot device"[^intrigpryl], så det kanske blir en lite lättare dust för mej
denna gång. Helt okej med tanke på att Finns-övningarna börjar medan Fallåker
fortfarande spelar!

Det var riktigt kul att vara på audition. Nästan hälften av Finns 2019 ensemble
var på plats, och flera blev antagna (men de får avslöja det själva). Det visade
sej vara svårt att provspela snälla personer som Melker och Krister. Hur jag än
försökte, så blev det ganska bullriga karaktärer, kanske mer som Mattis ur Ronja
rövardotter. Vad som fascinerade mej till ingen ände var att koreografidelen
bara verkade svår, inte totalt obegriplig, som koreografi hittills varit för
mej. Kanske jag faktiskt har lärt mej nåt under det senaste året. Miraklens tid
är inte över!

Saltkråkan-ensemblen träffas första gången i början av februari, så det lär bli
ny rapport efter det!

[^intrigpryl]: Plot device, "intrigpryl", en grej vars enda uppgift är att föra
  intrigen framåt. Se [wikipedia](https://en.wikipedia.org/wiki/Plot_device).
