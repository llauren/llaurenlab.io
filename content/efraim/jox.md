---
title: "Påskägg"
date: 2019-07-08T17:49:39+03:00
tags:
  - Pippi Långstrup
  - lustifikationer
  - regn

---

Det finns (ha!) en tradition inom teatern att i sista (och/eller första)
föreställningen lägga in några extra jox och lustigheter för att överraska sina
kollegor. Det gjorde oxå vi, fast sist och slutligen i ganska liten mängd. Det
fanns en grillkorv i Pippis väska, men den märkte hon (Jenny) före
föreställningen. Efraim gick omkring med en säkerhetsnål i örat som en bästa
punkare. Och så fanns det en bild av Efraim i hinken i havrelådan som både Pippi
och poliserna blev lite överraskade av (och Efraim oxå, eftersom Jenny tagit
bilden ur hinken och placerat den i botten av lådan).

Inom software-världen talar man om "påskägg", gömda små skojigheter som bara den
invigna kanske upptäcker och förstår. Sånt finns det ofta i film oxå. Och
eftersom jag är lite av en nörd, så  beslöt jag att sätta in några påskägg i
form av referenser till andra verk av Astrid Lindgren i några av Efraim
Långstrumps repliker.

**Pippi, min Pippi**
: Ursprungligen _Mio, min Mio_ från boken med samma namn, vilket är vad Mios
pappa "kungen" brukar säja till sin älskade son och vad Efraim sa efter att
Pippi kännt honom på nosen (ursprungligen förresten _känn på kalla nosen_, men
eftersom det var så varmt att spela teater i sommaren så blev det _känn på varma
nosen_ i vår version ... förutom den gången det regnade i andra akten och då
blev det _känn på våta nosen_)

**Voffor då då?**
: Vad rumpnissarna i _Ronja rövardotter_ brukade säja om det mesta. Vad Efraim
sa då Pippi ville att pappa skulle gömma sej i havrelådan

**Tänk jag drömde i natt**
: ... att jag hade en katt, är en låt som Paradis-Oskar kör i _Rasmus på luffen_
och som Efraim spelade på munspel tills han såg sin dotter med en hink på
huvudet. Det här var det enda påskägget jag vet att nån ur ensemblen bongade. I
sista föreställningen spelade jag temat ur _Pirates of the Caribbean_ på
näsflöjt.

**Du och jag, Pippi**
: är vad Emil (i Lönneberga) och Alfred brukade säja till varandra när allt var
så där _riktigt_ bra

Jag hade egentligen tänkt sätta in utropet _"jag har ett barn!"_ (Mattis, från
Ronja Rövardotter), men det blev inte av. Och dessutom hade jag ju ren en
Ronja-referens. Funderade ännu på nån bevingad fras av "farfar" Mattias från
Bröderna Lejonhjärta, av Melker från Vi på Saltkråkan eller nåt av "farbror"
Emil P. Nilsson (för så hette han!) ur Madicken, eller nåt från en helt annan
Pippi-bok, fast jag inte nu minns vilken. Fast med tanke på hur få repliker den
gode Efraim egentligen hade är väl fyra påskägg mer än väl?

Så nu vet ni det! Fast det kanske ni visste redan ;)
