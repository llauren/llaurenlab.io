---
title: "Kalla Mej Efraim"
date: 2019-01-29T07:01:12+02:00
tags:
  - Pippi Långstrup
  - Finns
  - reflektion
---

Kalla mej ... Efraim. För några år sen, inte så stor skillnad precis hur många,
började jag sjunga. Det var på ett musikläger, där mina ungar och senare jag
spenderade närmare två veckor i ett samhälle i Västra Nyland. Efter att ha
besökt ungarna titt och tätt under några års läger blev jag rekryterad av
lägrets rektor och hur det nu gick sej så fann jag mej i en sångensemble och
hur _det_ nu gick sej så skulle vi uppträda inför publik. Det var min stora
fasa. Och jag klev över den.

Med tiden blev min son lite trött på lägret. Kanske för att han sov för lite.
Han funderade på att vad om han inte skulle gå på nästa läger. Men då behövde
han ju nåt annat att göra tyckte jag. Och efter lite kverulerande så blev det
att han skulle söka in till Finns Sommarteater. Blev det en roll där, kunde
(eller "behövde") han ju inte vara på musikläger. I bästa fall var de ju
samtidigt.

Så han sökte. Och jag meddelade att eftersom jag ändå skulle komma att köra
honom till övningarna så kunde jag gärna hjälpa till på ett hörn. Bära lådor
eller något. Till vilket producenten svarade att skulle jag kanske vara
intresserad av en större roll oxå? Nå, på scen, bakom scen, inte precis vad jag
tänkt, men vad har jag att förlora -- så efter att jag konsulterat med min son,
sökte även jag till auditionen.

Dagarna gick och valet kändes allt bättre. Visst skulle det bli kusligt att
stå på scen, vara synlig, pajasa inför publik. En stor mental tröskel, och jag
skulle över den, eller åtminstone försöka. Att försöka kändes ren som halva vinsten.

Lång story kort så fick jag rollen. Som Efraim Långstrump. Vilket var precis
vad jag önskade. Och så det pirrar!

Det här är nu ett par veckor sedan. Vi har haft ett par övningar redan och jag
tänkte att som god kapten borde jag ju hålla loggbok för mina upplevelser. Och
här är den.

Kalla mej Efraim.
