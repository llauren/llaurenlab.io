---
title: "Mannen som sålde sitt skägg"
type: post
date: 2020-06-24T00:10:34+03:00
tags:
  - film
  - skägg
  - Den svavelgula himlen
draft: false
---

Idag fick jag ett samtal. Damen på andra sidan ville veta om jag fortfarande var
intresserad att hjälpa till som statist i en film. Visst, sade jag, vad behöver
jag veta?

Min karaktär är journalist och jag har två (!) repliker. Och så undrar hon om
jag kan skicka en ny bild.

En halvtimme senare kommer ett nytt samtal. Tidseran för filmen är nittiotalet
och regissören säjer att skägget ska bort. Sånt hade man inte på den tiden. Jag
föreslog att kanske ett stubbigt Miami Vice-skägg efter att journalisten jobbat
hela natten, men det dög då inte. Så jag beslöt att inte vara besvärlig utan
offra mitt eleganta hakludd för konsten och regissörens konstnärliga vision (och
ett maffigt tjugo euros gage).

Så nu hoppas jag att det inte slutar med att min scen oxå klipps bort i ivern,
för då blir jag nog ändå lite upprörd.
