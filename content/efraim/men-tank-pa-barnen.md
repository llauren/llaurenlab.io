---
title: "Men tänk på barnen"
type: post
date: 2019-10-22T18:58:56+03:00
draft: false
tags:
  - Robin Hood
  - Fallåker

---

Man kan säja mycket om Sheriffen av Nottingham, men klok är han inte. Sheriffen
är inte en glad eller förnuftig man. Han flippar vanligen mellan förnedrad och
förnedrande, och i scenen då han äntligen fått Robin Hood i klorna är han rent
vansinnig.

Det är kanske oxå skäl att påminna att Robin Hood inte är en barnpjäs. Vi har
rekommenderat en åldersgräns på sju, men stundvis kan det tyckas att tolv är en
mer realistisk rekommendation. Sitt åtminstone inte på de första två raderna om
du är eller har känsliga personer med dej (och största delen av pjäsen är ju
faktiskt lite snällare än de värsta delarna :) .

Vi hade alltså övning på sång- och dansnumret då Sheriffen gottar sej över att
Robin snart ska bli mos, och det är en i den grad fysisk prestation att
piratdansen där Efraim snurrar omkring på Pippi känns ganska beige. Kanske jag
borde börja cykla igen? Å andra sidan känns det lite roligt att verkligen spela
ut att vara en genomrutten person — jag har fått kritik av mina medskådespelare
att jag nog borde jobba på att inte alltid se så snäll ut!

Idag känns rören lite rensade och det är som om jag hade mer utrymme i
bröstkorgen. Fötterna har ännu lite svårt att hitta sina platser i de snabba
partierna och halsen är kanske lite sliten. På dagens träning ska vi köra slutet
av första akten och, i bästa fall början av andra, utantill och utan manus i
hand, så utmaningar har vi ännu gott om. Blir visst att träna replikerna på
tågvägen till Fallåker igen.
