---
title: "Mening och tomma ord"
type: post
date: 2019-12-06T11:33:39+02:00
tags:
  - repliker
  - Robin Hood
  - Fallåker
---

Det här med att spela skådespel är inte lätt för mej. Eller mer specifikt, jag
har inte så svårt att improvisera eller pajasa mej då när det inte är press, men
att lära sej saker utantill och sen upprepa dem under press ... det är en
utmaning. Åtminstone för mej. Därför är jag ganska tacksam att jag har en ändå
rätt så liten roll och egentligen bara en koreografi.

Sånger är det lite lättare med, och jag tror att det är en del av förklaringen.
Repliker som text i ett manus eller nåt man bara säjer i dialog med andra saknar
liksom mening. De bara är. Visst, de för historien framåt, men de är liksom ändå
bara text. Det var när jag märkte _hur_ jag skulle säja en del repliker som
påminde om varandra och _hur_ jag betedde mej från en ganska lika replik till en
annan som texterna fick en mening, en orsak, ett sammanhang.

**Prins Johan** Vi är inte populära vi.

**Sheriffen** Nej, inte vi

**Prins Johan** Vaddå inte vi?

**Sheriffen** De betalar gärna skatt när de tror att pengarna går till Richards
lösensumma

**Prins Johan** Men de gör de inte

**Sheriffen** Nej, nej, men de tror det

**Prins Johan** Hur kan de tro nåt sånt (...)

**Sheriffen** Ja, jag råkade säja det av misstag och så blev de genast mycket
medgörligare

Det här är egentligen bara en massa tjat fram och tillbaka, men när jag tolkade
det som så att sheriffen först är lite borta i sina egna tankar (och imponerad
av sin egen finurlighet) ända tills han vaknar upp av att Prins Johan kritiserar
honom och hans idé, då får dialogen en dynamik. De första raderna säjer
Sheriffen "inåt" och mer till sej själv, den sista mer i försvar och till
prinsen.

Flera och olikare[^olikare] sinnesintryck ger en starkare upplevelse och mitt i
allt har dialogen en _mening_ förutom att bara vara ordväxling. För mej
åtminstone. Andra kan ha det lättare.

När det gäller koreografi är det lite det motsatta. Stegväxling är mej så
obekant att jag behöver lära mej det steg för steg. Höger - hopp - vänster -
hopp - höger - vänster-halv - vänster - höger-halv - höger - vänster-halv -
vänster-bak - höger-fram - vänster-bak och hur i alla mina dar kan det vara
sådär?! Så _småningom_ blir det små helheter där fyra höger-vänster-halv-full
blir en liten fras och sakta, sakta börjar de få en plats i själva sången. Men
inte är det mej enkelt fast jag skulle önska det. Ja, är det nåt jag skulle
vilja lära mej lättare är det faktiskt dans. Jag dansar som en panda med två
vänsterben. Men principen är fortfarande densamma. Satta i sammanhang blir
repliker meningsfulla, även om replikerna ibland är steg.

[^olikare]: med ursäkt till min modersmålslärare och andra språkvetare; jag vet
  att det inte heter olika, olikare, olikast, men det _kunde_ göra det.
