---
title: "Midsommarpaus och hjärntrassel"
type: post
date: 2022-06-25T12:03:54+03:00
featured_image: /img/PXL_20220624_210103650.jpg
image_name: Midsommarnatt på landet
image_by: Robin Laurén
omit_header_text: true
tags:
  - Finns
  - paus
---

Finns håller nu paus för midsommar. Till skillnad från [förra gången vi hade
paus](/efraim/ingenting) måste jag säja att det känns skönt. Tolv
föreställningar på fjorton dagar blev ganska mycket.

Just nu känns det som att huvudet är fullt av trassel, och att gå ut med hunden
på landet och bara lyssna på skogen i stället för musik eller podcastar medan
jag går gör att trasslet sakta trasslar upp sej. Det har varit en intensiv
period de senaste månaderna och trots att det varit roligt känns det bra att
inte ha något jag har lovat delta i några dagar framöver. Skulle det inte vara
för min tå som jag smällde i en tröskel en vecka sen och som fortfarande gör det
svårt att gå, skulle det vara riktigt skönt. Och jag vet att det kommer att bli
härligt att se gänget igen och spela tillsammans efter det lååååånga
veckoslutet, bara jag får ta det lugnt ännu ett tag.

**Har du inte sett oss än -- eller vill du se oss igen -- börjar det bli dags
att [skaffa biljetter](https://www.finnssommarteater.fi/sv/biljetter/)!**

Efter midsommarpausen har vi faktiskt bara fem föreställningar kvar, vilket
låter lite konstigt när jag skriver det. Å andra sidan har jag skrivit om det
[förr](/efraim/artikel-till-ebuf/) så jag borde ju inte vara överraskad. Inte
för att jag kan påverka kalendern, men det vore klart mera morjens om man hade
färre föreställningar före midsommar och flera efter. Att få suga på karamellen,
liksom[^kula]. Och att inte ha så många föreställningar i rad (3+2
föreställningar i veckan med tex måndag och fredag lediga?). Midsommaren ser
ändå ut att infalla i stort sett samma veckoslut år 2023 som nu, så det kanske
inte blir så stora förändringar i Finns spelschema till nästa period. Och det är
nog så skönt att ha hela juli ledig och då kan man ju besöka andra
amatörteatrar!

När det lugnat sej i hjärnan ska jag skriva nåt om hur det är att jobba
tillsammans på Finns sommarteater. Det är nämligen ganska fint.

[^kula]: Eller den Gröna kulan, om man är så lagd
