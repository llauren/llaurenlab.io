---
title: "Nog repeterat"
date: 2019-06-05T21:03:22+03:00
tags:
  - Pippi Långstrup
  - Finns
  - repetition

---

Nu har vi tränat, övat, repeterat och generalrepeterat. Vi har sjungit, dansat,
nötit in repliker, skrattat, fnattat, tappat bort oss och hittat oss igen. Nu är
det klart och imorgon är det premiär. Det känns spännande, det känns pirrigt och
det känns bra. Det känns som om det är dags.

Vi hade genrep[^genrep] idag och igår (eftersom vi har två Pippin) och frånsett
några små missar, som jag hoppas publiken inte fäste så stor uppmärksamhet vid,
gick det faktiskt så bra att vi nästan kunde ha spelat inför betalande publik!

Jo, jag kom av mej en gång igår, men Pippi räddade räddade situationen, och idag
kom Pippi av sej på nästan samma ställe där jag i stället hade tillräckligt med
sinnesnärvaro att fylla i. Sånt där smått som egentligen inte betyder något för
själva skådespelet. Ibland har det improviserats repliker och sångtexter, men
showen gick vidare och i det stora hela gick det alltså bra. Nu är det bara att
samla ännu lite mera mod och ta ut svängarna ännu lite mer vågat.

Tänker man tillbaka på hur det såg ut bara en vecka sen, så kändes det mesta
lite osäkert. Tänker man tillbaka till vårvintern så är det ett rent mirakel att
vi fått nåt sånt här alls! Men det har inte kommit av sej själv. Det har kommit
genom jobb, repetition och mera jobb. Och det har varit roligt.

Och nu är det bara att välkomna er, kära publik! Vi har jobbat oss svettiga och
nu vill vi gärna dela med oss.
[Välkomna på sommarteater!](http://finnssommarteater.nsu.webbhuset.fi/svenska/arets_pjas/biljetter/)

[^genrep]: teaterspråk för generalrepetition och betyder sista övningen före premiär
