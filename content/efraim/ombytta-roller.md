---
title: "Ombytta Roller"
type: post
date: 2020-03-01T19:11:25+02:00
draft: false
tags:
  - Robin Hood
  - Saltkråkan
  - Finns
  - Fallåker
---

Jag står mitt emellan två kapitel just nu. Eller mer specifikt, mitt emellan två
skådespel.

Det är ett veckoslut kvar av Robin Hood på Fallåker. Nästa lördag spelar vi
dubbelfinal och sen lägger Nottingwood lapp på luckan. Det är samtidigt både
skönt och lite dystert, men så ska det väl vara. Åtminstone vet jag att
teatrandet inte tar slut bara för att teatern gör det.

Saltkråkan-övningarna för nästa års Finns-produktion är ren i full gång. En del
har jag missat eftersom vi har haft övningar på veckosluten, då det har varit
Robin Hood på Fallåker, men efter inkommande veckoslut blir det lättare att
koncentrera sej på bara en sak. Eller bara ett skådespel då. Jag har en
presentation att hålla i London tredje veckan i mars och till slutet av aprill
borde jag ha ett band sammankrafsat för konsert lite före valborg, och så är det
födelsedagsjubileumspartaj i början av maj. Och i juni är det premiär på
Saltkråkan. Så att man får väl bara vara glad om det blir lugnt sommarlov i år.
