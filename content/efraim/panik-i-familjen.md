---
title: "Det stannar på kliniken, eller Panik i familjen"
type: post
date: 2022-11-28T15:53:08+02:00
featured_image: "/img/paniken-stannar-till.jpeg"
image_name: "Tyst på scenen"
image_by: "Frank Skog"
tags:
  - Fallåker
  - Paniken
---

OK, det här är lite fånigt. Men vi vick tydligen ett barskt samtal från
innehavarna till rättigheterna av pjäsen vi spelar. Av upprättshovliga skäl bör
omedelbart sluta spela vår fars ... under namnet _Panik på kliniken_, eftersom
det namnet syftar på en annan översättning av samma pjäs! Så om jag i framtiden
talar om _Det stannar i familjen_ så är det alltså samma fars det gäller. Och
med precis lika mycket panik.
<!--more-->

Nu hade det ju förstås varit roligare, sådär rent logistiskt, om vi hade fått
veta om detta lite före vi började spela. Eller kanske rentav, lite före vi
började marknadsföra pjäsen som _Panik på kliniken_ (numera _Det stannar i
familjen_). Så lite knäckt blir man ju alltid.

Eller kanske man bara borde börja spela enligt den andra översättningen.
