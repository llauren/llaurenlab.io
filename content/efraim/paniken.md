---
title: "Paniken på kliniken!"
type: post
date: 2022-11-25T16:36:21+02:00
featured_image: "/img/paniken-crop.jpeg"
image_name: "Fallåker panikensemble"
image_by: "Tipi Olander"
tags:
  - Fallåker
  - "Panik på kliniken"
  - Paniken
---

Ja det är ju grymt hur länge sen det är sen jag skrev här senast ... och i kväll
är det premiär! Igen!

Javisst! Fallåker teater smäller i kväll igång spelperioden av **Panik på
kliniken**, en fars i läkarmiljö, ursprungligen _"It runs in the family"_ av Ray
Cooney, regisserad av Frank Skog (från, ni minns, [Stulen kärlek](/tags/stulen+kärlek))!
Yay! Nytt för i år är Robert och Ellen, stulna från Finns sommarteater -- och
vilka kap de är! Ellen gör den knäckta tonårstjejen med perfekt neuros och
Robert har ensemblens snyggaste ben. Ni får se.

Föga överraskande spelar jag åter utan byxor, denna gång en äldre herreman i
rullstol. Rollen är igen lite annorlunda än förr, inte minst för att mitt
uttryckssätt är mer kinetiskt än verbalt. Mest knäcker jag ur mej små
ovidkommande kommentarer, men det att spela med rörelser, och då inte
handrörelser utan hur jag guppar med rullstolen, det är nåt nytt för mej. Om ni
inte ser nåt konstigt med det så har jag fattat galoppen.

Som namnet säjer så är det en fars i full rulle. Doctor Mortimore är stressad.
Han ska alldeles just hålla årets stora föredrag på St Andrews -kliniken. Övriga
personalen vimlar omkring och en del jobbar med att få ihop julspexet. Dr Drake
kräver att få se talet och ändringarna han vill ha med. Och så dyker Mortimors
gamla flamma upp med en oväntad överraskning hon egentligen borde ha talat om
för 18 år sedan... nåt som Mortimors fru absolut inte får veta om. Eller
polisen, som väntar i aulan. Eller, nåja, praktiskt sett alla på St Andrews
sjukhus. Det är lite rörigt i början, men det blir inte mindre rörigt då pjäsen
fortskrider. Åtminstone inte för Dr Mortimore.

Biljetter och info på [Fallåkers webbsidor](https://www.fallaker.fi/new/services/panik-pa-kliniken/). Åldersrekommendation 14 och uppåt. Välkomna!
