---
title: "Pippi återvänder till Villa Villekulla"
date: 2019-05-31T00:23:42+03:00
tags:
  - Pippi Långstrup
draft: false
---

Pippi-bokserien (och skådespelet, spoiler spoiler) börjar med att Pippi flyttar
in i Villa Villekulla. Och man får intrycket av att hon är på ett nytt ställe.
Men tror man det så har man fel, för Pippi har varit där förr. Tror jag.

Jo, varning för spekulationer och obefogade teorier.

Skådespelet börjar med att Pippi kommer gående med en karta (sorry, mera
spoilers), eller åtminstone nån form av lapp, med instruktioner hur man ska
hitta till Villa Villekulla. Hur kan Pippi ha en sån? Jo, enligt böckerna har
nämligen hennes pappa Efraim i tiderna köpt huset så att han ska ha nåt ställe
att bo i när han tröttnat på att segla. Och eftersom Efraim tycker så väldigt
mycket om sin dotter har han naturligtvis haft henne med när han skaffat huset.
Vilket alltså betyder att _Pippi varit i Villa Villekulla förr_ och att hon
alltså kommer _tillbaka_ till Villa Villekulla!

Poof! Mind blown.

Och hur vet vi att det är sant? Jo, Pippi har ju en massa grejer på vinden som
tillhör hennes släkt. Det här kommer fram i en senare bok, men inte alls i
skådespelet (spoiler?).

Men inte nog med det. Varken familjen Settergren (de med Tommy och Annika),
poliserna, eller fru Prysselius känner till vare sej Pippi eler hennes far,
alltså har hon varit där _före_ dem! Pow! Pippi är alltså "The Original
Gangster" och alla de övriga byborna har faktiskt flyttat in efteråt!

OK, det där var kanske lite att ta i, men nog skulle man ju tycka _någon_ i
stan, åtminstone nån relaterad polisen, skulle ha nåt hum om vem som äger det
gamla kåken (eller kråkslottet, hur man nu vill se på saken) där unga fröken
Långstrump flyttar in. Eller stans självutnämnde moralväktare, fru Prysselius
(även "fru Prüzelius", fast hon finns ju inte i boken).

Staden där Pippi bosätter sej har förresten inget namn i boken.

Nu finns det en vidareutvecklad teori[^jonna] att det där inte helt skulle
stämma, utan att Villa Villekulla de facto är Pippis mammas släktvilla. Mamman
hade till släktens stora fasa träffat och förälskat sej i den rufsige men
varmhjärtade piraten Efraim Långstrump. Hon hade varit ute och seglat med Efraim
och hans besättning ett tag men sen hade de flyttat in till Villa Villekulla när
hon skulle ha barn, och där bodde de då när Pippi var riktigt liten. Det att
Efraim målat om villan enligt bästa förmåga (dvs inte alldeles bra) bidrog nog
till att ingen av Pippis släktingar på mammans sida riktigt ville komma och
träffa familjen Långstrump, vilket förstås är väldigt småaktigt.

Idag kom det ännu en ny teori som elegant kullkastar de två tidigare[^oskar],
nämligen att Efraim Långstrump skulle ha köpt Villa Villekulla på nätet, eller
via en bulvan, mäklare, på en bazaar i mörkaste mellanöstern, eller av nån annan
sorts skojare, utan att ha sett huset först. Det skulle förklara hur Pippi kunde
ha ett hus som hon aldrig sett förr att flytta in till, och att ingen i staden
hade vare sej sett eller hört om Pippi Långstrump. Hur det kan finnas
släktklenoder på vinden förklaras inte av denna teori, men det är ju flera
böcker mellan att Pippi flyttar in och att vännerna besöker vinden, så de kan ju
ha dykt in däremellan.

Mamman saknar för övrigt oxå namn i historien, men det är ganska säkert att
Pippi har fått åtminstone ett namn av sin mor. Det är bara att spekulera
huruvida det är Pippilotta, Viktualia, Rullgardina eller Krusmynta hon fått
efter sin mor i namn. Min gissning är Viktualia, eftersom det låter så fint. Det
finns förresten precis sex personer i Sverige med [förnamnet Viktualia](https://svenskanamn.alltforforaldrar.se/visa/Viktualia), vilket
gör det extra spännande att Viktualia faktiskt har en egen namnsdag i vårt
västra grannland den 11 mars. I jämförelse finns det 66 personer med förnamnet
Krusmynta, och jo, Krusmynta ha oxå en svensk namnsdag, den 19 oktober.

Och så finns det 9 personer med förnamnet Rullgardina. Namnsdag på julafton,
vilken hon delar med 1096 andra namn. De är ju älskvärda de där svenskarna! Och
så finns det två Efraimsdöttrar. Bägge har namnsdag den 29 januari. Så nu vet ni
det.

Nu förtäljer inte historien när, var, hur och varför Pippis mamma dog, men
senast efter det måste nog Efraim och Pippi ha dragit ut till sjöss igen. Inte
kan han ha haft lust att sitta på torran land i ett hus utan sin hustru.
Eventuellt kan han ha blivit så hjärtbrusten att han tog sin dotter under armen
och drog till havs av blotta knäckelsen.

Boken, den första alltså, berättar ju nog om att Pippi varit riktigt ung på
skeppet Hoppetossa oxå, fast utan bok bredvid mej minns jag inte riktigt vad där
stod. Kommer med flera teorier när jag tittat närmare i den!

[^jonna]: Tack till Jonna för teorin!

[^oskar]: Tack till Oskar för teorin!
