---
title: "Premiär, krasch, och upp igen!"
date: 2019-06-08T11:29:05+03:00
tags:
  - Pippi Långstrup
  - Finns
  - premiär
draft: false
---

<blockquote>NB: Jo, jag skrev det här igår och jo, vi spelar idag igen!</blockquote>

Igår var det Pippi Långstrump-premiär och jag lever i ett rus av lycka och
adrenalinkrasch. Bra att vi har ledigt från teatern idag, fast det känns lite
konstigt att inte ha gänget runt sej. Det att man inte behöver skärpa eller
hända är samtidigt konstigt och ändå lite skönt.

Premiären då? Jo, några missar blev det väl, men ändå var det nog en glädje och
lycka att spela! Det var varmt, det gick bra, publiken verkade nöjd och den där
känslan av att nu har vi gjort nåt, och vi har gjort det tillsammans, var nåt
man behövde uppleva och bara leva av!

Efter premiären var det kalas (ingen lade nyllet i tårtan) och så väl yngre som
äldre deltagare körde på tills sena timmen (och vissa till tidiga timmarna,
hörde jag). För det var vi nog värda 😁.

Nu ska det bli teater juni igenom. Det kommer naturligtvis att bli lite
annorlunda, men hur, det for tiden visa. Jag hoppas vi blir (ännu) proffsigare
och säkrare, men mest av allt hoppas jag att vi behåller vår glädje och att den
smittar av sej på vår publik! Vi har trots allt ännu minst 1500 personer att
spela för! Eller 4500!
