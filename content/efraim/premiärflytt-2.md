---
title: "Premiärflytt 2"
type: post
date: 2021-01-28T16:32:19+02:00
tags:
  - Fallåker
  - Stulen kärlek
  - corona
---

Nu har det blivit klart att vi flyttar på Stulen Kärlek-premiären till hösten.
Jädra corona alltså.

Vi kommer att ha en liten privatvisning för Fallåkers styrelse i februari, så
att vi nu har nåt att jobba _för_, men annars så kommer vi att försöka hålla
skådespelet "vid liv" tills dess med en eller två genomgångar i månaden tills
dess.

Blev du nu oerhört jätteledsen kan du alltid höra av dej, så kanske vi bjuder
in dej som övningspublik till en genomgång. Men annars är det att snällt vänta
på att alla är vaccinerade och/eller att den här eländiga pandemin blåst över.

Jädra corona, alltså.
