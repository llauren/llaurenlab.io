---
title: "Premiärflytt och övningsklipp"
type: post
date: 2020-12-15T21:24:02+02:00
tags:
  - Fallåker
  - Stulen kärlek
  - corona
  - video
---

Stulen kärlek kommer inte att ha premiär i slutet av januari 2021. När exakt
premiären blir är inte fastslaget, men vi flyttar enligt rådande plan premiären
ungefär en månad framåt. Det betyder (kanske) att de som bokat biljett för sista
föreställningen kan komma och titta på premiären i stället. Men vi lever med
läget och informerar därom. Och hur det slutligen blir får vi se när tiden är
mogen.

Under [coronapausen](/efraim/coronapaus/) klippte jag ihop våra övningsvideon
och jag kan ju skriva lite om den processen och vad misstagen kan oss lära.

Vi spelade in vår övning i två tagningar, akt 1 och akt 2 med kamerorna rullande
genom hela respektive akt. Vi hade tre kameror och två inspelande
Zoom-mikrofoner. Kamerorna placerade vi så att hela scenen var "täckt", dvs
vänster kamera spelade in höger halva av scenen, mittenkameran spelade in mitten
av scenen och höger kamera spelade in vänster halva. Dessutom hade vi lite
action på vänster sida av scen som inte spelades in av nån kamera alls (visade
det sej, men det är ju lätt att vara efterklok). I praktiken betydde det att
mittenkameran var helt för "utzoomad" och sidokamerornas vinklar vanligtvis
åtminstone lite fel. För så går det om ingen vänder på kameran under spelets
gång.

Mikrofonerna placerade vi så att en mick var rätt långt framme på scen och en
rätt långt bak. Det betydde att ljudet _ofta_ var begripligt, men ganska ojämnt.
Men som referensmaterial fick det duga. Bakmicken spelade in i fyra kanaler,
främre micken bara i tvåkanalers stereo, vilket betydde att allt prat mellan den
och scenkanten blev ganska tyst.

Det positivaste med att spela in med flera kameror och mikrofoner simultant är
att det enkelt ("enkelt") går att göra en multikamerafilm av det hela. Med
DaVinci Resolve går det att med ett litet antal ingrepp låta programmet
synkronisera filminspelningarna och ljuden så att det är smärtfritt att klippa
mellan kameravinklarna. Vad jag lärde mej i efterhand var att man först borde ha
fixat vitbalansen och justerat ljuset och först sen klippt. I brist på
kompetens, så fick jag sen fixa bilden på trehundra klipp. Antagligen kunde det
ha gjorts lättare, men jag får lära mej en annan gång.

När det var klart, orkade jag inte klippa bort de ställen vi tappade bort oss,
fick ta till suffleringshjälp eller spontant behövde ta en regisseringspaus.

Ljudet var det lättare med. Jag justerade nivåerna precis en gång, körde dem
kompressor och ekvalisator och så var det klart. Resolve mixade de sex kanalerna
till två och konstigare än så var det inte.

Jag lade titlar vid scenbytena, och hade jag riktigt orkat, så hade jag kopierat
in manuset som undertexter, men det hade tagit så lång tid att jag antagligen
fortfarande jobbat på det. Så det fick vara.

Eftersom kameravinklarna var vad de var, så lekte jag ganska friskt med att
zooma bilden i efterhand. Ack om man hade haft 4k-kameror i stället för
HD-kameror så hade resolutionen inte lidit! Några panoreringar knepade jag oxå
ihop i efterproduktionen. Det visade sej snabbt att rörlig kamera (fast kameran
alltså inte rörde på sej, det var alltså det som magin var) ser betydligt mer
levande (och proffsigt) ut än statiska kameror.

Jag kan ingalunda säja att jag nu är en kompetent videoklippare, men en hel del
övning fick jag ändå. Detsamma kan inte säjas om mina repliker.  Vi hade övning
nu på söndagen och det är obehagligt hur mycket man kan glömma under en så kort
paus.

Vad skulle jag göra för en framtida inspelning? Bemannade kameror. Inte
nödvändigtvis alla, men åtminstone en eller två. Och kameraoperatörerna ska ha
sett skådespelet så de vet vad de ska filma. Minst fyra kameror och alla i
4k-resolution. Och så ska man vara medveten att en systemkamera köpt i EU spelar
in exakt 30 minuter film och slår sen av strömmen. Så sen sitter man där med två
kameror. Och sen en. Det är lite trist. Flera mikrofoner, lite strategiskt
placerade runt scenen, och ännu lite mer ljus. Ja, och kompetenta skådespelare
förstås. Men det fixar vi till premiären :)

Det blir en övning till nu på torsdag och så håller vi håller jullov och kan
glömma resten av replikerna.
