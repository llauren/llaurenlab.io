---
title: "Premiärveckoslut"
type: post
date: 2019-11-24T20:31:06+02:00
draft: false
tags:
  - Robin Hood
  - premiär
  - Fallåker
---

Det är uppenbarligen få saker längre än ett premiärveckoslut. Egentligen började
det i början av veckan med två genrep och konsert med Devin Townsend :). Men
premiär var det på fredag och efter det premiärkalas! Lördag var ledig dag --
vilket var oerhört skönt -- och söndag var det andra föreställningen. Och
faktiskt, så gick allt riktigt bra!

Jo, några små missar här och där, men inget som skulle ha fällt föreställningen.
Jag kan berätta om ni är intresserade, men som sagt, ingenting seriöst.

Så hur kändes det med premiären? Bra. Inte alls lika nervöst som Pippi-premiären
på Finns Sommarteater förra sommaren. Antingen för att detta inte var min första
premiär untan min andra (berättar han med djup röst av solid erfarenhet) eller
för att vi tränat tillräckligt. Visst hade jag och många andra sina manus
backstage och kollade vad som skulle hända härnäst, men med de och varandra som
stöd -- och en hel dos adrenalin -- fixade vi oss igenom våra första två
föreställningar med hedern och självkänslan i behåll. Jag kan inte säja att jag
är helt säker på alla mina repliker, men då vi är live så bubblar i regel de
rätta raderna ur mej ändå. Men inte att jag här nu kunde säja i vilken ordning
de ska komma. Det kommer väl med tiden ännu.

Jag satt i bastun mellan spelningarna och reflekterade lite på vad jag sysslar
med, och hur. Det finns ett begrepp inom teater, film och teve som kallas
<i>[Den fjärde
väggen](https://sv.wikipedia.org/wiki/Fj%C3%A4rde_v%C3%A4ggen).</i> Principen är
alltså den att mellan de på scen, film eller teveruta och publiken finns en
osynlig "fjärde vägg" som de på den andra sidan är omedvetna om och som publiken
kan titta in igenom. Ibland, till exempel då man av misstag eller helt medvetet
tittar in i kameran bryter man den Fjärde väggen och gör tittaren medveten om
att det är ett spel som pågår på andra sidan. När det gäller teater, är det ändå
tillåtet, rentav önskvärt, att ta ögonkontakt med publiken emellanåt. Och jag
insåg att jag är så koncentrerad på vad jag gör på scen, att jag spelar inåt,
bakom den Fjärde väggen, och medvetet stirrar på mina medskådespelare i stället
för att våga blicka ut. Kanske oxå nåt jag lär mej med tiden. Speciellt de
stunder jag står och funderar ut i luften samt låten _Be din sista bön_ skulle
antagligen må bra av lite mer publikkontakt.

Eller så är jag färgad av min bakgrund av teknologspex, där det är _meningen_
att skådespelarna ska ha publikkontakt och, i ännu högre grad, att publiken ska
ha skådespelarkontakt. Men spex är en annan form av teater.

Men nu är korken ur flaskan (bildligt talat) och spelsäsongen är igång. Nästa
gång vi träffas är först om nästan en vecka, vilket känns jättekonstigt med
tanke på hur mycket vi jobbat och levt med varandra de sista tre månaderna.
Ingen aning vad jag ska göra under den tiden :)

Halva ensemblen har varit förkyld under slutet av träningsperioden. Det känns
lite som om min förkylning kommer nu.
