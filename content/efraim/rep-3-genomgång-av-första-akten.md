---
title: "Rep 3: Genomgång av första akten"
date: 2019-02-03T19:55:10+02:00
tags:
  - Pippi Långstrup
  - Finns
  - repetition
---

Idag hade vi en enklare repetition, nämligen genomgång av första akten. Det
betyder i praktiken att vi läste igenom våra repliker med manus i hand, gick
omkring på golvet som representerade scenen, och funderade på hur vi skulle få
de ganska föråldrade och stundvis väldigt _svenska_ replikerna lite mer
finlansvenska. Meningen är alltså att vår Pippi ska vara lite mer i vår tid, om
ändå lite obestämt så, och klart mer lokal än värmländsk. Eller egentligen är
väl tanken att vi ska lokalisera och avdamma pjäsen just så mycket att det inte
märks.

I det stora hela gick det bra tycker jag, och åtminstone jag fick känslan av att
det här fixar vi.

Vädret däremot var alldeles förfärligt och man får ju hoppas att det blir lite
mindre snö till premiären.

<!-- photos through publicalbum.org -->
<script src="https://cdn.jsdelivr.net/npm/publicalbum@latest/dist/pa-embed-player.min.js" async></script>
<div class="pa-embed-player" style="width:100%; height:480px; display:none;"
  data-link="https://photos.app.goo.gl/2EnJx24i7vzY7bU99"
  data-title="3 new photos by Robin Laurén">
  <img data-src="https://lh3.googleusercontent.com/t1YDSTpUPuoNo7Ty30rDF6gcOFv5JBGOcrvz8cspDu9zD8FM6olEqc022Bw9ZWD1dIMLov779IouA9dE6s0rSusErE0LmAz_j-0QNpEXfgFZv7FUU8a96VlVrknW6Z0W-p92NyHQnjs=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/L_ILK0JVEEeM0-4jLEoDJIg3Q0VemtTUpgtry6IXP3Wbn0Zd9A18dqNi6eJMtd2m-3lHyRn0ngyKE14XY1BFIXpga9zFP55Nbr-LRCQvbm5OnkJFvnQlOhi3I3qw1m007-01sAxvAQ4=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/Ab8mfDMQHeZZ4WRQAN_XPD_3s7NEIDiEii8CxBuKF5ie4J-pgm8NOs1sooUAgBDJ8MHOhCrH7FUyxjudzMZ8Ho3uj8DbU7EmOFWNANsIj-bnAM7PSv_pZeQ31uSIj2Cu2C7BfNbPyww=w1920-h1080" src="" alt="" />
</div>
