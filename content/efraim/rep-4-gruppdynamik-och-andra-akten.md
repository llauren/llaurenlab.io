---
title: "Rep 4 - Gruppdynamik och andra akten"
date: 2019-02-10T12:25:13+02:00
tags:
  - Pippi Långstrup
  - Finns
  - repetition
---

{% instagram BtqnNVRgOGQ %}

Det är bara det fjärde repet[^rep] och jag kan tydligt märka en skillnad i
gruppdynamiken. Folk är mera direkta och bekanta med varandra, det är mera jox
och fånigheter och gruppen börjar bete sej som … en grupp! Det känns lite som
något _vi_ ska göra och det är bra.

Idag gick vi igenom andra akten. Lite slattrigt var det och stundvis rådde smått
rörig pippistämning i leden. På gott och ont. Men vi hann trots allt igenom
andra akten.

Det här var första gången jag hade repliker på scen. Sist och slutligen är det
egentligen väldigt få repliker Kapten Långstrump har, med tanke på hur central
figur han är för själva handlingen. Jag menar, en av de bärande grejerna i
storyn är att Pippi hänger sin verklighet på att hennes pappa överhuvudtaget
existerar, och att han ska komma tillbaka en dag och hämta henne. Tommy och
Annika _(“Tommyka”)_ väljer att tro på att Pippi har en pappa men att han
antagligen drunknat eller blivit uppäten eller strandat och nog aldrig kommer
att dyka in. Och de vuxna i historien är fast bestämda att Pippi pratar strunt
och behöver bli omhändertagen av Systemet. Undra på att hon inte passar in. Fast
nu kom jag av mej i analyser igen. Anyways, kapten Efraim Långstrump har inte så
mycket att säja men han ska göra det starkt. Och så ska han bära sin dotter på
axeln oxå, utan att grimasera. Får visst sätta igång att träna muskelstyrkan!

{% instagram BttV_kphksj %}

[^rep]: Rep är teaterspråk för _repetition_ ... eller egentligen vilken sorts
    övning som helst. Man kan bra ha dansrepetition eller sångrepetition fast
    det skulle vara första gången man repeterade dans eller sång. Så rep.
