---
title: "Robin av Locksley"
type: post
date: 2019-09-10T21:43:30+03:00
tags:
  - Robin Hood
  - Fallåker
  - repetition
  - regissör
---

Hårda bud i kungariket. Vi har redan haft en handfull övningar och jag har inte
skrivit en enda grej om hur det är och går!

Det är ett ofantligt skojigt gäng som sätter upp Robin Hood! Vår regissör Mika
_("Miika, vi är alla olika")_ är en bullrigt skrarttande filur, energisk, full
av idéer. Make hanterar rollen som Robin med snille och smak. Den elaka trojkan
Prins Johan - Guy av Gisbourne - Sheriffen av Nottingham (jag!) är alla på samma
nivå av vansinnighet. Lille John är stor, Lady Marion underbar, Röda Jill har
attityd. Ni, kära publik, kommer att gilla det här.

Idag gick vi igenom scenerna två och tre. Det betyder att vi först läste igenom
replikerna i allsköns lugn, funderade på vilka ord som ska ändras, vilka som ska
understrykas och vilka som bara strykas. Och så har vi mysteriet med var Allan
är. Efter det här tar vi till scen, vi kollar var vi ska stå och gå, hur vi ska
reagera och förhålla oss till varandra, och så upprepar vi det tills det känns
bra. En mycket iterativ process, och trots att vi inte tar oss framåt i rasande
fart, känns det som det vi gör är på riktigt.

Men det är inte bara repetitioner vi haft. Faktiskt tror jag det varit fler
dans- och sångövningar. Koreografierna är milt sagt ambitiösa, åtminstone för
den dansande panda jag är. Lyckligtvis har jag inte många, och lyckligtvis finns
det andra som är bättre :). Sångerna är medryckande, men det vet väl de flesta
redan. Det är alltså partituret av Sås och Kopp, och vi kör dem, så gott vi kan,
i stämmor.

Det blir rätt så tätt med övningar. Det är nån form av rep eller övning fyra
till fem gånger i veckan. Alla övningar gäller fårstås inte alla i ensemblen,
men vi ses nog så ofta, och därför är det väl bra att vi trivs tillsammans. Och
mer lär det nog bli. Jag borde inte måla permiärfan på väggen, men det är 73
dagar till premiär. 72 om vi räknar med att den här dagen redan börjar vara
slut. Men vi ser väl dej i publiken sen?
