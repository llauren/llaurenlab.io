---
title: "Konsten att se det uppenbara"
type: post
date: 2020-10-07T20:27:38+03:00
draft: false
categories:
  - Stulen kärlek
  - regissör
---

Vi har just en onsdagsövning med folk som vimsar fram och tillbaka och så har en
ny grej kommit med i form av en väska (ni får se sen). Frågan är vad som ska
hända med väskan för att det ska verka både naturligt och kul, samtidigt.

Strålande idéer sprudlade i fors och kors ur skådespelarna tills Regissören
morrade till att _"Nu är här för många idéer."_ Så äntrade han scenen med säkra
steg, regisserade skådespelarna till sina platser, gick scenen fram och tillbaka
sekund för sekund, placerade väskan och koreograferade den som bär den och mitt
i allt var det alldeles som det skulle vara. Naturligtvis skulle det vara såhär.

Och det var då jag insåg att en regissörs största uppgift och gåva till världen
är att se -- och förmedla -- det uppenbara, när ingen annan kan det.
