---
title: "Sheriffen Av Nottingham"
date: 2019-07-20T15:59:22+03:00
lastmod: 2019-07-21T17:46:08+03:00
tags:
  - sheriffen
  - Nottingham
  - Fallåker
  - Robin Hood
---

Låt mej börja med att blåsa luften ur myten med en skopa verklighet, så får vi
det undanklarat: Sheriffen av Nottingham, så som han beskrivs i sagan om Robin
Hood, har aldrig existerat. Robin Hood har antagligen aldrig heller existerat,
men det tar vi en annan dag. Händelserna kring Robin Hood-historien utspelar sej
nämligen riktigt is slutet av 1100-talet, närmare bestämt år 1193-1194[^1] och
Nottingham fick sin första sheriff år 1449, alltså nästan 250 år senare (och
Robin Hood-sagan handlar i sin populärt berättade form [inte om
tidsresande](https://tardis.fandom.com/wiki/Robot_of_Sherwood_(TV_story)) :).

Robin Hood-sagan har berättats åtminstone sen sena trettonhundratalet, och trots
att berättelsen ändrats en hel del under de senaste 643 åren[^2], är det i grund
och botten den om en gentlemannatjyv som stjäl tillbaka skattepengar den illa
omtyckte Prins Johan håvat in. Egentligen är det ganska kul att en saga levt så
länge, trots att både kyrka och hov välat glömma den och dess anarkistiska
budskap. Det måste vara protestandan och någon som står upp mot mobbaren och för
den lilla människan som måste fungera genom århundradena.

Ursprungligen var Robin en herde eller "fri bonde" _(yeoman)_ men med tidens
gång uppgraderades hans status slutligen till adelsman, först earl av Huntingdon
(Cambridgeshire) och senare, som det nu berättas, av Locksley (nära Sheffield,
Yorkshire). Det är 112 km från Sherwood till Huntingdon och 55 km från Sherwood
till Locksley. Men det här inlägget skulle inte handla om Robin Hood, Robin,
earl of Huntingdon eller Robin of Loxley utan om Sheriffen. Som alltså inte
existerat.

Eller, sheriffer har förstås existerat, och de första sherifferna är ingalunda
de i Vilda Västern, med stjärna på västen och revolver i bältet. Ordet sheriff
kommer från det fornengelska _shire-reeve_, så sheriffer fanns det gott om i
gamla England. De som kan sin Sagan om Ringen vet att _shire_ betyder "fylke",
dvs en lite lagom stor region, och _reeve_ är besläktat ordet _greve_ (och
antagligen engelskans _referee_, dvs domare) och betyder i
shire-reeve-sammanhanget nån som håller reda på lag och ordning i Kronans
(alltså Konungens) namn. Lite sådär som Lagens Långa Arm, eller _Hand of the
King_ liksom. På den tiden kallades ett område stort nog för ett hushåll att
odla en _hide_. Tio  _hides_ bildade en _tithing_[^3] och tio tithings en
_hundred_. Och en lite lagon grupp _hundreds_ bildade ett fylke, alltså en
shire. Fylkets lokalhärskare betecknades _earle_ (av "eldorman") och earlens
officer med ansvar för praktiskt tillämpande av lag och ordning var
shire-reefens. Distriktspolisen, alltså. Så nu vet vi det!

Sheriffen av Nottingham är en av de två aller tre antagonisterna i Robin
Hood-historien, beroende på version; Prins Johan, Sheriffen av Nottinghan, och
Guy av Gisbourne, som inte är med i alla versioner av Robin Hood, men nog i vår.
Av dessa är det bara Prins Johan som med full säkerhet existerat på riktigt.

Prins Johan ("Utan Land") är en av de absolut sämsta regenter England haft. Han
regerar medan Richard Lejonhjärta är ute på korståg, eftersom Richard inte är så
intresserad att hålla reda på England. Prins Johan har stora planer på att bli
kung av England och kung blir han faktiskt sist och slutligen efter att brodern
Richard dog. Tilläggsnamnet "Utan Land" fick han däremot först senare, efter att
han behövde ge makten över England till Kyrkan, så att Johan bara blev länsherre
under påven. Voj voj.

Sheriffen av Nottingham är alltså Kunglig Upprätthållare av Lag och Ordning. Han
har porträtterats som allt mellan tafatt och ambitiös, mesig och listig,
ondskefull och dum. Sheriffen rapporterar direkt till Prins Johan, vilket alltså
betyder att han sitter väldigt högt i näringspyramiden. Ofta är det ändå
Sheriffen som har ett lite mer utvecklat systemtänkande än hans högra hand Guy
av Gisbourne, som hellre tillgriper mer handgripliga metoder. Det är tex. ofta
Sheriffen som föreslår att Gisbourne ska ha (tysta) vakter när de transporterar
pengarna genom Sherwood-skogen och det brukar vara Sheriffen som hittar på
tävlingen i pilskytte för att fånga Robin Hood.

I alla de versioner av Robin Hood jag har sett är det ändå Guy av Gisbourne som
är den riktigt onda och obehagliga personen, fast han är ju inte med i alla
versioner av sagan. Gisbourne riktigt njuter av att pressa pengar ur pöbeln och
gottas av att straffa dem som inte kan betala. Som jag ser det i denna dynamik,
så är Sheriffen lite avlägsen från verkligheten "på fältet" och närmast beordrar
sin hantlangare Gisbourne att håva in pengar, vilket Gisbourne gärna gör, mer en
gärna faktiskt. Alltid kul att få göra illa åt folk, speciellt då man har
immunitet från ovan. Ska man se nåt positivt i det hela så är Guy av Gisbourne
en man som tar sitt arbete med stor entusiasm och noggrannhet.

Hur Sheriffen, Gisbourne och Prins Johan kommer att vara i vårt skådespel
återstår att se. Och samma gäller naturligtvis alla de andra karaktärerna, som
jag säkert kommer att skriva ett och annat inlägg om oxå!


[^1]: Kung Richard Lejonhjärta blev tillfångatagen år 1192 och frigiven 1194, vilket är en av de bärande delarna av historien. Richard satt tillfångatagen i Österrike och frigavs mot en lösensumma på 150000 Mark, vilket var nog att ge England finansiella bekymmer för flera år framöver, och alltså väldigt mycket mer än vad det froma folket av Nottingham hade lyckats skrapa ihop. Jo, historien är knepig.

[^2]: Den senaste är en smådystopisk men ändå lite gullig [scifi-animation på Youtube](https://www.youtube.com/playlist?list=PLjq6DwYksrzzc2YaTAaGJ8xj6OwgYqCbM), där Robin Hood förresten är en flicka! Bra så!

[^3]: Varför en "hide" inte hette en _thing_ när tio hides är en _tithing_ är  nog något jag aldrig kommer att förstå.
