---
title: "Skrivet i stjärnorna"
type: post
date: 2022-06-04T22:09:33+03:00
featured_image: '/img/PXL_20220526_094946266.jpg'
image_name: 'Ditt namn står skrivet i stjärnorna'
image_by: 'Robin Laurén'
tags:
  - Finns
  - Pinocchio
  - Sås & Kopp
  - cowbell
  - regn
---

Nästa vecka är det .. _premiärvecka!_ När jag skriver ut det och läser det på
min skärm grips jag av en liten förlamning som får mej att känna som om min
kropp vill krympa och gömma sej. Jag är med all nödvändig uppenbarhet inte
riktigt redo.

Jo, vi börjar bli bättre. Tillochmed jag börjar bli bättre. Och rent logiskt
tänkt kommer vi att klara det här eftersom jag vet att folk mer eller mindre kan
allt de bör och att premiär och publik får folk att skärpa sej, men min
ödlehjärna tittar oroat omkring sej för att hitta en stor sten att gömma sej
under. Ödlehjärnan och den rationella hjärnan har sina meningsskiljaktigheter
emellanåt.

Sen senast jag skrev har tränat med mikrofoner, vilket igen fick det hela att
känna sej ännu mer "på riktigt". Om några dagar har vi *generalrepetition* och
det är så på riktigt det kan bli utan att alldeles vara det. Folk från tidigare
Finns-produktioner har bjudits in att sitta i publiken. Dels hoppas jag alla
kommer och dels hoppas jag att vi gör bra ifrån oss. Fast jag vet ju att man
knappt kan få en bättre publik än en bestående av skådespelarkollegor. Raseborg,
jag minns er från den enda regninga Pippi-föreställningen. Ni var underbara. Så
vitt jag vet har producent Malin bjudit in både Sås & Kopp och casten som
spelade Pinocchio på Svenska Teatern i början av nittiotalet att titta på oss.
Jag hoppas kanske ändå att de kommer på nån lite senare föreställning när
nervositeten så småningom förvandlats till rutin.

Och detta för mej till cliffhangern jag lämnade i förra postningen. Den gällande
musiken. Nämligen: Sås & Kopp, som skrev det manuset och den musik till
Pinocchio för trettio år sen, som även vi spelar nu, _spelar in musiken på nytt
för oss!_ Jepp! Vi kommer att köra med musik rakt från gynnarna som skrev och
spelade "originalmusiken" på Svenska Teatern på nittiotalet! Och inte bara det.
Musiken har arrangerats och transponerats enligt våra önskemål,[^cowbell] så det
är faktiskt så att Sås & Kopp spelat in för oss, för just detta skådespel, för
denna produktion och för er, kära publik! Kan man säja att detta är *Sås & Kopps
första nyinspelade musik på 22 år?!!* Det här är ju enormt! En rent ut sagt
episk kulturhändelse! [Nog har ni väl skaffat biljetter
ren?](https://www.finnssommarteater.fi/sv/arets_pjas/pinocchio/) :)

[^cowbell]: Okej, inte _riktigt_ alla önskemål. Jag skickade hälsningen
  _"[I need more cowbell](https://vimeo.com/425939085)"_ till Pasi, men det hann
  de visst inte med så jag får köra Sergeis sång med koskällan klonkande inuti
  mitt huvud bara.
