---
title: "Som Statist"
type: post
date: 2020-11-23T11:27:57+02:00
draft: false
tags:
  - film
  - statist
  - Sorjonen
---

Idag är jag statist i filmningen av en finsk deckare. Sorjonen och muralmorden,
eller det som heter _Bordertown_ på Netflix. Kameran går och det är lite
pirrigt.

Det andra gången jag medverkar, på ett mycket litet plan, i film. Första gången
var jag "reporter" i Den svavelgula himlen och eftersom jag hade en (1) replik,
var jag skådespelare -- filmskådespelare! -- och idag är jag statist, på
hemligstemplad ort och gör som regissören säjer.

Att vara statist är, baserat på min gedigna expertis, lite som att vara i armén.
Dagen börjar tidigt. Sen väntar man i tre timmar. Ibland kommer nån stammis från
produktionen och berättar vad som händer. Sen händer inte så mycket så man
väntar vidare. Sen kommer en annan stammis in och berättar mer eller mindre
samma sak. Plötsligt ska man börja hända, och så tågar man iväg till "setet".
Där blir man placerad, dirigerad och regisserad att, som i mitt fall idag, sitta
vid ett bord i ett auditorium och reagera enligt givna direktiv. Ibland ska man
"göra anteckningar", och det gör jag nu.

Sorjonen är som sagt en deckarfilm, så det vimlar av "poliser" här. Jag antar
att de är skådespelare, eller åtminstone poliser som idag är skådespelare, men
stämningen är ändå lite tajt. Poliser i blå haalare (overall, för er
vattusvenskar) eller svart kravallutrustning, tjänstevapen och stormgevär, inger
inte mej nån större mån av trygghetskänsla, utan får min ödlehjärna att
arbitrera mellan flyendets och fäktandes gängse fördelar.

Nu blev det lunch, så nu får vi sitta ner och vänta igen ett tag. Jo, jag vet.
Medan vi väntar är det många som arbetar. Tekniken jobbar, kamerateamet hittar
nya vinklar. Och jag bloggar i klagande ton. Inte ska jag klaga.

Aspirerande statistkandidater funderar kanske hur man blir statist. I mitt fall
var det min producent på Finns, Robert Ingman, som kablade ut att filmstudion
Fisher King söker statister, så jag nappade på och här sitter jag. Och när jag
tänker på saken är det precis så som det gick även första gången jag satt på
business-sidan av filmkameran. Men om man tar en titt på Solar Films (som
spelade in Den svavelgula himlen), så har de ett
[förmulär](https://solarfilms.com/avustajaksi/) för intressenter. Vissa studion
basunerar ut [på Facebook](https://www.facebook.com/ohjelmaavustajat/) och
Fisher King (Sorjonen) tar emot intresseanmälningar per elpost (år 2020!) på
adressen avustajat-@-fisherking.fi[^afish]. Och varför skulle man det? Tja? Det
är lite spännande ändå att se hur film görs på andra sidan duken. Och spännande
om man kanske skulle få en glimt av sej själv på vita duken. Eller kanske få en
liten roll i en framtida produktion? Med _två_ repliker nästa gång? Vem vet.


[^afish]: Jag hoppas jag har orsak att ändra detta snart
