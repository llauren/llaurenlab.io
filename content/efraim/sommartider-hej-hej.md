---
title: "Sommartider, hej hej!"
type: post
date: 2022-02-06T22:38:41+02:00
featured_image: "/Finns_2019-Pippi-i-skolan.jpg"
image_name: "Pippi i skolan, en regnig repetitionsdag (Finns 2019)"
image_by: "Robin Laurén"
tags:
  - audition
  - Raseborg
  - Finns
---

Vet ni vad. Det kanske blir sommar i år igen. Och skall det riktigt till det, så
har vi koronan i skick tills dess (ja, hoppas kan man alltid).

På söndag var min dotter och jag på audition till Raseborgs sommarteater och om
en vecka blir det Finns audition! Få se om nåndera vill ha oss :)
<!--more-->

Jag har varit på två Finns-auditioner så Raseborgs var ju nåt ganska nytt för
mej. Här skulle man dra igenom en del av en scen och göra lite
improvisionsteater med regissören. Scen-genomdraget var lite roligt, eftersom
den bestod av en dialog mellan två personer och den tog vi två gånger, med
ombytta roller andra gången. Här var grejen, tyckte jag, att ha en klart olika
person mellan varven; att inte vara samma slags person i båda rollerna men inte
heller kopiera regissörens person. Improvisationsdelen var oxå rolig. Här
spelade regissören en upprörd vänsterhänt kund som dagen innan hade köpt en
kanna för högerhänta och vad är det här för jox? Det var bara att hastigt inta
en personlighet av nån som var på jobbet andra dagen och spela konsekvent enligt
vad den personen skulle göra. Håll rollen. Mitt butiksbiträde var osäkert ...
och vänsterhänt. Från Pargas dessutom, vilket sedermera visade sej att regissörn
oxå är från i verkliga livet.

Så var det koreografiandel så som på Finns, och sång med gruppen, solo och
tillsammans. Koreografiscenens bakgrund var ett gäng osäkra människor som inte
haft kontakt med rock'n'roll men känner hur det börjar svänga i bunten. Bra.
Kräver inget spelande, eftersom åtminstone jag inte har nån koll på vad
självsäker koreografi ska se ut som. Vad sången gäller så skulle man först (i
grupp) sjunga _Heartbreak Hotel_ på eget sätt (jag var ganska högljudd) och sen
lärde vi oss att dra de lite mer komplicerade synkoperna tillsammans. Till slut
drog vi _I can't help_ en i taget och alla ihop. Riktigt sött :)

Intrycket av auditionen var att det var roligt och det var ... _på riktigt._ Det
kändes som om jag redan var en liten del av produktionen då jag fick agera med
produktioneteamet på lika villkor med det gamla gardet, i Karis där magin blir
till &emdash; som om jag redan uppnått något när jag bara varit på audition!
Produktionsteamet kändes jättebra och de stödde med hejarop när vi shejkade loss
i koreografins virrvarr. Tackolov krävde inte koreograf Katrin(e) riktigt samma
grad av tiktok-precision av oss som av ungdomen. Men hur som helst, vare sej jag
kommer med i Raseborgsensemblen eller inte, så har jag haft det bra.

Igår plingade det till i postlådan. Både fröken och jag har bjudits in till
auditionsrunda två, nu på onsdag kväll! Och jag ska framföra en sångsnutt till
ur musikalen som jag inte lyssnat på förr, så det blir till att lära sej lite
hastigt och lustigt. Det är inte utan att jag är lite hajpad!

Inkommande söndag är det sen audition för Finns. Tills dess borde jag lära mej
en text som jag kan dra utantill så att jag kan "spela" den på olika sätt. Vi
presenterades två poem att välja emellan, eller så får man ta vad man vill.
Eftersom det där med att lära sej saker utantill inte (heller) är mitt forte,
kom jag i morse på att jag ju läste både _Hur gick det sen?_ och _Vem ska trösta
knyttet?_ varje kväll i flera år för en femton tjugo år sen, så kanske jag kan
återkalla nån av dem. Fort.

Mitt stora aber är vad jag egentligen själv ska göra sen. Raseborg är juvelen i
kronan vad gäller finlandssvensk sommarteater enligt mej, men det är nog så
långt att åka för att komma till repetitionerna och föreställningarna. Finns å
andra sidan cyklar jag till, eller från, på en kvart, vilket visade sej vara
jättehärligt efter Pippi-föreställningarna. Lite så att Finns är min
"hemteater". Nu söker jag till båda produktionerna i hopp om att få plats på en,
så i bästa fall löser problemet sej självt (eller så står jag nästa gång på scen
inkommande höst på Fallåker, vilket oxå är en acceptabel lösning). Men skulle
jag sådär som ett tankeexperiment ges möjligheten att välja mellan att spela för
Raseborg eller Finns, har jag faktiskt ingen aning vilken jag skulle välja. Det
beror väl på rollen? Vad skulle jag hellre göra?

För tillfället är jag bara lyckligt lottad att jag fått vara med såhär länge.
