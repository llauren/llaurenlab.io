---
title: "Stulen kärlek!"
type: post
date: 2020-09-30T23:00:21+03:00
tags:
  - Fallåker
  - Stulen kärlek
---

Det är full rulle på [Fallåker](https://fallaker.fi) trots att jag inte skrivit
något här på länge. Vi jobbar på en fars som heter _Stulen kärlek_. En fars är
ett slags fartfyllt komiskt skådespel, ofta med förväxlingar eller missförstånd,
oj-då-hur-gick-det-så-här -situationer och en rapp dialog. Premiär blir det i
januari 2021 och [biljetterna](http://www.fallaker.fi/new/boka-biljett/) har
precis börjat säljas![^corona]

Ensemblen består av bekanta nunor från förra årets Robin Hood, styrkt av
Matilda, som är ny på Fallet för i år (men ingalunda ny på teaterscenen; hon
spelade faktiskt Ronja med Thomas och Tipi som föräldrar för några år sen på
Finns). Vi är en klart mindre trupp än senast, bara [sju personer står på
scen](http://www.fallaker.fi/new/ny-sida-under-uppbyggnad/skadespelare-pa-fallaker/).
Det är både roligt och underligt att se hur alla passar in i sina
roller[^komplex].

Vi har övat ett par gånger i veckan sen lite efter skolstart och har nu hankat
oss genom hela skådespelet. Det betyder att vi sådär i princip vet var vi ska
stå på scen och hur vi tänker och gör när vi säjer våra saker. Än har vi inte
kommit så långt att vi skulle lärt oss våra repliker utantill (utom då kanske
Make, som är ett fenomen i sej vad repliker gäller), men det kommer nog snart.
Lyckligtvis finns det ingen koreografi i skådespelet, för då hade jag nog mer
bekymmer än jag behöver.

Just nu är jag i fasen där jag bekantar mej med min rollkaraktär Evald Eriksson
Blom, en businessman och "ägare" av allt omkring sej. Som person är jag nämligen
nog så olika Evald. Evald bryr sej om saker, om fasader och hur saker --
däribland även medräknad hans fru -- ser ut. Nåt större känsloband har han inte
till något och inte heller till sin unga och vackra hustru. Fast hon är ju
trevlig att visa upp på affärsmiddagarna förstås. Lite sådär som en trofé.

Själv lider jag i jämförelse lite av ett anspråkslöshetskomplex. Att gå omkring
och anse att jag har rätt och har rätt till vad som helst, och att pengar är
vägen till lycka -- det är något jag medvetet behöver arbeta på om det ska bli
bra. Kanske det sådär i det stora hela är en bra sak.

En för mej spännande skillnad till tidigare produktioner jag spelat med i[^tcv]
är att Stulen kärlek inte är en saga. Det här betyder att både handlingen och
karaktärerna är på något sett mer _verkliga_, även om de är karrikerade och lite
tilldragna. Det betyder att det finns underförstådda ramar för hur man kan bete
sej för att det hela ska kunna hålla sej inom pjäsens ramar av realism.

[^komplex]: speciellt alla _andra_ i ensemblen, då -- som vanligt känner jag
        mej miltals efter dem

[^corona]: och skulle coronaläget så kräva, flyttar vi på föreställningarna
      eller hittar på nåt annat lägligt. Dessutom säljer vi bara 60 biljetter
      per föresällning, då salen i normalläge drar 180 pers.

[^tcv]: _Pippi Långstrump_ och _Robin Hood_, säjer han med rösten i barytonläge,
      i ett desperat försök att låta som en övertygande amatör_skådespelare_
