---
title: "Stulet och klart"
type: post
date: 2021-10-31T22:43:00+02:00
tags:
  - Fallåker
  - Stulen kärlek
  - slut
---

Så var Stulen Kärlek färdigspelad på Fallåker. En vecka sen var det faktiskt,
och nu har jag haft en blandning av lugn och tomhet i detta teaterlediga
veckoslut.
<!--more-->

Jag spelade in de två sista föreställningarna och i sammanfattning kan väl säjas
att det inte gick riktigt så bra som man kunde ha hoppats. Första akten fick jag
inspelad mer eller mindre enligt plan, men andra akten finns bara ... till halva
scenen. Har inte ens vågat lyssna hur ljudinspelningen låter. Men vi har haft en
fantastisk spelperiod[^spelperiod] och vårt spel har mått bra av att ha fått
mogna. Dessutom har vi haft jäkligt kul att spela inför en _live_ publik, och
det är ju därför man gör det här.

[^spelperiod]: Ja eller egentligen har vi ju haft _tre_ spelperioder -- rekord
    på Fallåker!

Så vad har jag lärt mej? Att göra en lite mer _"riktig"_ roll och att lyssna
lite mer på regissören. Visst, alla karaktärer i Stulen kärlek var ganska
vrickade, men ändå kändes min Evald på något sett trovärdig, och det är ju något
helt nytt i jämförelse med Pippis pappa Efraim eller Sheriffen av Nottingham.
Jag har lite undrat när jag känner mej färdig att titulera mej
_amatörskådespelare_ men det börjar nog bli snart.

Nu gör vi plats för Fallåkers jubileumsproduktion, där äldre pjäser (och
skådespelare) ställer upp i kavalkad, vilket betyder att jag har lite ledigt
från fallåkerscenen.

Men trodde ni det var färdigstulet? Ha!

Edit post factum: Vi hade planer på att åka ut på världsturné genom hela västra
svenskflinland, men si den planen tog coronan. Jädra corona.
