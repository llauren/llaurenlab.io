---
title: "Stüldt Håjt!"
type: post
slug: styldt-hojt
date: 2019-09-13T21:58:22+03:00
tags:
  - repetition
  - Fallåker
  - Robin Hood
---

Idag var det bra rep! Vi tränade tre scener i början med Sheriffen, Guy _(Gi!)_
och Prins Johan och jag har nog sällan skrattat så mycket på en repetition som
nu!

Prins Johans karaktär är bara så fantastisk! Bortskämda ungen som kunnat springa
omkring i hovet och göra bus medan storebror blivit uppfostrad till kung, fast
som lillebror så kan man inte _bli_ kung ... förrän _nu!_ Själv som sheriff får
jag både vara förtryckt av min flamboyanta chef Prins Johan, och domdera över
den otvättade pöbeln. Lady Marion hade verkligen sin stund att skina när hon
fick berätta vad som egentligen hände med prinsens pengar och varför.

Det blir mycket hojtande i dessa scener. Det är högljutt, spänt och vansinnigt.
Och oj vad det är roligt :D
