---
title: "Tack och hej, leverpastej!"
type: post
date: 2022-07-04T23:17:06+03:00
featured_image: /img/PXL_20220628_165553908.MP.jpg
image_name: "Tack och hej!"
image_by: "Hanna Laurén"
tags:
  - Finns
  - Pinocchio
  - slut
---

Så var Pinocchio färdigspelad -- tack och hej, leverpastej! Kanske för att jag
spelat slut en produktion nån gång tidigare, eller kanske mer för att jag ännu
inte hunnit sitta ner och göra ingenting, så känns inte riktigt den där ekande
tomheten som infunnit sej efter tidigare [slut](/tags/slut). Ännu. Kanske.

<!--more-->

Vi spelade vår bästa föreställning till sist. Det finns egentligen tre teman som
kan beskriva varför: Säkerhet, spelglädge och sammarbete. Efter sjutton
spelningar inför levande publik var alla till den grad bekanta med både
materialet, sej själva och varandra att det som under premiären kanske varit
mest utantill-läsning, nu utvecklats till någon form av rutin, som utvecklats
till kontroll och en självsäkerhet både på vad egna prestationer gäller och en
känsla av att alla andra oxå klarar det. Och känner man sej säker på scen, kan
man ha det roligt. Det betyder inte att varje replik går precis som i manus
varje gång, men om någon missar nåt är det inget man behöver bli upprörd över.
Nån tar koppi [^koppi] och showen rullar på. Men framför allt betyder det att
man kan njuta av att stå och göra sin grej, tillsammans. Och tillsammans är just
precis vad teater är. Det finns ingen kapellmästare som står och koordinerar med
viftande händer. Alla vet var, när och hur de ska vara -- och vet de inte det,
så hjälper nån. Regissören har gjort sitt och nu är det ensemblens skådespel.
Skådespelet utvecklas under tidens gång, men det gör skådespelarna oxå. Det är
nåt väldigt fint med det.

[^koppi]: Lyra, i betydelsen att ta emot den boll som kommer flygande

Just det där med samarbete och ansvar är nåt som gör amatörteater väldigt fint,
speciellt med tanke på att Finns till stor del består av barn och ungdom (och så
några mer eller mindre ansvarslösa personer i länge sedan ren myndig ålder) som
kanske inte haft orsak att lära sej sånt riktigt än. Teatern lär folk att ta
ansvar för sej, för sina grejer, för varandra och för sin publik. Jo, man lär
sej allt det här i lagsport, FBK och scouting oxå, men teater är ett sätt att på
ett handgripligt och praktiskt sätt lära ut praktiskt teamwork, att ta ansvar
och att våga vara både sin karaktär och sej själv. Dessutom är det helt
vansinnigt roligt att göra teater. Det harmar mej lite att jag inte insåg det i
tonåldern, men på den tiden hade jag fullt upp med scoutande och dessutom var
jag ganska blyg. Var sak har sin tid och min tid för teater började på gamla
dar.

Vad har jag lärt mej den här gången då? Jag tror jag sjunger bättre än förr och
jag lyssnar på regissören lite mer villigt. Den stora insikten var väl ändå den
att eftersom vi inte hade en producent med i ensemblen (som Jonna i Pippi, eller
Danielle på Fallåker) och att det egentligen bara fanns två (till åldern) vuxna
människor i ensemblen, så har jag ett större ansvar än tidigare att föregå med
gott exempel. Det betyder inte att jag skulle ha _varit_ ett gott exempel --
lärt mej mina repliker, varit i tid, hållit tyst då regissören pratar -- tvärtom
var det här nåt jag insåg först när vi var i slutet av övningsperioden och då
var det så dags. Bra att det inte blev helt katastrofalt ändå :)

{{< figure
      src=/img/PXL_20220609_162631864.MP.jpg
      caption="_Åsnor ska ni va, ha ha ha ha!_"
>}}

Mitt finaste minne är nog Sergei-scenen med åsnorna. Sergei är nog den
vansinnigaste roll jag spelat (men Sheriffen av Nottingham kommer som bra tvåa)
och det där med att skräna metallmusik på scen och vara hemsk och hotfull var
skoj! Finast av allt var nog ändå åsnorna som spelade så bra att jag ibland
tänkte de var rädda på riktigt. Då var det bara att lita på att vi bakom sminket
egentligen är vänner och att de bara spelar så himla bra. Men det är inte helt
lätt att gå och skrika på små lättskrämda åsnor som bakom scen är folk jag bryr
mej mycket om.[^arg]

[^arg]: Jag har skrivit om det här förr i [att våga vara arg](/efraim/att-våga-vara-arg.md).

Nu börjar det ändå kännas lite lugnt. Vi träffas i nån konstellation ännu under
sommaren för att gå o kolla på andra sommarteatrar och till hösten ska vi se
inspelningen av Pinocchio, bara jag får den klippt (och bara tillräckligt många
av kamerorna fungerade!). Tack till er alla i ensemblen, i produktionen och i
publiken. I vinter spelar jag igen på Fallåker i en fars,
[Panik på kliniken](https://www.fallaker.fi/new/services/panik-pa-kliniken/).
Kom gärna å se!
