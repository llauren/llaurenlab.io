---
title: "Titta nu hur du ser ut"
type: post
date: 2019-09-18T22:57:55+03:00
draft: false
tags:
  - Robin Hood
  - koreografi
  - Fallåker
---

{% instagram B2kP4CVBNVp %}

Vi fick teaterkläderna härom dagen och de ser förfärligt snitsiga ut. Kläderna
är från en affär i Sankt Karins som specialiserar sej på medeltida jox och
attiraljer. Ännu har vi inte haft träning i kostym, men en del klädprovning har
det varit redan.

En sak som är uppenbar är att medeltida kläder inte är av riktigt lika mjukt
material som moderna, och det gäller även medeltida kläder gjorda på detta
årtusende. Men varma är de. Så om jag ska skutta omkring i min sheriffskrud i
tjugo föreställningar, behöver jag skaffa en modern, långärmad sportskjorta att
ha under.

Dessutom behöver jag skaffa ett par röda tajts. Odin hjälpe mej.
