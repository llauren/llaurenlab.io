---
title: "Transformationer"
date: 2019-05-05T22:11:00+03:00
tags:
  - Pippi Långstrup
  - Finns
  - koreografi
---

Det här veckoslutet har vi haft dansövning både på lördag och söndag.

Till min inte alldeles lilla överraskning så har jag börjat lära mej mina steg
för koreografin jag deltar i (för närvarande alltså en), till den grad att jag
vet vad som går fel och att jag har ett hum om hur det egentligen borde gå.
