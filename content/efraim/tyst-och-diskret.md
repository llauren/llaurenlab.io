---
title: "Tyst och diskret"
type: post
date: 2019-09-18T22:24:55+03:00
tags:
  - Sheriffen
  - Robin Hood
  - Fallåker
---

Sheriffen är ingen lugn och avslappnad person. Oftast är han arg, spänd eller
nervös, eller både arg, spänd och nervös. Det betyder att han ofta skriker,
vispar med händerna och står hopkurad i obekväma poséer. Efter en vanlig övning
är jag trött i halsen och det är inte bra.

Uppenbarligen är det en skillnad mellan att skrika och teaterskrika och min
utmaning blir att hitta skillnaden samt att ersätta det nervösa skrikande med
någorlunda övertygande teaterdito.

Jag har gått på sångtimmar på Arbis ett par år och lärt mej att det finns fel
sätt att sjunga och rätt sätt att sjunga. Fel sätt är det man brukar göra på
konserter, kalas och karaoke och får en att undra varför man är så hes nästa dag
när man hade så roligt kvällen innan.

Baserat på en Internets samlade visdom så är tekniken för att teaterskrika
besläktat hur man sjunger rätt. Tala högt. Med stöd från nån magtrakten utan att
spänna hals eller stämband. Nu är det bara att studera och träna. Och skydda
rösten. Hoppas det låter som en arg och nervös sheriff (och inte en hes
sheriff).
