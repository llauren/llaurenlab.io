---
title: "Tyst på scenen"
type: post
date: 2020-05-05T12:36:23+03:00
tags:
  - Finns
  - Saltkråkan
---

[Saltkråkan](https://finnssommarteater.fi/svenska/arets_pjas/vi_pa_saltkrakan/)
på Finns sommarteater inhiberades till en blandning av förståelse och sorg, och
flyttades till sommaren 2021 till kollektivt jubel från hela produktionsteamet!

Övningarna på distans har varit en lucka av normalitet under karantänen och det
är trist att de har avbrutits, men det är ju ett finansiellt faktum att vi inte
kan fortsätta med dem om vi inte har en teaterproduktion att öva för. Men nytt
försök nästa år, och jag hoppas att många från Saltkråkan 2020 är med i
Saltkråkan 2021!

Edit post factum: Våren 2021 fortgick coronan ännu utan avtagande, så hela
Saltkråkan avblåstes.
