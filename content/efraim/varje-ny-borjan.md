---
title: "Varje ny början kommer från slutet av något gammalt nytt"
type: post
date: 2019-10-20T16:43:37+03:00
draft: false
tags:
  - Pippi Långstrump
  - Robin Hood
  - Finns
  - Fallåker

---
Varje ny början kommer från slutet av en tidigare början.

Ungefär så sa Seneca[^seneca], eller Dan Wilson från orkestern Semisonic. Och precis så kändes det när jag sent om sider kom i säng igår efter Nyländsk Afton, Nylands teatermotsvarighet till Oscargalan. Det var där Kapten Efraim Långstrump fick uppträda för sista gången, och Sheriffen av Nottingham den första.

På onsdag kväll träffades Pippi-gänget för en första och enda träning inför NSU-galan. Det var en mystisk blandning av att inte ha setts på flera månader och som att familjen träffas igen, vi sågs ju alldeles nyss! Det var kramfest och glada miner, och snabbt infann sej hysterin, vimsigheten om den nästan kompletta bristen på proffsighet som präglat hela ensemblen (frånsett regissörn och producenten, som till motvikt är mestadels proffsiga i sitt beteende).

Vi skulle framträda med den avslutande sekvensen från Pippi Långstrump, från att Efraim skyndar på Pippi att komma iväg, ända till slutsången med dans.

Det gick inte så bra. Det var främst Liv och jag som hade repliker och det mesta vi mindes hade vi glömt. Koreografin gick kanske inte riktigt med lika säkra steg om i juli. Men en och en halv timme gör mirakel och då vi efter avslutad träning hämtade våra kostymer ur lagret kändes det som om vi nog klarar av det här.

Parallellt med Finns-handlingen har Fallåker teater svettats med sitt program — beskrivande nog från början av pjäsen — där Prins Johan gör Robin laglös. Scenen är egentligen ganska krävande för den har många repliker, miner och rörelser som måste gå jämnt ihop för att den ska fungera. Därtill skulle vi sjunga Hårda bud i stämmor.

Kvällen kom. Det var mingel och vimmel, svammel och yra. Jag hade nu två familjer[^familjer], två omklädningsrum, två bord att sitta vid, två set kostym att byta till, förutom galakostymen förstås, och två teaterstycken att spela i. Jag var ju ingalunda ensam om mitt dilemma; Ellen, Make och Wilhelm är och var ju både i Raseborg och Fallåker.

Nyländsk Afton, visade det sej för mej som var där första gången, en väldig mysklubb för inbördes beundran, trivsel och gladlynt ryggdunk. Det kändes samtidigt lite inåtvänt, en intern svanslyftarfest, men samtidigt väldigt trevligt. Skoj att vara med sina egna men även morjensta på nya bekantskaper och såna men sett lite i omgåendet tidigare.

Finns hade soundcheck och på det stora hela gick det ganska bra. Fallåker soundcheck drog sheriffen i full Efraim-mundering, med hatt och flätor ツ

Och snabbt var det showtime. Alldeles pyttelite pirrigt kändes det, men mer bara skönt att komma upp på scen. Resten gick på adrenalin och rutin, och som vanligt, improviserade Liv och jag hälften av våra repliker eftersom ingen riktigt ville minnas precis vem som skulle säja vad och i vilken ordning. Så var det vild dans, klapp i takt, applåder och sista bugningen till publiken. Tack, adjö, breda leenden och så var det slut, så slut det blir.

NSU-oscarsgalan är naturligtvis oxå en prisutdelning. Vår Anna tilldelades välförtjänt priset för Årets Ungdom, Wilhelm från Raseborg/Fallåker utsågs till Årets Talang och Finns fick priset för Årets Ungdomsförening, så det var glada nunor i båda teatrarna[^talang].

Så var det dags för Fallåker. Vi var alla i våra scenkläder, för första gången, och vi såg snärtiga ut. En viss premiärnervositet låg i luften. Hur skulle det gå. Ett par repliker hade strukits från manuskriptet, likaså en grej där Gisbourne och sheriffen skulle springa efter vakter. Tajmingen var ganska kritisk för att det skulle bli flyt i dialogen. Skulle det bli blackout på scen? Skulle det ... och sen speakade konferentier Riko Eklundh in oss och bollen var i rullning. Bära eller brista, men nu fanns ingen återvändo.

Och bra gick det. Vi spelade med våra roller. Fyllde i små grymtanden på nya ställen, utväxlade menande blickar som inte stått i manuset. Jag tog en av Prinsens repliker, fast jag är inte säker om jag stal eller sufflerade, men flyt hade vi. Robin av Locksley såg precis så indignerad ut som han skulle och Prins Johan just sådär härligt motbjudande som hans karaktär påbjuder. Blir resten av pjäsen så här, blir det succé (i Fallåker mått, men ändå).

Efteråt, på Fallåker teater med ett pyttlelitet glas skumvin i hand slog det mej att det här faktiskt var vår premiär. En trailer, men live. Nu har vi kommit ut. Det här är vad vi är, eller åtminstone vad vi blir.

Kapten Efraim Långstrump sätter nu kaptenshatten på hyllan för sista gången, med ännu ett återseende med Finns-familjen i form av en filmkväll om några veckor[^film].  Här tar Sheriffen av Nottingham facklan, eller kavlen. Och kanske, kanske blir det att ställa upp för auditionen i Finns nästa produktion _Saltkråkan_ nästa vår. Vi får se. Lite kittlar det.

[^seneca]: fast på latin, men hur citatet egentligen löd har jag inte hittat. En möjligt version lyder _Omnis exitus initium novum est._

[^familjer]: Fotnot till den oinvigna läsaren, min son och jag är i Pippi-ensemblen, min dotter och jag i Fallåker, vilket gör begreppet ”familj” ännu mer förvirrande.

[^talang]: Visst tycker jag att Jenny och Liv borde ha vunnit priset för Årets Talang, men i all rimlighets namn så är nog Wilhelm värd sin utmärkelse!

[^film]: Jag ska bara behöva klippa filmen först...
