---
title: "Verkligheten finns där ute"
date: 2019-04-23T19:42:03+03:00
tags:
  - Pippi Långstrup
  - Finns
  - backstage
draft: false
---
<script src="https://cdn.jsdelivr.net/npm/publicalbum@latest/embed-ui.min.js" async></script>
<div class="pa-carousel-widget" style="width:100%; height:480px; display:none;"
  data-link="https://photos.app.goo.gl/L9x8x7FBWj5Vh9166"
  data-title="Verkligheten Finns Där Ute"
  data-description="Verkligheten Finns Där Ute · Album by Robin Laurén"
  data-background-color="#292a2d">
  <img data-src="https://lh3.googleusercontent.com/ZqNwjg1K8WubI7R5sALqUIrmRTCOfwaAMRriHTYzVPEihork0NneCf8ADiOJbE0YRflbllvFnCweLKETBhtFNbhugxBnbFadheT2TTl6F9BYukunXRiBBC8Xf5Voq-MR1pfxnvdp01s=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/WXjvWUIca7hCrsbwkBq9kGo-fKY1HvDHJDvENXTbEGTNCrwijWizTvtGZPgz2MlNUB_82NunvBf_RgcjuT4Slw-Hld_B975sx6cnCecxNomMoXhoyTUB_0vqspwPXu8mv9HDGxRqWBU=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/YzU1-_b270-RRXKIj7ZhcTuJ8cFlfiP2_y0UjRtISRfQE0u8HzMeIC0Z7kBhWfpGBJzTk-jgxTco0_NzXg-GjIjYWSYvP9zv923G7Kfmu45GyRo1F1cDaQQ9SoA8w2ANvQiZdg0avwk=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/l4Luca_67YcV_MqYKsF0XjrI7gKXHSCqGvX3-u59NLaGvQgQE5A8AM8zWCgLht5LzhWcCGREgiZHhjxufTrv21B0m3qMq4MRprpyRfDStnaQeuRg35gojZ7T4sWAYsEW-ULarVXTDuw=w1920-h1080" src="" alt="" />
</div>

Jag vet inte hur det är med er. En del människor tycker om att bara se och
beundra spektaklet framför deras ögon och att kika "bakom kulisserna" förstör
magin. Om ni hör till dessa så kan det vara bra att sluta läsa nu :) . Själv
tycker jag det är spännande att få veta _hur_ saker fungerar och jag tycker inte
alls att vad jag ser blir på nåt sett mindre värt bara för att jag vet varför
det gör det. Tvärtom tycker jag det bara är mer fascinerande att veta att allt
det här ska klaffa för att det hela ska gå ihop. Men ni har blivit varnade. Här
berättas om vad som händer för ett spektakel att bli till.

En hel del har hänt sen jag skrev senast. Vi har flyttat våra repetitioner ut,
till Finns sommarteaterscen, och det känns som om våra lösryckta rep på en scen
här och en sång där mitt i allt börjar bli _verklighet_. Ja, jag hittar inte på
nåt annat sätt att beskriva det, men det är som om det håller på att bli nåt
_riktigt_ av vad vi gör.

Många börjar kunna, om inte alla, så en hel del av sina repliker, och vi har
kunnat börja öva på hur folk säjer saker och ting och inte bara vad de ska säja.
Och så har vi tränat var man ska stå och vart man ska röra sej. Hur folk
rör sej på scen är nämligen helt koordinerat och koreograferat. Det finns ingen
som står på en plats "av misstag", vilket kanske man inte tänker så noga på
när man ser ett skådespel. Fast om det hela ser naturligt ut, så har regissören
och skådespelarna gjort ett bra jobb.

Ofta berättar man en liten historia med hur folk står i jämförelse med varandra.
Det finns alltså alltid en orsak eller en liten berättelse varför Pippi står
framför eller bakom Tommy och Annika. Eller så kommer man gående från en viss
sida eftersom man gick ut från den sidan tidigare, eller så står man i en viss
ordning för annars ser det råddigt ut och det drar bort uppmärksamheten från
vad som egentligen händer på scen.

Hur ni sen ser vårt skådespel i sommar är helt upp till er. Ni ska vara välkomna
ändå! Och ser ni nån av skådisarna efteråt är det bara att säja hej! Vi är
alldeles vanliga människor och vi hör gärna om ni gillade vad ni såg -- och inte
ens piraterna är farliga på riktigt (bara lite högljudda!).
