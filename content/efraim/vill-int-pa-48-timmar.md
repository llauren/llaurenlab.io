---
title: "Vill int på 48 timmar"
type: post
date: 2022-02-10T12:38:14+02:00
featured_image: 'i_dont_want_to.png'
tags:
  - Raseborg
  - musik
  - sång
  - audition
---

Igår på Raseborgs audition framförde jag en sång jag inte hört 48 timmar
tidigare. När jag insåg det i morse, förstod jag att jag måste blogga om
processen, från noll till uppförandet.
<!--more-->

# Steg noll - pling i postlådan

På måndag kom det ett mail från Raseborg att jag var välkommen på audition två
(!!) på onsdag kväll, och att vi på auditionen skulle framföra en av två låtar
ur _All shook up_, och en fritt vald låt. Nu har antagligen många fans av
musikalen hört igenom hela skivan mången gång, men ingen av de två givna låtarna
var såna jag lyssnat på så att jag skulle minnas att jag gjort det.

Som stödmaterial skickades noterna som pdf (cut-och-paste'ade med sax och tejp
och med annoteringar för hand) samt kapellmästare Felix' pianokomp. Lär dej att
sjunga på det här. Till onsdag.

Ingen panik. Ännu.

# Steg ett - välja låt

Ut med hunden och plupparna i öronen. Efter några genomlyssningar av spåren från
Broadway-skivan, valde jag låten _[I don't want to](https://open.spotify.com/track/02GSe4Hww1RWvmt9JS3bPc?si=f60e61d3f8e542b8)_, en smörig ballad med fet
Broadway-crecendo á la Frank Sinatra mot slutet. Det ska jag väl klara av.

Jag är redan långt förbi att känna nån form av skam eller konstighet där jag går
omkring i skogen och sjunger för mej själv (och hunden, som antingen vant sej,
eller gett upp hoppet för fridfulla skogspromenader i det här skedet), så jag
gjorde så gott jag kunde att försöka få nån koll på text och melodi. Eftersom
det här var audition, tänkte jag att auditorn kanske uppskattar om jag får tag
på noterna (och texten) och klarar av att sätta dem i rätt ordning. Vilket
visade sej vara lite mer trixigt än jag tänkt mej, eftersom jag gärna
improviserar och kör lite sisådär i vanliga fall. Men nu skulle det vara enligt
noter och text.

Så kom en liten surpris. Kompet är ... radikalt olika Broadway-versionen. Dels
är den flera snäpp närmare metronomen (tackålov), men andra halvan, den med
Sinatracrecendot, är inte med! Jag försöker bestämma om det är bra eller dåligt,
men konstaterar att det får vara detsamma för nu kör vi som det är här. Simma
eller sjunk.

# Steg två - rena noter

Hemma igen, skrev jag rent noterna på Musescore. Inte bara för att de givna
noterna var lite plottriga, men för att jag då har ett verktyg att lyssna på
exakt vilka noter där ska vara och till vilka ackord. Där är en not jag har
extra svårt med (ett F# i takt 10; låten går i F-dur) och några passager som jag
av naturen vanligtvis sjunger baklänges. Och så var det ju texten, som av nån
orsak var himla knepig att gå rätt. Kanske jag bara är stressad.

# Steg tre - öva

Så var det bara att öva-öva-öva. Svårt att sova, så öva. Liten pinglande känsla
att det kanske inte var så lätt som tänkt. På tisdag hade jag lyckligtvis
sångtimme, så jag tog _I don't want to_ till [Minna](http://www.minnanyberg.fi/sv/) som hjälpte mej att hitta
tonerna. Jag låter så himla svagt före jag fått en känsla av säkerhet, men efter
en knapp halvtimmes knogande så lyckades jag böla ur mej en version som jag
kände att jag kan jobba vidare med. Sjunga i bilen på vägen hem. Gnola där
hemma. Försöka minnas texten. Ja och noterna. Sitta och analysera hur raderna
förhåller sej till varandra, vad som upprepas och vad som är olika. Lite klarare
blir det, men inte är det smidigt.

# Steg fyra - vem är det som sjunger?

Det kanske allra viktigaste Minna sa åt mej på sångtimmen är att tänka på vem
det är som sjunger och vilken situation han är i. Det blev grunden för ett nytt
kapitel.

Personen, en cool, vit, och i högsta grad heterosexuell ung man i midvästern-USA
på 1950-talet, lider av väldiga kval eftersom han just insett att han blivit kär
i en man. Men han vill int (och därav låtens namn). Han vill int medge det för
personen han blivit förälskad i och naturligtvis inte för den övriga världen
heller. Hela hans värld är på sned och dessutom kan man ju inte vara gay på
1950-talet i mellersta USA. Knappt man kan vara det sjuttio år senare ens. Så
det ska rinna av kval och osäkerhet i första strofen, förvirring, desperation,
äckel, motvilja i andra. I tredje strofen minns han hur fint livet var före han
råkade in för de här nya känslorna, så då är han lycklig, fri och tillfreds. Och
så kommer fjärde och sista strofen där han är riktigt kluven mellan vad han vill
själv, vad omgivningen tycker... och till slut ger han upp för sina känslor och
medger till sej själv att han nog älskar den där andra trots att han inte vill.

Och allt det här på 52 sekunder. Klart och tydligt, men utan att överspela. Och
som körsbär på tårtan, finns där en liten extra plingeling i backgrundsspåret
jag kan använda för att lägga in en liten uppgiven konstpaus före _but I do..._.

Hur låter uppgivenhet, förvirring, sinnesfrid, blandade känslor? En del kommer
helt naturligt när jag lyssnar på vad min karaktär känner. Vissa saker behöver
jag analysera lite; den självsäkra tredje raden är ljudligare, mera legato, tar
tonerna före metronomen. Noter som går lite före eller efter klicket vid de
osäkra delarna. Ett litet voice crack när det är riktigt illa. Och så ska det
vara riktigt tydligt i uttalet att de första gångerna är _want to_ men tredje
gången _want you_ och att man ska höra skillnaden utan att det låter som om "hör
nu att jag fattar att det är olika". Lösning, en pytteliten paus mellan want och
you, fungerar både för att förtydliga orden och att spegla personens ovilja att
medge till sej själv att han nog vill honom ändå.

Övertänker jag det här?

# Steg fem - Karis

På gott och ont har fröken och jag alltid varit i olika auditionsgrupper, så jag
har haft ett par timmar på mej att samla mej. Dels behöver jag lite sjunga upp
mej och dels behöver jag helt enkelt få upp fiilisen. Det går med hörlurar,
rokkenroll och lite spring fram och tillbaka i tamburen. Joe Strummer ska tack
än en gång för hans [kotribut](https://youtu.be/Et_Shzz-GhY) till en bättre
värld.

Och så är timmen kommen. Jag står där framför kapellmästaren som, trots att han
är bekant sen länge, ändå _är_ kapellmästare och står mellan mej och vad som kan
vara min roll, med noterna skakande i svettig hand (OK, riktigt så illa var det
inte, men ska man syssla med drama så må väl lite utsvängningar tillåtas). En
enorm koncentration att få text och noter på gängse platser (ingen aning om de
gjorde det, men de flesta torde ha gjort det) _och_ att få karaktärsdramat att
höras, synas, kännas, och i balans.

Det ryms mycket på 52 sekunder. 52 sekunder går fort. Sen är det slut. Ingen
feedback, inga applåder. Det är audition, inte konsert. Kusligt.

Min andra låt då? En sång vars tema står mej nära hjärtat. En låt om individens
strävan till ett orealistiskt mål han kanske ändå inte är funtad för: Kung
Louies sång. Jo, en risktagning, men lite roligt måste man väl ha. Och Felix
smålog nog lite :)

{{< youtube uX2mopyE5Z0 >}}
