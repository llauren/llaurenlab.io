---
title: Now
menu: main
date: 2022-01-13T15:07:00+02:00
omit_header_text: true
description: What _Now?_
featured_image: /img/PXL_20220710_201313722.jpg

---
Derek Sivers suggested one should have [a now page](https://sivers.org/nowff) on
one's blog to tell the world what one is doing _now_ (as to remind oneself what
one has decided to do, and to reduce unnecessary contacts from strangers ...
among other good reasons).
