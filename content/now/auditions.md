---
title: "Auditions"
type: post
date: 2022-02-14T11:46:03+02:00
---

As some of you may know, i dabble with amateur theatre. Last week, i was on
auditions to two theatres; one with my "home" theatre Finns, where i have been
in one production and a half (meaning it was cancelled due to this pandemic
thing, but we did have several practices over the year and a half we didn't
play) and one for a bigger theatre, Raseborg, which is a lot farther from home.

The Raseborg auditions were rather exciting because they were different in many
ways from the Finns auditions. The Finns audition on the other hand was nice
because it was familiar, and i met some familiar faces both with the auditioners
and the theatre crew.

Now i'm just waiting for the results from the auditions. I'm hoping to get
exactly one viable offer, so that i don't have to choose, because really, i'm
not sure which one i would rather choose!

On a more professional side, i'm increasingly interested in IT fleet management
on the global scale at our work. This means managing our laptops with MDM and
repairs and warranties with AppleCare Enterprise. It's not like i'm turning into
a CTO, it's just that i need to start thinking of one.
