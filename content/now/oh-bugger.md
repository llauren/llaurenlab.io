---
title: "Oh Bugger"
type: post
date: 2022-04-10T23:06:55+03:00

---

I'm just on the other side of COVID. It's been tiring but easier than i had
feared. After a few weeks of stress on both body and mind, i thought it was just
my body telling me it's time to wind down a bit when i was so tired last
Saturday, but in hindsight, i should have understood it was the Corona virus
bugging me.

Infection: most likely at the MacADUK conference in Brighton, UK, around
Tuesday. Friday: AG test negative. Saturday: AG test negative. Tired, sneezed
once or twice, and the tiniest tingling in my throat. Took a nap. No fever, no
ache. Lots of partying and unhealthy life for nearly a whole week up until that
moment, so i really thought it was that. Sunday: AG test positive. Monday: PCR
test positive. Duh.

The start of the week was a wave of exhaustion, runny nose, tiredness, some
sneezing and generally the brain being half a step behind the rest of me. But i
had no loss of smell or taste, no aching head or muscles. My self-worth was
bruised though. I should have read the signs.

Today was the first day i was among the uninfected masses. I still didn't feel
mentally ready, but there wasn't much physically wrong with me. My drive and
alertness is still in waves, but otherwise i'm fine. Tomorrow i'll do a remote
day. If concentrating a whole day gets too much, i'll have a nap on an impromptu
sick leave.

On some sunnier news, i might get a guest post on a tech company's blog and
regardless if they want me or not, i'll still post it here. It's, hopefully, the
first post in a series on human IT security, which is what i talked about in the
UK a week ago.
