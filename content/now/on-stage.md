---
title: "On Stage"
type: post
date: 2022-06-18T00:55:14+03:00
---

The last two weeks have been intense. I've been on stage nearly every day at the
[Finns summer theatre](https://finnssommarteater.fi), which is an amateur
theatre company i'm with for the second (and a half[^half]) time now. This time,
we're playing Pinocchio and i'm playing the two roles of gentle Gepetto and evil
Sergei. It's been amazing and a carousel ride.

When that's done, it's soon time for summer vacation, during which i'm going to
work on getting a remastered [talk](/talks) on Security for humans for the
[MacSysadmin conference](https://macsysadmin.se) (sadly still remote this year).

There's also going to be some travelling, but for opsec reason, i'm not telling
you about that until i'm back ;)

[^half]: I would have been there in 2020 and/or 2021 but that play was taken
  by the COVID...
