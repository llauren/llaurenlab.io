---
title: "Unwinding"
type: post
date: 2022-07-11T17:36:53+03:00
tags:
  - vacation
  - mindfulness
featured_image: /img/PXL_20220710_201313722.jpg
---

One thing with me under stress is that i don't often recognise it. Well, not
until it's really severe anyway. I like to be involved in a lot of things, i
like when things move and happen and i'm part of them. But i don't really see
how this affects me. Not until i take some time off.

Which is precisely what i've done, because i'm now on summer vacation. I've been
for a week now. And it's only now that i start to feel that i'm winding down and
that i've had a state of mind that required some unwinding.

Specifically, there were two things that woke me up to this. First, i realised i
was walking around with our dog without having a podcast playing. I love input,
especially _meaningful_ input. It's my antidote to boredom, which is what keeps
me going -- and stressed. The other was when i was sitting next to my phone
which was taking [a time lapse of forming rain
clouds](https://www.instagram.com/p/Cf3oAN0llr6/), and i realised that i could
actually just keep sitting there and regard my surroundings without having to
check how the phone is doing, what the time is, or being in any sort of hurry or
pressure to get something done.

Yes, i love input, i love doing things, but i need to get better at purposefully
_not_ doing things. Preferrably, not doing anything except just being.

**Edit:** Not sure if this is a good or a bad thing, but i seem to have
forgotten to actually post this update during my vacation :). Now i'm back at
work and trying to keep the good vacay vibes alive.
