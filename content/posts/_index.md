---
title: "Learning in public"
featured_image: '/learning.jpg'
description: |
  Curing my incompetence, one step at the time, in front of an audience.
menu: main
weight: 20
show_reading_time: true
---
