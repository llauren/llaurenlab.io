---
title: "Alive in the box"
type: post
date: 2020-02-27T16:12:25+02:00
draft: false
tags:
  - public speaking
  - security
  - macaduk
categories:
  - learning
---

Lo and behold. The cat in [Schrödinger's speaker](/posts/schroedingers-speaker)
box is alive! Or in more concrete terms, i will be speaking at Macaduk this year
after all!

Yes, the Macaduk organisers had dropped my request, but then, another already
confirmed speaker had to cancel, so i'm on the stage again. And with a little
less time that i'm entirely comfortable with.

Like last time, i'm going to talk about security, but this time from a more
humane perspective. The working title of my talk is _Security for humans_ --
let's see how long that'll stick :)
