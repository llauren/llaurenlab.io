---
title: "Almost Famous"
type: post
date: 2022-04-22T09:30:09+03:00
featured_image: "https://live.staticflickr.com/65535/51971904359_3af9c0aabb_3k.jpg"
image_by: "[llauren](https://www.flickr.com/photos/llauren/)"
image_name: "[Poster boy](https://flic.kr/p/2nbzAux)"
tags:
  - public speaking
  - MacADUK
  - podcast
---

The fame! The fortune! The _almost!_ It's more than my little brain can handle!
<!--more-->

After my [talk at MacADUK](https://youtu.be/N6xy91hyPJo) on Security for humans,
i was approached by a gentleman at dataJar who kindly asked whether i would
write a guest post on [their blog](https://datajar.co.uk/blog/). Being awestruck
and easily persuaded, i said sure! So i crafted a write-up to explain what i
really talked about[^question] on my way home from Brighton, sent it to
[our](https://reaktor.com) marketing team for copy editing, and it's now been
sitting in my text editor while i suffer from getting-it-done-paralysis. Do or
do not. There is no try. Yeah right.
[Pareto](https://en.wikipedia.org/wiki/Pareto_principle) and [Tim
Urban](https://www.ted.com/talks/tim_urban_inside_the_mind_of_a_master_procrastinator?language=en)
would have a collective laugh.

I presented the same talk at the Finnish MacAdmins' miniconference a few weeks
later (it went better this time)[^miikkali]. After the talk, i was approached by
the IT manager at a large Nordic architect house, asking whether i'd be
interested in talking to them about human nature and how it relates to IT
security. I said sure and he asked how much i wanted. I was flabberghasted and
said anything between a coffee and a million will be fine, thank you very much.

And now, the icing on the cake. I was approached by none other than the adorable
[Charles Edge](https://krypted.com/), asking whether i'd like to appear on the
[MacAdmins' Podcast](https://podcast.macadmins.org/)! The honour! The horror!
I'm not sure _how_ i can handle _this!_  I've blogged about the [difficulties
being interviewed](/posts/interviewing-difficulties/), basically saying that i
need more practice before i'm any good, so either the MacAdmins' Podcast folks
haven't read the post or they're truly out of people to interview.

There's a saying in Swedish, _värp först och kackla sen_, which translates to
"lay [your eggs] first and cackle later". Truth is, i don't mind if any of these
opportunities tank -- just the vibes that i've been asked makes me all warm and
fuzzy inside :) . Sure, i'll provide if the folks asking are serious, and the
blog post will always appear [here](/) if it's not welcome on the larger arena.
But i don't need the fame and fortune itself. Just the prospect will do.

[^question]: I guess that might been the question dataJar was also
    struggling to answer

[^miikkali]: Actually, it was very well received! Thank you Miikkali for the
    kind _and_ useful words!
