---
type: post
title: "Blogging on an iPad (the non-Apple way)"
date: 2019-09-19T22:54:05+03:00
draft: false
tags:
- ipad
- git
- gitlab

---

Thers’s this thing in the Apple admins community called _The Apple Way._ 
It’s a half-humourous, half-cynical and just half-given-up way of saying that on
an Apple device, you can do things the way Apple intended, or you can do it the
hard way (but regretting you did). I publish my blog the hard way.

My blog is built using a static website generator [^hugo]. Under the hood, it’s
a bunch of markdown files, configuration files, styles and stuff, which the
generator converts into a website. The nifty thing is that the files i just
mentioned live in version control on Gitlab, and when i save (“push”) a blog
post to Gitlab, this kicks off the website generator software which blurts out
my site and publishes it, all on Gitlab.

It’s all very hacky, and it’s not the Apple Way.

To properly write a new blog post, i’d use Hugo on my computer to create an
empty post with metadata for the post‘s title, date, tags and draft status.
I then edit the post, which is a markdown text file, with a text editor [^atom].

Now there might be a text editor for the iPad with a git backend, but i don’t
have one. Gitlab has a thing called the web editor, but it really doesn’t play
well with the iPad. The focus goes all weird. Sometimes i can write text,
sometimes only use the arrow keys and backspace. Sometimes i can edit text
i’ve written, sometimes not. Having all of these work at the same time is
kind of a miracle.

So no, blogging the hackish way using an iPad without any other tools than
the web ide is not the way to go. Also, since there is no way to create a
new blank post with the metadata filled in, i first need to copy the metadata
from another post, create an empty file, paste in the metadata header, edit
all the fields (with arrow keys that don’t play well) and try not to lose
my good sense of humour while doing so.

This post was written that way. It wasn’t very enjoyable.
Now looking for a good text editor with git support for the iPad.

*Update!* I have now found two code editors which might solve my use case,
[Working Copy](https://workingcopyapp.com) and [Buffer Editor](https://buffereditor.com/),
Working Copy can be downloaded for free but requires a purchase if you actually
want to push your changes after a ten day trial period. Buffer Editor wants your
money up front, so i didn't try it yet. This was written on Working Copy,
and i'll write more the app soon.

[^hugo]: Namely, [Hugo](https://gohugo.io)

[^atom]: Usually Atom, or vim.
