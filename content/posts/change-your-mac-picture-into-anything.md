---
title: "Change Your Mac Profile Picture Into Anything"
date: 2018-09-19T15:12:50+03:00
tags:
- macos
- tips
- picture
category: mac
type: post
---

So you're tired of the standard profile pictures, don't want to use the
camera, don't have a Mac Server? Here's a fairly simple workaround to
use any picture as your profile picture.<!--more-->

Locate or download a picture. Import it to the Photos app by dragging
it into the app window. Open System Preferences and enter Users &amp;
Groups. Click the user and then the profile picture. It'll say "Edit"
when hovered upon. Choose "Photos" as your source -- which indeed is
the "Photos" app and not your `~/Pictures` folder. Select the newly
imported image, click Next, zoom and crop to your liking, and click
Save. That's it.

{{< figure src="/img/change-macos-profile-picture.png"
    title="Changing the macOS User profile picture"
>}}

(Yeah, i know it's a bit silly.)
