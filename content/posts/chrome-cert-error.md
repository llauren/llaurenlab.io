---
title: "TIL: Chrome Certificate errors can be useful"
type: post
date: 2022-07-18T17:35:05+03:00
tags:
  - certificate
  - TLS
  - TIL
---

Today i learned, quite by accident, that the Chrome certificate error page can
be quite useful! If you get an `NET::ERR_CERT_something-or-the-other` error
message, you can click that `NET::ERR_CERT...` code. Your cursor won't change,
indicating that the text is clickable, but it will display the problematic
certificate or cert chain in PEM format.

You can then copy the certificate. On your terminal, invoke the spell

```
pbpaste | openssl x509 -noout -text
```

(replacing `pbpaste` with whatever command on Linux or Windows to paste the
contents of the clipboard if so needed)
