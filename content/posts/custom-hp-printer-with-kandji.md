---
title: "Custom HP Printer With Kandji"
type: post
date: 2021-11-18T16:06:30+02:00
draft: false
tags:
  - Kandji
  - HP
  - MDM
  - sysadmin
  - printers
---

## UPDATE! HOLD YOUR HORSES! THIS MIGHT NOT WORK! STILL REQUIRES MORE INVESTIGATION! DO NOT USE! AT LEAST NOT IN PRODUCTION! I AM SO SORRY! IT WAS MY FAULT!

**Update on the update! I found the problem and i will post about that soon. The article below is part of the solution, but it will require another trick. Now please read on, at your own risk :)**

HP has a strange way of (not) distributing proper drivers for their printers. This is oddly true with reasonably modern "enterprise" printers. To install one of those, you either need the [HP Printer Drivers 5.1 for macOS](https://support.apple.com/kb/dl1888?locale=en_US) (outdated for 12.0.1) or the even less desirable [HP Easy Start](https://support.hp.com/gb-en/drivers/selfservice/hp-color-laserjet-enterprise-cm4540-mfp-series/4073004) package. HP does not distribute drivers for single printers, at least for printers of that generation. Things might have changed, and the current trend is to use "driverless" AirPrint printers, but for the rest of us who already have paid several tons of cash for existing printers, that's not really an option.

HP Easy Start is a big and clumsy package at around half a gigabyte really isn't viable for enterprise use. Also, i was not able to find visible traces of any printer drivers within. And since there are no drivers, you can't really install them with an MDM solution like Kandji.

If your [printer is listed here](https://support.apple.com/en-gb/HT201465), there's a solution. You'll need a terminal and a wee bit of fearlessness.

Start by downloading the HP Printer Drivers. It's a .dmg disk image. Open it by double clicking or with the `open` command. Now some terminal magic:

```
mkdir -p ~/Downloads/HPmacOSdrivers
pkgutil --expand --expand /Volumes/HP_PrinterSupportManual/HewlettPackardPrinterDrivers.pkg ~/Downloads/HPmacOSdrivers
cd ~/Downloads/HPmacOSdrivers
```

Inside the package, there is a huge file named `Payload`, which in fact is a gzip'd tar file. Let's crack that payload open.

```
tar xzf HewlettPackardPrinterDrivers.pkg/Payload
```

Now let's `find` our printer driver. For the sake of argument, let's say it's a HP Color LaserJet CM4540 MFP. I'll look for the model of my printer like so:

```
find . -iname '*CM4540*'
```

If you're in luck, this will turn up two files named just as your printer; one `.icns` and one `.gz`. The second one contains our printer driver. Copy these (substituting the file name for whatever's appropriate) to the `HPmacOSdrivers` directory.

If you didn't find any files, try experimenting with what you are `find`ing. If you still don't find any files, it's like those drivers aren't within that distribution.

To deploy custom printers with custom drivers using Kandji, you need to distribute the `.ppd` file uncompressed, and named precisely as _the CUPS name_ of the printer, plus the `.ppd` suffix. Let's open up that gibberish with code:

```
cp "Library/Printers/PPDs/Contents/Resources/HP Color LaserJet CM4540 MFP.gz" hq_printer_3.ppd.gz
cp "Library/Printers/hp/Icons/HP Color LaserJet CM4540 MFP.icns" hp_cm4540.icns
gunzip printer.ppd.gz
```

Here, the CUPS name of the printer will be `hq_printer_3`. This will be important in a while.

Now, let's prettify things. Open up the printer icon file in Preview:

```
open -A Preview hp_cm4540.icns
```

...and export the first icon as a `.png` file From `File > Export...`. Make sure to change the file format to PNG and the destination directory to `~/Downloads/HPmacOSdrivers`. Save as `hp_cm4540.png`.[^unimportant]

[^unimportant]: The name is actually unimportant; i just used this name to avoid using spaces in filenames
      which makes the documentation easier to write (and follow).

Now go to Kandji. Create a Library Item of type Custom Printer. You can name the Library item whatever you like, for example "HQ Printer, 3rd floor". Click the empty icon next to the name of your Library item and upload the pretty png you created above.

Assign the item to whatever Blueprint(s) you see fit. Set it, for now at least, to Install on-demand from Self Service (you can automate it once you've tested it). Choose a category, say Utilities, and click the Pencil icon to customise the Self service item as you wish.

Give the printer a nice and pretty Display Name. This is what it'll say on the Mac's Printers list once it's installed. Enter the printer CUPS name. Above i've used the `hq_printer_3` but you can use whatever is appropriate as long as you are consistent. Give its location (HQ, 3rd floor) and address URI. Depending on your printer settings, it might might start with `ipp://` or `lpd://` (old school admins will probably use lpd while hipster admins probably use ipp) and continue with the DNS hostname or the IP address of the printer.

Now click to upload the printer driver. I repeat, the name of the printer driver must be the same as the CUPS name plus `.ppd`, so in this case it would be `hq_printer_3.ppd`. In the final section, Don't provide any printer files.

Press save. Take a deep breath.

Your printer files could also be in the dreadful HP Easy Start pack. Running it will install a bunch more of printer drivers, which live in `/Library/Printers`. Sey i'm looking for drivers to the HP Color LaserJet FlowMFP M682. Let's go `find`ing!

```
cd /Library/Printers
find . -iname '*m682*'
```

The print driver and icon files have slightly different names here, but the idea is the same.

```
cp "PPDs/Contents/Resources/HP Color LaserJet MFP M681 M682.gz" ~/Downloads/HPmacOSdrivers/hq_printer_4.ppd.gz
cp "hp/Icons/HP Color LaserJet MFP M681 M682.icns" ~/Downloads/HPmacOSdrivers
cd ~/Downloads/HPmacOSdrivers
gunzip hq_printer_4.ppd.gz
open -a Preview *M682.incs
```

The rest is exactly as above. Ask me on Slack if you got stuck!

Happy distributing!
