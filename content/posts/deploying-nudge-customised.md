---
title: "Deploying Nudge - Customised!"
type: post
date: 2023-02-19T21:49:56+02:00
featured_image: "https://live.staticflickr.com/2422/4011062640_7ebc0325bb_c_d.jpg"
image_name: "[Nudge](https://flic.kr/p/77rKbq)"
image_by: "[Ben Terret](https://flickr.com/photos/benterrett/)"
tags:
  - nudge
  - sysadmin
  - macsysadmin
  - macos
---

Nudge is a piece of software which _encourages_ the user to please update their outdated macOS.
I suggest you read (lots) more about it on the [Nudge wiki](https://github.com/macadmins/nudge/wiki)
and elsewhere on the interwebs if you aren't as familiar with it as you would like. Suffice to say,
Nudge should be configured to suit your organisation and part of this configuration changes with
each released version of macOS. As goes with any Admin tool, Nudge should be configured as kindly
and thoughtfully as possible.

You can customise the Nudge behaviour by creating (at least) one configuration file and distribute
it either along with the Nudge package or over the web. You can even distribute the plain vanilla
Nudge if you have a reliable way to send the config file(s) to your client machines, but for our
organisation, i wanted to create a custom Nudge package. The benefits here are that i can control
exactly what i send along with the Nudge installer -- which includes custom Reaktor graphics instead
of the Nudge default Apple logo -- and i get to learn a bunch of new stuff to me.

> For many seasoned Admins, re-packaging packages is an everyday operation. This was my first.
> I'm learning, in public.

To make a custom version of Nudge, i'm going to use Munkipkg. There are other ways, but this is
mine, at least today.

BTW, Nudge and Munkipkg are all free software, thanks to the MacAdmins community.

## Step zero: Get Nudge Suite and Munki

Nudge comes in two flavours: plain Nudge and Nudge with logging, known as Nudge Suite. If you like
logging, chose Nudge Suite.

Fetch the latest [Nudge](https://github.com/macadmins/nudge/releases/) (Suite or not), which at the
time of writing this is 1.1.11.81465. Save it into your `~/Downloads` directory.

Using `git clone`, fetch the latest [Munkipkg](https://github.com/munki/munki-pkg). Let's assume you
cloned it into `~/git/munki/munki-pkg`.

## Step one: Unpack and prepare

`cd ~/build` or to wherever you build your stuff.

`~/git/munki/munki-pkg/munkipkg --import ~/Downloads/Nudge_Suite-1.1.11.81465.pkg Nudge_Suite-1.1.11.81465`
(replacing paths and version numbers as appropriate).

This creates an error, but no worries, we'll fix that.

```shell
ERROR: Nudge_Suite/build-info.plist key 'postinstall_action' has illegal value: b'none'
ERROR: Legal values are: ['none', 'logout', 'restart']
munkipkg: Sync successful: no changes needed.
munkipkg: Created new package project at Nudge_Suite
```

`cd Nudge_Suite-1.1.11.81465`

Edit the file `build-info.plist` and change the lines 18-20

```xml
	<data>
	bm9uZQ==
	</data>
```

(which is `none` encoded in base64) ... to

```xml
    <string>none</string>
```

To make Nudge look more like "our" tool, i wanted to use company graphics instead of
the Apple logo that is the Nudge default.

```shell
mkdir payload/Library/Reaktor
cp <path-to-your-graphics>/payload/Library/Reaktor/Reaktor_logo_dark.png 
cp <path-to-your-graphics>/payload/Library/Reaktor/Reaktor_logo_light.png 
```

I'll configure Nudge to use these graphics later.

## Step Two: Customise

Nudge's behaviour is controlled by two files:

* the scheduler
* the config file

The scheduler is the LaunchAgent plist (because it runs as the logged-in user), stored here as
`payload/Library/LaunchAgents/com.github.macadmins.Nudge.plist`. It will cause the Nudge app to
run twice an hour.

The config file is a JSON file, described [here](https://github.com/macadmins/nudge/wiki/Preferences).
By default, it resides in `/Library/Preferences/com.github.macadmins.Nudge.json` but it can also live
on the Internet, which solves many problems (and creates another few, but that's life of a
sysadmin to you). The really important things to configure here is which versions of macOS you want
Nudge to control and by when you want the update to be installed. You can customise an insane number
of things with the config file, but i'll only change some texts and the graphics.

This is the part which says:

* If you're running macOS 13, we want you to install macOS 13.2.1 by the 22nd of February 2022
* If you're running macOS 12, we want you to install macOS 12.6.3 by that same date

```json
{
    "osVersionRequirements": [
        {
            "requiredMinimumOSVersion": "13.2.1",
            "requiredInstallationDate": "2023-02-22T00:00:00Z",
            "targetedOSVersionsRule": "13"
        },
        {
            "requiredMinimumOSVersion": "12.6.3",
            "requiredInstallationDate": "2023-02-22T00:00:00Z",
            "targetedOSVersionsRule": "12"
        }
    ],
```

The following directives thell Nudge to use the Reaktor graphics (included in the package):

```json
    "userInterface": {
        "iconDarkPath": "/Library/Reaktor/Reaktor_R-Symbol_White.png",
        "iconLightPath": "/Library/Reaktor/Reaktor_R-Symbol_Black.png",
```

And finally, the following lines change the text to be more Reaktorian (or at least, more like me :)

```json
        "updateElements": [
            {
                "_language": "en",
                "mainHeader": "Your Mac needs updating",
                "subHeader": "A friendly reminder from your Admins",
                "actionButtonText": "Update Mac",
                "mainContentNote": "Hi there!",
                "mainContentText": "Keeping your Mac up to date is important. It shows you care for your computer, your peers, your customer and even for your Reaktor Admins. It's also your responsibility.\n\nTo start the update process, click the 'Update Mac' button above.\n\nFor more help, see the #please-update-your-mac channel on Reaktor Slack.",
                "secondaryQuitButtonText": "Not now"
            }
        ]
    }
}
```

Because i don't want to re-package Nudge each time there's a new macOS release[^release], i want to
remote control what macOS versions Nudge encourages. To do this, the Nudge.json file must be reachable
over the Interwebs. Cooler people than me set up a web server or use GitLab/GitHub and automate the
heck out of things. I just share the config through Google Drive, which is a little involved, but works.

[^release]: I still need to repackage Nudge for each time _Nudge_ is updated.

> ### Side quest: Sharing raw files using Google Drive
> 
> The trick is to create a new "public" shared drive (you don't want to share from your secret stash),
> create a directory for your nudge stuff, create the `nudge.json` file within, share the file so that
> anyone on the Internet can view the contents (danger, Will Robinson!) and copy that link. It'll look
> something like `https://drive.google.com/file/d/<ID>/view?usp=something`. This will be the link to the
> Google document, with all its chrome and fluff. The `<ID>` part is a long string of characters, and
> you'll need it to create a link to the actual raw file, the URL to which is
> `https://drive.google.com/uc?id=<ID>` (there are no `<` or `>` charaters in the ID).

For extra points, feed this long link into an URL shortener that you control, and you end
up with something pretty like `https://u.ri.fi/nudgeconfig`. Yeah, that one exists. You can
see what's within if you want.

Now, edit the LaunchAgent file in `payload/Library/LaunchAgents/com.github.macadmins.Nudge.plist`.
Take a look at the lines 15-20 or so that they point to your config file out on the Internets. Mine say
something along the lines of

```xml
       <key>ProgramArguments</key>
       <array>
           <string>/Applications/Utilities/Nudge.app/Contents/MacOS/Nudge</string>
           <string>-json-url</string>
           <string>https://u.ri.fi/nudgeconfig</string>
       </array>
```

## Step two: Pack and deploy!

I did have a previous step two where i signed the package, but this text is long enough as it
is, so i'll write a separate (shorter) post about that.

Anyway, now it's just to re-pack your customised Nudge package.

```shell
cd ~/build
~/git/munki/munki-pkg/munkipkg --build Nudge_Suite-1.1.11.81465
```

Copy this rebuilt Nudge Suite package to your MDM or Munki, or whatever you use to distribute
software -- and distribute away!

Remember to be kind. Nudge will bother people with outdated operating systems nearly immediately.
Test on a smaller group of folks. Tell your peeps in advance what's going to happen. Give enough
update window; Nudge will only look to the calendar and wave its hands, it won't care if a user
has been on vacation and missed the deadline. It doesn't care if the user is in the middle of a
meeting. Being considerate is one of the key duties of a sysadmin.

Questions? Find me as `llauren` on the [MacAdmins' Slack](https://macadmins.slack.com)! (or even
better, talk with the Nudgies on the MacAdmin's Slack, because they know this stuff way better
than i do!)
