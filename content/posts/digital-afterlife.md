---
type: post
title: "Digital Afterlife"
date: 2019-07-29T18:20:05+03:00
tags:
- afterlife
- "social media"
- testament
---

One day i'm going to die, which is something i might as well come to terms with.
It's not like i'm in any kind of hurry to die; i rather like living and i'd
prefer to postpone the inevitable end as much as is easily possible. But after
i'm dead, i'll leave a big digital heap of memories behind me, and that's what
this post is about: making those memories accessible to my near and dear, if
that's what they want.

I have so many outlets for my digital memories. Some i use more, most i use
less. Many are tracked without much of my active input, which is really
convenient (and could take my privacy preferences into better account). There
are the obvious social networks. I take photographs with my phone, some of which
i publish and most of which i don't. My whereabouts, biometrics and activities
are stored for posterity. I have email spanning back since my first forays into
the field of electronically delivered missives.[^email] I have things and thoughts
written in docs, notes and blog posts. I have well over half a thousand entries
in my personal password manager, leading to services i use daily and digital
doors long forgotten. And herein likes a problem.

If i don't even know what accounts i've registered, how on Earth are my family
going to make any sense of it, even if they wanted? I mean, it's not that i know
if they eventually will, but at least, i should leave the decision up to them.
And that's the first decision.

When someone i care about dies, i have two thoughts. _Why?_, and _I wish i still
had asked/could say this or that_. When i die, i want my family to have the
possibility to access my digital memories and tracks. This is my choice and you
can choose this or differently. You can choose to let your near and dear inherit
some of your memories, all of it, or none of it. But if you ignore it, you're
just creating a derelict wasteland.[^wasteland] It's probably kind of weird and
not very sensitive to those still in life to get a suggestion to befriend you on
a social network once you're gone.

Some social networks have this under at least some sort of control. You can
assign somebody to be the warden of your memories on Facebook who may convert
your profile into a memorial (Settings &raquo; Memorialisation settings &raquo;
Legacy contact). If i don't use any Google services for a few months, Google
will send an "account activity reminder" message out to me, and if i don't
answer in a month, i'm probaby either dead or incapacitated --a fairly sensible
assumption-- and hand over the master key to my wife (Google Account &raquo;
Data and personalisation &raquo; Make a plan for your account). And i can set
her as my emergency contact in that password manager. Twitter doesn't have this
provision, but a family member may ask the account to be closed and removed if
you become dead or incapacitated. I suppose the dead don't tweet. Instagram lies
somewhere between Facebook and Twitter; an account can be memorialised once a
lawful representative of the deceased has provided that person's birth _and_
death certificate. Really. Seems like an awful lot of hassle if you're grieving
a lost one. I say the social media companies still have work to do.

Even automatically logged information can be interesting. Maybe my sleep or
heart patterns will tell something which can answer some questions. Maybe my
location data will tell a story, if somebody wants to hear. Who knows. It's not
up to me. I won't be around.

But the real question is what to do about access? What about finding out what
those accounts are? What about actually understanding what some of those do
(like our home automation and network)? What about understanding my intentions
(if anyone's interested)? Sure the password list will help, but what's really
missing is a kind of a Digital Testament, which would contain some greetings,
state what you think could or should be done with the digital memories, some
kind of a list what there is, along with instructions on how to operate. The key
issue here is that you are a nice and caring person, even if you're dead. It's
bad enough for the people left behind; you might as well lend a hand.

What i don't know is where to store your digital testament. The store should be
accessible to those it is for, and it should be somewhere permanent enough that
it doesn't disappear like so many other digital services over the year. But
where? Under version control? On a blockchain? On a USB stick somewhere?
Integrated with the account metadata in the password manager? My best idea so
far is to have a link, or the document itself, in the email Google will send out
to your loved one, but you probably want more than one copy. And you want to
update the document every once in a while. I'm not much of a documenter myself,
but this one is pretty important.

Doing this isn't an indication that you're planning on dying anytime soon. It
just means you're doing something for the ones you love, ahead of time.

[^email]: That's not entirely true. My FidoNet mails are very much gone forever.

[^wasteland]:  (which, of course, may also be what you want, but make sure that's your decision and not due to your ignorance)
