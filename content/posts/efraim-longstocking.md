---
type: post
title: "Efraim Longstocking"
date: 2019-06-24T15:16:24+03:00
draft: false
tags: ['theatre', 'Pippi Långstrump']
categories: ['public performance']
---

I can't believe i haven't written about my largest project in ages! Yes, i've
taken the stage in an amateur children's theatre production, Pippi Långstrump,
which is Swedish for Pippi Longstocking. I'm playing Pippi's dad Efraim and a
small role as Starke Adolf who i don't know the English name for. But it's this
strongman who performs at the market (originally, at the circus) and gets
wrestled down by the strongest girl in the world.

It's been insanely fun and very, very intensive, both emotionally and
physically. I'm not much of a by-heart-learner, so learning my lines was a bit
of a challenge, but way more so was learning choreography. I am not a dancer. I
dance like a panda. And after half a year of theatre, i still don't dance any
better than a panda. Also, dancing, lifting and spinning Pippi around and
singing at the same time _really_ takes my breath, which is why i've started to
take the bicycle half the way to work instead of going only by train or by car.

I didn't originally intend to apply for a role. The whole thing started with my
son not really caring to go to a music camp this year and i suggested the
alternative to apply to the local summer theatre Finns. And as they wanted a
parent's signature and comments on the application form, i said i could help
with something simple like carrying the crates or something. To this, the
producer Jonna asked whether i might be interested in a role instead, and, well,
here we are. But i'm happy she did and i'm happy we got the roles. My son plays
Tommy, the neighbour kid, and he's doing an awesome job.

Half of the cast is children, half are adults and whatever you should call 17
year olds. One fun thing is that on production, we are all colleagues and age
doesn't matter. What matters is your attitude and how you do your thing. Just
the fact that i can sit and talk with a twelve year old, in total seriousness
(or total madness, which also often is the case), is something i value
immensely.

We've played through June, and unfortunately, there's only this week left. After
that, i'm left with a gaping social void and a well needed vacation. I kinda
miss my crew already. They are just the bestest.

If you're fluent in Swedish, you can read all about it on [Efraim's Logg](/efraim/).
