---
title: "End Of Disqussion"
type: post
date: 2022-04-22T11:37:58+03:00
featured_image: "https://live.staticflickr.com/5310/5631426072_85053deae5_c_d.jpg"
image_by: "[Dennis Wong](https://flickr.com/photos/denniswong/)"
image_name: "[Please keep quiet! 請保持安靜!](https://flic.kr/p/9zCwLY)"
tags:
  - blog
  - disqus
---

I got a message from Graham Pugh from the Mac Admins' Slack that my blog was
lagging. This was a bit surprising because it is served from Gitlab Pages, which
should have all the resources in the world. So i looked under the hood. And i
was surprised.

<!--more-->

I am not a developer but i opened up the Developer Tools on Chrome (and so can
you; see `View` > `Developer` > `Developer toools`) and opened up the `Network`
tab. Then i navigated to my [Almost famous](/posts/almost-famous) post. And i
raised an eyebrow.  Loading a page from my blog requires 48 requests or 1.5 MB
of data. Most of them are to resources i don't really recognise;
`recommendations.10022a97...`, `lounge.bundle.59fbd11ef...` and so forth. Is it
my theme that is requesting all this? Is it something else on the page? Is my
page _-gasp!-_ spreading spyware?

The developer tools also allow me to look into these resources, many of which
are JavaScripts and many of which contained clues to why there are so many
unexpected resources included with my page. The finger pointed at Disqus, the
commenting plugin i'd been using on my blog ever since moving to Hugo (which is
a static blog engine and does not support commenting).

I removed Disqus from my blog config and sent it to Gitlab. A minute later:

|          | With Disqus | Without Disqus |
|----------|-------------|----------------|
| Requests |      48     |          6     |
| Data     |     1.5 MB  |        667 kB  |
| Load time |    2.08 s  |       1.04 s   |

The load time has halved. The amount of data is 44% of what it used to be. The
requests are down from 48 to a nice 6. And there is probably _way_ less spyware
on my page now.

Thank you Graham, and you're welcome everyone!
