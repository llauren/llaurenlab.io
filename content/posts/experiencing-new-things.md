---
type: post
title: "Experiencing New Things"
date: 2018-09-07T00:16:12+03:00
draft: talse
tags: ['mindfulness', 'centos', 'freeIPA', 'beer', 'artificial intelligence']
categories: ['me']
---

Today i've experienced several things, many of them new. Some new in a
meaningful way, some mainly trivial. And in a broader scope of things, mostly
trivial really.<!--more-->

The day began with the graduation event of the course [Elements of Artificial
Intelligence](https://www.elementsofai.com/). It's an on line course, giving you
enough basics on the subject that you can hold an informed discussion about the
topic. It's free, and it's good. It's also created and maintained by Reaktor,
the company i work at, and the University of Helsinki. And i thoroughly
recommend you take the course if you have any interest in the subject. It took
me twelve (or eighteen?) hours to complete, required some reading, some paper
and pen level mathematics and some thinking. I guess i could have used some
more time on it, and maybe i'll go and revisit the course when the follow-up
course comes. Ninety thousand people have already signed up, and currently maybe
a tenth of those have completed it. I really hope more will work the way through!

**But anyway** -- the course graduation event was today. The President of
Finland, Sauli Niinistö, held the commencement speech. Nearly a thousand people
were present. Two things came to mind: i don't think i've ever been this close
to the President. In fact, i'm not even sure i've seen him "live" more than once
in passing. But more so, the event was both dignigied and laid back at the same
time. There was no security or identity check at the gates -- we are, after all,
in Finland -- and President Niinistö moved with one aide-de-camp and one
security guard. I didn't even notice there was a security guard before they
were leaving the building, when i saw a translucent curled wire behind somebody's
ear.

Later, at work, i got myself acquainted with [FreeIPA](https://www.freeipa.org/).
FreeIPA is like Active Directory for Linux, but without Group Policies (sorry!)
and the license fees. It's a Directory service for keeping your users (and
their groups, passwords and some settings), it's a Certificate Authority and a
DNS (Domain Name Server). And probably a few more things. I started out looking
at a few videos on FreeIPA (namely [this](https://youtu.be/LCT6E8-j8m0) and
[this](https://youtu.be/VLhNcirKFDs)) and taking the
[FreeIPA Workshop](https://github.com/freeipa/freeipa-workshop) where i actually
installed a simple FreeIPA environment (albeit with "proper" virtual machines,
not Vagrant boxes on my laptop). And i did it on CentOS.

All in all, it was a surprisingly good experience.

I've been a Ubuntu/Debian guy for the last ten years or so, leaving the RedHat
camp and Mandrake Linux shortly after Ubuntu started happening. It's been kind
of a "religious" (or at least, statementy) thing, and i've become at ease with
the platform. So stepping into CentOS (Community _Enterprise_ Operating System)
was a bit of a leap for me. And now it actually feels kind of peaceful to have
a foot in "both" Linucen[^plural] Debian and RedHat (and yes, i do realise there
are more than two but i really don't have the capacity to get into Tails, Mint,
Suse, Arch and Slackware .. and OpenBSD).

I even tried installing the FreeIPA Client on one of my Ubuntu boxes, and it
sort of half-worked, maybe because i had some settings off. Maybe i'll look at
this tomorrow.

Finally, i tried doing just one thing. I had a beer, a
[BrewDog Choco Libre](https://www.brewdog.com/item/2863/BrewDog/Choco-Libre.html),
and nothig else. Usually i tend to social-network it
[on Untappd](https://untappd.com/user/llauren) but having recently read the
words of wisdom

> All of humanity's problems stem from man's inability to sit quietly in a room alone.  
  - Blaise Pascal

, i decided to give it a try.

The beer was exquisite. Smooth. Coffee and chocolate. The sweet taste of
alcohol. And ... was there plum, and maybe tobacco there ...? And i must log
this on Untappd now and _damn_ it is hard not to do anything else at the same
time! I guess this is what the mindfulness people and the beginning meditators
struggle with, to just keep it to one point at the time.

I decided i'll allow myself to think and to reflect, not only to experience. I
can do this another time. So i thought about work, about stuff that goes into
[Munki](https://munki.org), about FreeIPA, about all the stuff i should do at
work ... and how i shouldn't actually work on _working_ on them next time at
work, but try to organise my stuff and make sense of it all. And that i should
work at working _less_ -- in the sense of spending less time at work but still
get work done. Seven and a half hours of work should be enough, and really, i
have very little of actual _life_ outside work.

One solution would be to actually be at work between 9--17. Show up at nine,
not ten (which i kind of like to do, because i get an hour of morning time to
read the paper or just laze), and leave at five, not six, seven, or eight. I
could try this for a week and see how it goes. I guess it too will be hard.

Expect me to report on it. Or if i don't, just bug me to report on it. In the
name of transparency and accountability.

[^plural]: _Linucen_ is a plural form of Linux, because
[it's fun](http://catb.org/jargon/html/overgeneralization.html). Also, the
plural of octopus is octopi and the plural of ananas is ananae, except you
call them pineapples in English.
