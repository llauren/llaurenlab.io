---
title: "Forty-four Daily Pictures"
type: post
date: 2023-02-19T21:59:24+02:00
tags:
    - photos
featured_image: "https://live.staticflickr.com/4420/36382816075_cc5e89cdbe_c_d.jpg"
image_name: "[Glowing people](https://flic.kr/p/Xr2rEk)"
image_by: "[me](https://flickr.com/photos/llauren/)"
---

OK, so i got as far as 44 "[daily pictures](https://www.instagram.com/robin.lauren/)"
and 44 "[Daily Kelvins](https://www.instagram.com/kelvin_the_lapphund/)". Most pictures were
really shot, edited and posted during each day, but in the end i ran into two problems:

* Kelvin, while he is a really cute dog and really poses well, does look pretty much
    the same after 44 pictures. I either need to up my game significantly, give up,
    or accept that my dog looks the same on most of the pictures. He's cute, though.

* I noticed that i was stressed, and that after a streak of 44, i wasn't able to keep
    it up any longer. It wasn't enjoyable.

Now what? Well, i'm going to publish more pictures, of course. What i haven't decided
is whether to post pictures to fill the days where i didn't post (preferably with
pics that more or less match their scheduled day) or should i just pick things up
and be happy that i continue? If so, what happens to my counter? Should i jump from
44/365 to 50/365 or should i re-number to 45/355? All very confusing. I guess i need
to sleep on it.

So what have i learned? That pictures that look [sad and gloomy](https://www.instagram.com/p/CoalCGNLja5/)
get very few Instagram likes, compared to pictures of [sunsets](https://www.instagram.com/p/CoDZJUoLTVW/),
[winter landscapes](https://www.instagram.com/p/CnAUhjWrmg6/?utm_source=ig_web_copy_link) or
[backstage pictures from the theatre](https://www.instagram.com/p/CnThMj0MPsD/?utm_source=ig_web_button_share_sheet).
