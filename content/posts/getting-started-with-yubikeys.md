+++
type = "post"
title = "Getting Started With Yubikeys"
date = 2018-09-22T13:51:11+03:00
draft = false
tags = ["2fa", "yubikey", "google", "lastpass", "github"]
categories = ["security"]
+++

I bought a set of Yubikeys to figure out what the fuss was all about. Getting
started was surprisingly painless and i now have a new authentication factor
for Google, Github and Lastpass.<!--more-->

I've been using 2FA for a bunch of services for quite some time now. My most
used second factor for authentication is the Google Authenticator app, which
runs on a mobile device that is _not_ my phone. Hence, it actually is a second
factor when it comes to authenticating stuff on my phone.

For Google, i clicked "Add security key" on my Google Account's
[Two Step Verification](https://myaccount.google.com/signinoptions/two-step-verification)
page. I then inserted the yubikey 4c nano, ignored the keyboard identification
dialog box and tapped the key with my finger. A little later, i gave the key a
friendly name, and the key was registered.

The procedure is very similar with with Lastpass and Github, so i won't repeat
it here. The only difference is that i didn't unplug and re-plug the yubikey.

There are still a few things that bother me.

I'm not the most orderly and organised person. This means i will probably find
myself where my authentication key is not. I'll lose it, forget it at home, or
whatever. How can i then log in? All these services i've used with 2FA so far
still list Google Authenticator as a second factor, so i suppose adding a new
key didn't remove the old one. And Youbico recommends installing _at least_ one
more backup key, which i will, and which is something i'll touch a few lines
down in another context.

The Yubikey easily only protects _applications_ i use, not the computers
themselves. To protect my computer login, or the operating system, i'll need
to install a new PAM module. That's a tad scary even for a geek like me (what
if i lock myself out?) but for most folks, that is nowhere near anything
realistic. I'm waiting for the day macOS will support 2FA keys out of the box,
but i'm not holding my breath.

Logging in to stuff like switches and firewalls would be really nifty too.
Not really expecting it to happen any time soon though.

Then a practical issue. The key that i installed is a Yubikey nano, which is
meant to live in the USB port. That means my authentication factor is just as
safe as my computer. If the computer is stolen, so is my key. Doesn't seem like
added security to me, though in all honesty, it's pretty convenient. But if i
remove the nano-key from my computer, i'm pretty sure to lose it fairly quickly.

I have exactly one computer i use with a USB C port.  This means i need to
register another key for the computers i use that have only legacy USB A
connectors. Another reason to create more yubikeys.

Furthermore, i have a phone, and while it does have a USB C port, i won't be
walking around with a yubikey nano plugged into it. For that, there's an NFC
compatible Yubikey. It's kind of a pity that none of my computers support NFC.

So all in all, to have some sort of hardware based 2FA security, i'll need a
"daily" USB A key with NFC, another "daily" USB C key, and one backup key which
can be USB A and can be used with an adapter dongle. And that's the minimum.
I'm not yet convinced what the proper mode of operation is with these keys,
should i leave a key plugged in to the computer i use, which means i know i'll
have it with me but also know both will get misplaced at the same time, or
should i walk with a authenticator key in my keychain, which really isn't all
too convenient to plugg into my computer each time i need to authenticate with
a second factor. That said, i don't yet know how often i will need to 2FA.
Every 30 days? On each git push?

There are a lot more applications that i can protect with my yubikeys, but
i'll need to kick the tires with what i've installed so far. I'll write a
follow-up if things go really sour :)
