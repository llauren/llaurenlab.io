---
title: "Gitlab Pages and Automatic Letsencrypt Certificate"
type: post
date: 2019-08-31T15:46:34+03:00
draft: false
tags: ["ssl", "tls", "certificates", "gitlab pages", "hugo"]
categories: ["meta"]
---

It happened again. I let this site's certificate expire. I'd blame my faulty
calendar entry, but it's really just a case of my procrastination.

As i was getting ready to read my article on how to renew the [Letsencrypt
certificate the hard way](/posts/renew-gitlab-cert-the-hard-way/), again, i saw
that Gitlab Pages had something new to surprise me with.

Automatic Letsencrypt Certificate Management. By the beard of Odin.

Here are the steps to enable that

* On Gitlab, logged in and viewing the project of your pages, go to `Settings
  > Pages`.
* If you, like me, let your certificate expire, delete the domain that expired
  (don't delete the pages themselves, that would be counterproductive).
* Click the green New domain button on the top right.
* Enter your domain name and turn on Automatic certificate management using
  Let's Encrypt.
* Click the green Create New Domain button.
* Now you'll need to edit a TXT record on your DNS (or at your DNS provider).
  Please note that the value of the TXT record includes the bit
  `gitlab-pages-verification-code=` and not just the code itself. I talk from
  experience :)
* The Gitlab user interface will have a slightly alarming red notification
  button that says Unverified. Wait a minute or five, then press the
  arrow-go-round thingy next to it. Your domain should now be verified.
* There is no step eight.
* Well, actuall there is. Once you have verified that you have a certificate
  in place, go back to `Settings > Pages` and tick the box Force HTTPS.
  This'll take a few whiles, so you might want to keep this tab open until
  you've done it.

Granted, there are a few steps there, but it's still a lot more painless than
renewing your certificate manually every few months, and you'll save yourself a
few facepalms for letting your certificate go old. And since it's now automated,
you don't have to worry again, ever! Whee, what a win!
