---
type: post
title: "Going home, not back"
description: "A lament on the lacking keyboard usability of the iPad"
date: 2019-09-25T08:17:09+03:00
tags:
- ipad
- keyboard

---

Using an iPad with a hard keyboard is kinda nifty. There's `Cmd+Tab` to switch
between apps, `Cmd+Space` to search. Arrow keys, with the Cmd and Alt modifiers
work as they should. And while i've been searching for a Back button on the iPad,
you can go Home using the keyboard with the shortcut `Cmd+H`. Nifty.

Coming from an Android world, a universal Back button is a central navigation
device that i'm truly missing. While there is a left caret in many apps, it's not
really the same thing. Now there might still be a Back button on iPadOS, with or
without a hard keyboard, but i haven't found one.

There also is no Escape key to close things with, like if you open Search
(`Cmd+Space`) and decide you don't want to search anyway, you'll have to delete
the stuff you wrote in the search box, then press Enter, as searching for nothing
will mean the same as cancelling the search. Or you have to lift your hand from
the keyboard and poke Cancel on the screen. There's a principle in Usability that
says you should keep the stuff you need near at hand. Or in this case, near the
hand, which is on the keyboard.