---
type: "post"
title: "Help, It's Summer!"
date: 2018-06-26T19:54:49+03:00
lastmod: 2018-06-26T19:54:49+03:00
description: "I'm on holiday and i don't know how to handle it"
draft: false
keywords: ["spin-down", "holiday"]
topics: ["self"]
tags: ["self"]
---

The good news is that summer holiday in Finland is four weeks, or as i
have so many extra hours worked in, five. Five weeks paid holiday. Whoa.

<!--more-->

Now what? I've been on high gear for so long that i have no idea what to
do with all this liberty. I even passed the [Elements of AI](https://www.elementsofai.com/)
course (and got a [certificate](https://certificates.mooc.fi/validate/fon5yhura0j)!).

I thought about learning Puppet 5. I thought about learning Python. I thought
about learning Redis. Or how to talk to Redis with Python. All these things
that _might_ be useful for work but i never have the time to learn when i'm
actually at work.

And then i realised that i don't have to, and that it's probably just better
that i do things which are as far from work as i can. At least for a week or
two. Then i can ... _maybe_ take a look at techy stuff again.

Instead, i can read. I can photograph. I can sing. I can watch stuff on Netflix
that i've put on my list and not touched since. I can be outside. I can go for
a jog (i don't even know how long it was since i last jogged regularly). I can
even blog, just not about techy things, at least not those that are about tech
i use at work. Because it's summer and because i can. I won't say it's easy,
but sometimes spinning down from work can be ... work. Just not the type i'm 
paid for.

And i'm paid for being recharged when this summer break is over.

Oh, and did you already take a look at [this video](https://www.youtube.com/watch?v=eN-EJMAde6w)?
Heck, i might even cut together another one, and it's way crazier! :)

