---
title: Customising my Hugo site
description: It's just like being a coder!
featured_image: "https://live.staticflickr.com/65535/50540963643_4b7f04b6e0_c_d.jpg"
image_name: "[code](https://flic.kr/p/2k18DET)"
image_by: "[Amanda Wentworth](https://flickr.com/photos/146278242@N03/)"
image_license: CC0
type: post
date: 2022-01-15T17:04:55+02:00
draft: false
tags:
  - hugo
  - coding
  - shower thoughts
---

I'm not very good at throwing old stuff away. On the Internet, storage space is
nearly infinite (in comparison to my brain). So i rather archive than delete.
I'm like a squirrel in that way. Thus, i decided that i wanted to keep my old
[Now](/now) posts, to turn the Now section into something akin to a blog. How
easy i though it was.

This site is built on [Hugo](https://gohugo.io), a static web site generator.
You can publish most any kind of static site with Hugo, most notably
documentation sites and blogs. Hugo relies on
[Themes](https://themes.gohugo.io), which are fairly complicated beasts that
turn your Markdown text files into glorious and organised web pages.

I use a fairly popular theme called [Ananke](https://gohugo-ananke-theme-demo.netlify.app/).

One basic assumption about Ananke pages, and Hugo pages in general, is that it
can be of a given class; a "leaf" page like a blog post (like this one!) or a
Branch page, which is a kind of an index page to a section. Typically, in Ananke
fashion, your blog would be presented as a Branch page, based on a `list`
template, from where you can navigate to any article or Leaf, generated using a
`single` template.

Well, that's not how i wanted my Now page. Thinking of it, it's not really how i
want my Blog either, but lacking the competence to change it, i've learned to
live with it. Silly me. But i wanted the Now page to display the latest post,
like it were a Leaf page,  with the possibility to navigate to earlier Now
posts. Preferably with a little menu on the side for earlier posts, and with
another box quickly explaining what this Now thing is. For that, i needed to
rewrite the whole `list` template, and it's still not quite right (including the
fact that while i like the layout more than the current `single` layout).

This was oddly reminiscent of toying with web pages at the turn of the century.
Uh, wait. Thirty years back. _(Oh goodness)_ It was possible for me to copy and
paste stuff from the `single` template, experiment around, read the [Tachyons
documentation](https://tachyons.io/docs/) and watch the results. One _really_
nifty thing Hugo comes with is a local server with instant live updates on the
client side. You start `hugo serve` on your computer and watch the magic happen
on `http://localhost:1313`, with navigation and everything working like a
miracle. The only thing i had to do to get it to work with the theme was to `git
submodule update --init themes/ananke`, even though i had the submodule
configured from before (on another computer, if that's of any relevance).
Anyway, seeing live results, and the occasional error messages, makes it feel
like i'm a coder :D
