---
title: "How Not to Lock Yourself Out of Two Factor Authentication"
type: post
date: 2021-04-26T10:00:10+03:00
draft: false
featured_image: https://live.staticflickr.com/7362/10197469343_4ca01f411e_c_d.jpg
image_name: "[porta rústica](https://flic.kr/p/gx7GmX)"
image_by: "[Cícero R. C. Omena](https://www.flickr.com/photos/10015563@N03/)"
tags:
  - security
  - MFA
  - 2FA
summary: |
  Adding a second layer of authentication to your account is essential for your
  security, but like having a lock on your door, it can lock you out when things
  go wrong. You really need to think of the spare keys before that happens.
---

# Some background for the uninvited

Two Factor Authentication (2FA) and it's more serious cousing _Multi-Factor
Authentication_ (MFA) is a way of protecting your access in more than one way.
In the traditional world, it means that only you have access to your credit card
and its four digit PIN, only you know which bicycle or home is yours and carry
the key to the lock, that only you carry your ID or password and look
sufficiently like the person on the photo[^idphoto]. These days, things are a
little bit more complicated.

These days, your data is what makes your phone or computer _yours_. All the
email, all your work, all those documents, the instant messages, the social
media ... this combination of stuff is what makes your gadgets uniquely yours.
Which is why they're worth protecting. And to protect them better than just
hiding them in your drawer or using a password. This we do with requiring more
than one way, or in technospeak, authentication factor.[^authentication]

There are several popular ways of MFA, some of which suck. Many web services
will send you an email to click to prove you're you. What this proves is that
you have an email address, you were able to spell it out correctly, and that
you're able to respond to email. This is really popular with password resets.

Several services require you to authenticate using your phone number (Twitter,
WhatsApp). Some will send you an SMS with an authentication string when you want
to log in somewhere, or send you your new password. This is really unfortunately
if somebody's just nicked your phone or your SIM card. I'd like to say that SMS
authentication is better than nothing, but it's not much better.

You can have an authenticator app on your phone or a "hardware token", which
basically is just a thing that looks like an USB memory key but won't store your
docs or pictures. Those are the best, but they do come with a few drawbacks,
caveats and gotchas.

# The problem with 2FA and how to cope with it

A 2FA token is meant to uniquely prove that you are you. That's all nice and
dandy until you lose hold of your 2FA token. If you're a Messy Person like me,
you are bound to lose your keys one day (you'll probably find them again, but in
between you'll be pretty miserable), you'll break your phone or it just runs out
of battery when it shouldn't. Or you're travelling (once the Plague is over) or
at the countryside house doing remote work, the 2FA fob is at the bottom of the
lake and your backup token is 340 km away. Well, oopsie-doo. You have a problem.

It's not so much a problem with 2FA itself, it's how you deal with things once
you no longer have access to that second factor. It's really just a feature of
2FA, and 2FA doesn't really discern whether it you or somebody else that doesn't
have your second authentication factor.[^2faproblem]

Here's a tip that lets me sleep tighter. Think what would happen if you would
lose your 2FA method. And then think of how you’d solve it. Then test your
thesis. Learn and refine. Security folks call this Threat Modelling, and it's
really quite a fun exercise if you take it that way.

Have several 2FA methods. I have more than one hardware "token"; one that i
carry with me and one which is at home. I’m pretty sure i know where my backup
token is... I also use authenticator software on a mobile device (tip: it's not
my daily driver phone[^authapp]). Sadly it seems you can’t have more than one
authenticator software configured per service, so if i lose the phone which
carries my authenticator app, i have work ahead.

Finally, all services with 2FA provide backup codes. Copy these into your
password manager – but realise that since your password manager also is using
2FA (or will be, right after you've read this), you might be locked out of in a
really twisted, multi-layered fashion. For this, consider having the backup
codes for your password manager on actual paper, saved somewhere at home,
without the header “Password Manager 2FA Backup Codes” … just in case.

[^idphoto]: As a side note, if you look like your passport picture, you're
    probably too sick to travel.

[^authentication]: Authentication is the way you prove that you are you. This is
    different from _identification_ and _authorisation_. Identification: "Me:
    Hi, i'm Robin. Computer: Prove it." Authentication: "Me: *types password*.
    Computer: Looks legit." Authorisation: "Me: Show me my calendar. Computer:
    Sure, i can show you _your_ calendar."

[^2faproblem]: So to put it bluntly, _you_ are the 2FA problem. But then again,
    computers and algorithms aren't known for their sense of empathy or
    reasoning, so there's no need to take it personally. There are a whole lot
    of professionals and academics who work really hard trying to get humans and
    computers interact better together. They just haven't solved it yet.

[^authapp]: It's not because i'm paranoid, it's because i recently changed
    phones and i've just been too lazy to weaken my security and move important
    things related to security on my main phone. Here, laziness proved good for
    security!
