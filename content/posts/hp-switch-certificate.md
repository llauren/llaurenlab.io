---
type: "post"
title: "HP Switch Certificate"
date: 2018-03-02T11:40:13+02:00
#lastmod: 2019-04-30T12:36:00+03:00
lastmod: 2019-06-07T10:31:59+00:00
description: "HOWTO create a SSL/TLS certificate for your HP 2530 switch"
draft: false
keywords: ["hp", "switch", "ssl", "tls", "certificate"]
topics: ["sysadmin"]
tags: ["hp", "switch", "network", "config", "certificates", "ssl", "tls"]
---

Setting up a SSL certificate on a HP (HPE/Aruba) 2530 switch is not hard or complicated but it is frustrating when you try to figure out exactly how it's done. There's some terminology that isn't immediately obvious and the switch tends to be a bit picky about how things are done.

So with that said, let's dive in.

I'll assume here that you have a functioning PKI on your site, or that you're able to get a certificate request signed somehow. I'll briefly touch on self signed certs local to the switch, but that's just a kluge until you have "proper" PKI up and running.

I'll also assume that you can have a command line connection to your switch, either by console or ssh. Make that connection now. Connect, authenticate, and enter the magic word `config`.

Also: friends don't let friends use telnet.

In the code below, substitute the stuff `IN_CAPITAL_LETTERS` to what's relevant in your environment. For example, my Trust Anchor and Identity Profile names are the hostname of the switch, a dash, and `ta` or `id` respectively.

You should, eventually, have a DNS entry for your switch, but you don't need DNS support on the switch itself.

## The Trust Anchor

The Trust Anchor is HP's term to describe the certificates to trust. In regular
openssl parlance, this the trusted certificate chain, ie your root certificate and
any intermediate certificate(s) bundled together. When creating a certificate,
you'll need to associate it with a Trust Anchor. To do so, first create one:
(substitute `YOUR_TRUST_ANCHOR` below with something nice like `my-pki` or
`trust-chain`)

```
crypto pki ta-profile YOUR_TRUST_ANCHOR
```

You need to get the certificate chain related to the trust anchor profile
transferred to the switch. This is, rather inexplicably, done using `sftp`. If you
don't have a PKI but intend to use a self signed certificate on the switch (for
now), skip this step.

From the switch, enter the following incantation to fetch your trusted certificate
chain `PATH_TO_CHAIN.pem` from `HOST`

```
copy sftp ta-certificate YOUR_TRUST_ANCHOR USERNAME@HOST PATH_TO_CERT_CHAIN.pem
```

<blockquote>
<em>In all honesty, this is a lot easier using the Traditional Web UI (it's not
yet on the modern "NG" Web UI, but i'm sure it will soon). However, to access it,
you'd need to log in to the Web UI over unprotected HTTP and you don't usually
want that. As a workaround, you could _first_ roll a self-signed certificate (more
on that below), then create a proper PKI-backed certificate. Or you could continue
the command line way (which is why you're here anyway).</em>
</blockquote>

## Identity Profile

Optionally, you'll next create an _identity profile_ to describe your switch's
identity for the certificate. Here, pressing TAB for autofill is your friend.
Though i've split the command on several lines, you'll need to have them on one
line for the spell to work. Country should be the two letter (ISO 3166-1) country
code, like FI for Finland.

```
crypto pki identity-profile YOUR_IDENTITY_PROFILE subject common-name SWITCH_FQDN
  org 'THE_NAME_OF_YOUR_ORGANISATION' org-unit 'YOUR_ORG_UNIT'
  locality 'WHERE_YOU_ARE_LOCATED' state 'WHERE_THAT_IS_LOCATED' country 'FO'
```

The other option to creating an identity profile is to include all the identity
information when creating the certificate request.

## The Certificate Request

Now you'll create a certificate request. This has less stuff to type. Still, and
again, do write the following on a single line.

```
crypto pki create-csr certificate-name YOUR_CERT_NAME
  ta-profile YOUR_TRUST_ANCHOR key-size 2048
  valid-start TODAY_IN_MM/DD/YYYY_FORMAT valid-end FUTURE_DATE_IN_SAME_FORMAT
```

If you didn't create an identity profile above, include `subject common-name
SWITCH_FQDN org NAME_OF_YOUR_ORG ...` in the csr request incantation above.

If you don't have a proper PKI on your site, instead enter the following spell and
be done with it:

```
crypto pki enroll-self-signed certificate-name YOUR_CERT_NAME key-size 2048
  valid-start TODAY_IN_MM/DD/YYYY_FORMAT valid-end FUTURE_DATE_IN_SAME_FORMAT
```

### Pro tip! Don't press enter yet!

You can actually get the the certificate request file sent to your computer by
`sftp` magic using the meta-incantation

```
copy command-output 'crypto pki  create-csr certificate-name YOUR_CERT_NAME
  ta-profile YOUR_TRUST_ANCHOR (...)' sftp USERNAME@HOST SWITCH_FQDN.csr
```

Now you can press Enter! :)

## Sign the CSR

Assuming you're still here, you followed the `create-csr` command above, not the
`enroll-self-signed` path. Copy-paste the certificate request (`-----BEGIN
CERTIFICATE REQUEST-----` etc etc) to your PKI, sign it, and receive a nice
certificate in `.pem` format

Now issue the command

```
crypto pki install-signed-certificate
```

(If you didn't install the trust anchor cert chain above, this command will fail.)

Paste the certificate (`-----BEGIN CERTIFICATE-----` etc etc) and press enter
(twice if needed). If there were no errors (sorry, no hurrahs given), you should
be able to check that you have a certificate in place by

```
show crypto pki local-certificate
```

## Enable SSL (TLS) on your switch

```
web-management ssl
```

While you're at it, disable plain http from the switch:

```
no web-management plaintext
```

## Congratulate yourself

Yay!
