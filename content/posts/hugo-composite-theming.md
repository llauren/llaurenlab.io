+++
type = "post"
title = "Hugo Composite Theming"
date = 2018-07-08T18:56:07+03:00
draft = false
tags = ["hugo", "css", "theme"]
categories = ["meta"]
+++

Just recently, Hugo, the web site generator i use for this site, got a new
feature: [Composite theming](https://gohugo.io/themes/theme-components/).
This means that you can build themes on other themes, or even do weird things
like multi theme sites. I decided to try theme compositing just to change the
fonts of my site. <!--more-->

This is my first attempt to composite theming with Hugo, so i may be very wrong
in my assumptions, but the standard disclaimer is "it works on my computer"
(which isn't entirely right, as it also works on the Gitlab CI pipeline).

I first took a browse at [Google Fonts](https://fonts.google.com/) and decided
upon [Source Sans Pro](https://fonts.google.com/specimen/Source+Sans+Pro) for
my body text and bold [Titillium Web](https://fonts.google.com/specimen/Titillium+Web)
for my headings. And then went to work.

In my site's `config.toml`, i changed `theme = "kiera"` to `theme = ["my",
"kiera"]` where `my` is what'll become my customisations, or _overrides_, as i
like to think of them at this time. Then:

```
cd themes
mkdir -p my/static/css
mkdir -p my/layouts/{partials,_default}
cp kiera/static/css/styles.css my/static/css
cp kiera/layouts/partials/header_includes.html my/layouts/partials
cp kiera/layouts/partials/aside.html my/layouts/partials
```

I then edited "my" copy of `styles.css`; added the line
```
@import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro:regular,bold,regular+italic|Titillium+Web:regular,bold');
```

...and search-replaced the original fonts to the ones of my choosing. I also
increased the `line-height` value for text in the `#container main`. Lo and
behold, i now have new fonts! (well, at least i have them on my computer; i can't
actually see the final results before i publish this :)

I then changed some colours so that i get blue on link hovers and code with grey
background instead of yellow or red.

To indulge my OCD (of which i have very little), i removed the link to the old
fonts from `header_includes.html` and edited the `.Date.Format` for the posts'
publish dates in the `aside.html` and `list.html` files.

And that's about it! My first theme customisation! If you want to do something
similar, you'll need to check the theme you're editing since themes aren't
_that_ standardised just yet. Still, this article should be able to serve as a
starting point. Happy hacking :)
