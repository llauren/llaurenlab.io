---
title: "I Found a Bug"
type: post
date: 2022-12-19T13:34:02+02:00
featured_image: "https://live.staticflickr.com/65535/52572813031_1483bd9abc_c_d.jpg"
image_name: "[It's a bug](https://flic.kr/p/2o6FpGT)"
image_by: "[Robin Laurén](https://www.flickr.com/photos/llauren/)"
tags:
  - sysadmin
  - coding
  - Apple
---

I found a bug. I investigated it and [reported it](https://github.com/endoflife-date/release-data/issues/47).
I created a fix. And now it's gone. These things are everyday for the coder but not for me the sysadmin, who
did get a little bit of a kick out of this.
<!--more-->

At work, i have this system which checks what operating system versions our Macs
are running, compares those versions to the latest macOS versions supported, and
sends out Slack messages to those who need to update. I'm not much of a coder,
but i'm kinda satisfied with the work i've done.

The macOS version comes from [a web API](https://endoflife.date/api/macos.json)
on endoflife.date, which gets its data from, among many sources, the Apple
Support article [HT201222](https://support.apple.com/en-us/HT201222), which
outlines the latest versions of macOS and the other Apple operating systems.
Unfortunately, HT201222 had a small but pesky bug. You see, on line 485 in the
HTML source, the code went

```html
</tr><tr><td><a href="https://support.apple.com/kb/HT213532">macOS Ventura&nbsp;13.1</a></td>
```

See the `&nbsp;` between `Ventura` and `13.1`? Yeah, that's the problem. And i
had no idea how to fix it.

Option one: Contact Apple. Except the support article doesn't relly provide a
way to provide a bug report. But hey, we have AppleCare Enterprise (ACE), so i
give them a ring, report the problem, and half an hour later, the same support
engineer gets back to me and tells me where to file the bug! Nice. Not sure if
Apple _will_ ever fix it, but time will tell.

Option two: Add bug-compatibility to the code. The web API gets its data using a
[script](https://github.com/endoflife-date/release-data/blob/main/src/apple.py).
The script scans the support page for what seems to be macOS version info,
including that line 485. I tried like silly to change the search criteria but it
turned out i was searching for the wrong thing. The regexp should be changed from

```python
        r"macOS [\w ]+ (?P<version>\d{2}(?:\.\d+)+)",
```

to

```python
        r"macOS [\w\s]+(?P<version>\d{2}(?:\.\d+)+)",
```

...because the HTML understander in the code will (correctly, i might add)
convert the `&nbsp;` entity into a UNICODE non-breaking space, which is not the
same thing as a `" "` or even a `&nbsp;`. But lo and behold, `\s` in the regex
will find both regular and non-breaking spaces.

It's a damn small change, but it changes everything, and the code is now
bug-compatible with Apple's release notes. And tomorrow at this time, when the
change has been accepted to `main`, some automations will run and the API will
report the correct version, and with that, those of our peeps who have an
outdated macOS will get their update notifications!

Post scriptum: In the end, it wasn't me who submitted or pull-requested the
updated code, but i did create [the issue](https://github.com/endoflife-date/release-data/issues/47)
on GitHub, i investigated what was bugging and i found and crafted the solution. Next, i
suppose, i need to learn how to create pull requests too so that
[other hackers don't get all the glory](https://github.com/endoflife-date/release-data/pull/48) :)
