---
title: "In I Dimman"
type: post
date: 2019-08-12T23:08:21+03:00
category: radio
tags:
- radio
- prog
- "progressive rock"
---

It is now twenty years to the day since the last show of my radio programme
"Into the mist" (or "In i dimman", in Swedish) aired. The show ran for two years
on [Yle X3M](https://x3m.yle.fi)[^x3m], first weekly and then every two weeks,
until it was pulled for being too far off the mainstream. It was the best job
i'd had, and i still miss it fondly.

Into the mist played Progressive rock. Prog. The stuff that was way uncool back
then and now is celebrated by artists Tool, Steven Wilson and Devin Townsend. I
got to be a monkey on air, i got to speak with my heroes (sometimes live, face
to face), i got free records and concerts, i got to play the music i love and i
even got paid! Sure, not much, but i was a student back then and any money was
good money.

But the real kicker was the autonomy to do what i loved. It was my programme and
i could do whatever i wished with it. I chose every single song played and why
it was played. I contacted record labels. I arranged interviews. I edited them.
I was behind the mixer when the show was on air. I was my own producer. It was
wonderful. I met Genesis, had Pekka Pohjola in the studio and got to interview
all of Dream Theater (including John Myung who at the time was rather shy ...
_and_ they recognised me on their next gig), Ian Anderson of Jethro Tull, Neal
and Alan Morse of Spock's Beard, Roine Stolt of The Flower Kings, Piotr
Grudzinski of Riverside, the guys from Ritual, Anekdoten, Änglagård... I got to
go on tour with Ageness! Yeah, the list is longer, but you get the drift. As a
small time just-a-guy, this was big stuff to me. Huge thanks to all these wonderful
people who gave up their time to talk with me!

During my last season, i had two live gigs in the giant M2 studio at Yle,
something no "popular radio" would do. M2 was reserved for Serious Music, ie
classical music. I had the Finnish bands Ageness and Kuha. playing. Because i
could, and because my (actual) producer agreed it was a nifty idea. On that last
show, with that last band, i even had a live audience in the M2 studio. Big
times! We even pulled of a gag (borrowed from U2) where we ordered a pizza to
the studio (for free, as Pizza Taxi would get free exposure :). Oh yes, good
times.

People who know me, know my favourite artists are Marillion and Fish, so i got
to promote them by playing their music and doing interviews with them. I
remember one time when, for some odd reason, Fish actually came down to the
radio station (that was in the days when Fish got some actual promotion) and i
got to properly surprise a friend and colleague by coming down to the smoking
room (because those were the days when you could smoke at work, inside, but not
in public areas).

The last song i played was _Sunsets on empire_, by Fish.

> Sunsets on empire -- is this really the end?

Two colleagues from x3m has suggested i'd bring back Into the mist on Lähiradio,
a "citizen's radio" (so even more public than public radio). Who knows, i might!
I just have this theatre project i need to pull through. Or maybe Yle gets
nostalgic :)

All my recordings from those shows are on DAT and MiniDisc, which means i can't
listen to them again unless i buy some legacy equipment. It sure would be fun to
relive those shows.

[^x3m]: Strangely enough, X3M doesn't seem to have proper web pages anymore‽
