---
title: "On the Difficulties Being Interviewed"
type: post
date: 2022-03-09T15:51:01+02:00
tags:
  - interviews
  - practice
---

I was recently a guest on the Reaktor Podcast [Fork, Pull, Merge,
Push](https://www.reaktor.com/forkpullmergepush/), giving my thoughts on being a
sysadmin. This is the second time i've been interviewed in recent times, and i
can tell you that it's a whole 'nother beast from any other forms of
communication. You might think talking with your colleagues —which it in all
essence was— would prepare you for being _interviewed_ by your colleagues, but
boy, it was a rollercoaster. Yes, some things went alright, whereas some went
down the can, and back again.

I tried to keep my sentences short and my answers concise (which is a challenge
itself for me!) but when coming to a full stop, i wasn't sure i had answered
well enough or if i should go back and clarify. There is a saying in Finnish
that _clarifying things will only make it worse_, so i didn't. I was stumbling
and mispronouncing words. There were times i got so enthused i forgot the
question i was answering. At times, it felt disastrous, and i _hope_ the editor
will do a merciful job in cutting my ramblings into something coherent.

You might think my historical background in radio _doing interviews_ would have
prepared me, but no. Thinking of what to ask is not the same as answering. Sure,
i knew that if i stumbled on words, it's easier for the editor if i pause
briefly, then rewind and say the whole sentence again, because it's really
tedious to edit stuff within sentences (for added points, try to say the
sentence with the same tone as you just stumbled with, so that the edit will
sound more natural).

Anyways. From a sample size of _N=2_, i can tell you that **being interviewed is
a skill**. Like any other skill, **it requires preparation and practice to
master**. Or that's what i assume, as i had neither. And with that insight, i
have discovered another skill i don't have ... _yet!_ I'm not sure exactly how
to fix it, but if you'd like practice your interviewing skills, we could help
each other improve :)
