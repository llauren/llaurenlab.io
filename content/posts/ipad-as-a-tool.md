---
title: iPad as a tool
type: post
date: 2019-09-19T20:33:35+03:00
lastmod: 2019-09-20T17:32:35+03:00
draft: false
tags:
  - ipad

---

Two happy things coincided this week. Apple released a new iPad that you can use
an actual keyboard with, and i had a meeting with my Apple SMEs. So i voiced an
earlier thought i'd had, "can you get any actual work done with this?". So the
other one asked "Do you want to try?" Yes! Absolutely! Shure dang! (just as soon
as i return the MacBook i borrowed from them :) [^macbook]

So what do i need to do work? Not much, i'd assume. A terminal and a browser. A
proper keyboard. And git. A proper editor would be nice. Do they have Atom for
iPad? Or vim? [^vim]

Opening my goodie bag, i realise it's not a 10" 7th gen iPad, but an iPad Pro.
Boom! Stoked! Not only does that mean a lot more oomph, but that i can charge it
with a proper (USB C) charger anywhere i roam. I'm an Android guy and don't have
anything that requires a Lightning plug to charge. And that brings me to the
next thing. I've actually never used an iDevice more than in quick passing. This
is going to be a new endeavour for me.

Half an hour later.

Okay. It's cool. It's shiny. The iPad sort of magically sticks to the
keyboard-cover. The pencil sticks to the side of the iPad with a satisfying
click. The keyboard doesn't need charging at all, it just connects to the iPad
and off you go. The pencil charges by induction when it's snapped to the iPad.

Oh that pencil. I just tried it quickly with the Notes app. It's smooth. It's
responsive. It's... instant! I'm going to like this experiment.

**Update!** After some help from my colleague, i even found how to get the
Nordic characters ÅÄÖ on the hard keyboard. It wasn't totally obvious.
`Settings` > `General` > `Keyboard` > `Keyboards`. Add Swedish or Finnish keyboard.
`Settings` >  `Keyboard` > `Hardware keyboard` > `Swedish` (or Finnish) > `Swedish
- Pro` (or Finnish). _Then_, while in text input mode in an app, press the globe key
at the bottom left of the keyboard, which usually lets you select between
your default keyboard (whatever your install language was) and Emoji. Whop and
behold, there's a new option there! Swedish (or Finnish)! And now you can type
with your favourite character set!

Of course, the spell checker goes all boinkers and assumes your English writing
now is misspelled Swedish... I suppose. In this sense, the Android system is a
bit more clever, as it figures out i've changed languages mid-sentence if need
be.

I still miss a "Home" key, so i can get back to whatever home screen there is
without lifting my hands from the keyboard. Cmd-Space works[^sherlock], which is
neat, and it's possible that'll cover the lack of a Home key. An Escape key there
is not.

[^macbook]: I'll skip the story what i'd have to go through to wipe and re-install
    the MacBook, which is kinda silly since wiping and re-installing MacBooks is
    in of my description...

[^vim]: That much i know that the iPad keyboard does not have an Escape key

[^sherlock]: For you non-macoholics out there, Cmd-Space is the most useful key
        combination on a Mac. It opens up a quick search functionality called
        _Spotlight_ which searches both apps, filenames, file contents, stuff on the
        web (if you allow it), and practically any other content on your device.
