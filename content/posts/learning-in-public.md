---
type: post
title: "Learning in Public"
date: 2019-04-23T10:53:02+03:00
draft: false
tags: ["meta", "learning", "swyx", "nzd", "no zero-days"]
categories: ["me"]
---

I listened to a [swell podcast episode from freeCodeCamp](https://www.freecodecamp.org/news/shawn-wang-podcast-interview/)
where [Shawn _swyx_ Wang](https://twitter.com/swyx) told an insane amount of great things. I really urge you to listen to
the episode even if it's two hours long (there's at least four hours of content within those two hours!) to hear about
practical work discipline ("No Zero-days"), of which i have way too little, and the concept of "Lerning in public". Among
several other things (like salary negotiation, react.js and maintaining on-line comminities).

Learning in public means you're telling an account of what you are learning, as you are learning. You don't wait until you
are perfect. You acknowledge that you will be wrong (at times) and you invite both those who are at your stage of learning
to take part of your journey, and those with more expertise to _correct you_ on your misconseptions. And you make a deal
with yourself that if what you've written earlier turns out to be hogwash, that you go and correct or amend your earlier
post.

There are several benefits to learning in public. One is that for a hundred people who are learning something, maybe less
than one actually writes about it. Another is that you'll draw positive attention to yourself by being honest and
accountable about it. And as a side effect to that, you'll (or you _may_) get the attention of those who know more than
you and who can help you along the way. Yeah it's scary to put yourself out there, not only admitting but actually
_demonstrating_ to the world that you are incompetent, but it's also honest. And i have a whole blog post about the
bliss of incompetence coming up soon. Just gotta write it first :D

As my kudos to Shawn Wang, i'm replacing my previous subheading _Exploring my way out of Imposter Syndrome_ of this blog
into the much more positive and actionable _Learning in public_. I can't submit to the No Zero-days ethos just yet.

No Zero-days is not a security thing, but rather that you promise to do at least _something_ each day to advance yourself
to whatever goal you've set. Doesn't matter how much, as long as it's something. Do not stop the momentum. No days of zero
progress. It's commendable, recommendable, and scary. I can't say i'm ready to commit to NZD just yet, but i do feel its
imporance. It's something one of my favourite authors [Neil Gaiman](http://www.neilgaiman.com/) recommends strongly _("If
you want to be a better writer, you should write. Every day.")_ and it's something i should have understood when i wrote
my thesis. Writing it out like this now makes me slightly embarassed that i sneakily don't commit to NZD. I think the
reason here is that i haven't really set out my goals. I have a few vague ideas, but they're more of the improvement kind,
not of the measurable kind (lose more weight, sleep better, become a better public speaker, learn more devops, become more
organised, and for goodness sake, _blog_ more!) which may very well be the reason why i don't have them.

But one step at the time. And this is one of them.
