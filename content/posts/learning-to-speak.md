---
type: post
title: "Learning to Speak"
date: 2019-04-26T22:12:17+03:00
draft: false
tags: ["public speaking", "security"]
categories: ["learning"]
---

Today i learned (the hard way) that doing something by hand that you usually get
through automation can be really tedious. So when you Automate Everything, make
sure there's an easy manual override when needed. But that's for another post.

I've given my second ever presentation, which was at the Finnish Mac Admins'
meet-up. The presentation was essentially the same as i did in London earlier
this year, about "Government Level Security (while maintaining your sanity and
humanity)", but in Finnish. As would be expected, i went over time, even though
i had a longer slot this time than at Macaduk. This may be a manifestation of
[Parkinson's Law](https://en.wikipedia.org/wiki/Parkinson%27s_law) (that every
project will use up all its given resources, ie deadlines are your friend), or
that i just talk too much (_and_ have an optimism bias).

_Note to self: remove half of the presentation (but how can i do that?!)_

My presentation really has two topics: High-security requirements in the public
sector, and how to create a secure environment without everybody going bonkers
(including you) or heading up the barricades in protest. Or just going paralysed
because you just can't get anything done because of all this security. It's
really this second topic that fascinates me. I could still set the stage by
describing some of the fairly stringent but oddly sensible requirements, but
spend a lot more time on how this may sound insane but just requires some work
and attitude.

On the plus side, i was not nervous, panic struck or lost for words, except when
i was supposed to list the names of the protection levels (restricted,
confidential, secret, top secret) in Finnish. Shoulda written some more speaker
notes :)

So now i just need to prepare another talk!
