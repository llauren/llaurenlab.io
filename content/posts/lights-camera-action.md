---
title: "Lights, Camera, Action!"
type: post
date: 2023-02-04T22:45:38+02:00
featured_image: "https://live.staticflickr.com/4127/5180501119_8e49117f07_c_d.jpg"
image_name: "[Camera lens](https://flic.kr/p/8TMquP)"
image_by: "[Dominique Godbout](https://flickr.com/photos/benterrett/)"
tags:
  - camera
  - photography
---
Alright, so i've challenged myself to take, edit and publish two pictures a day, one "regular" of whatever happens around me, and one "doggo" with our lapphund Kelvin. Here's the thing. It's really hard to think of a new thing to photograph each day, at least for me. Since i work Monday to Friday and have theatre on Saturday, and it's winter time, most of my waking hours are *dark!* And once i've managed to photograph something, it's really hard to remove all those pictures that aren't bad but didn't pass the publish threshold -- the pictures that are _good enough_ but didn't make the cut. The old me would have saved all of them. The new me really tries hard to remove at least most of them, thinking that the future me will be happier this way. But it's hard.

With my regular feed, i find many of my pictures are about the interplay with darkness and light. I have a long love for pictures taken in the dark -- or at least in insufficient light -- and with my new camera, i find it's a lot more ... possible! I actually shot a picture of an old house near us in near total darkness, handheld and in ISO 40k! The picture came out grainy but very legible and much to my surprise, with very "daytimey" colours, just a bit washed out. The picture didn't make the cut, but maybe i can change my routine and allow myself to sometimes post pictures i didn't take that same day.

What's getting less hard by the day is to edit the pictures and publish them once i have something shot. My workflow is to shoot as many pictures i need but as few that i can get by with. I import them into Lightroom (Classic or "Modern" -- i like the Classic version more, but i'm trying to wrap my head around the new version), i do a quick pick/discard triage, with the "unflagged" ones being the most problematic. I try to narrow down my pics to a minimum set of shots that are different enough to each other, and, if possible, fit together. It's easier with the Kelvin pictures, as they often form a story arc of a moment.

I then tentatively decide on the picture format and try to stick to 4:5 because the less wide the picture is, the bigger it will be on Instagram. This is tricky to me, since i tend to like wide shots but hey, i'm here to learn. Often, i'm drawn into the 5:7 aspect ratio, but really, i'm trying to get the hang of the square 1:1. This very much doesn't come naturally to me, who've photographed 16:9 with the telephone for so many years.

I then fix the lighting. Sometimes i start by hitting the Auto button, which sometimes works wonders, or at least serves as some sort of inspiration, and sometime is just too much, since many of my photos were shot in fairly dark. I often use Lightroom's magic AI masking tools to darken the background, or just use linear gradient masking. I touch up both the subject and the background and try to make it look subjectively natural (so somewhere between impressionist and documentary) unless i'm feeling experimental (or just lost for inspiration) and toy with the built in filters.

I've now hopefully ended up with between one and six shots and publish them on my [regular](https://www.instagram.com/robin.lauren/) and [doggo](https://www.instagram.com/kelvin_the_lapphund/) Instagram feeds. And if i get something _really_ nice, i post it on [flickr](https://flickr.com/photos/llauren), but this has just happened once so far, and i like to have a wider aspect on flickr than i do on Instagram.

All in all, i need this work to be pretty fast so i can get it done before it's sleepy-time, so as you can guess, it's a lot of learning, in skill, routine and in habit. No wonder it feels so hard! :)
