---
title: "Macaduk 2022: Security for Humans"
type: post
date: 2022-03-29T16:15:42+01:00
featured_image: "https://live.staticflickr.com/65535/51971904359_3af9c0aabb_3k.jpg"
image_by: "[llauren](https://www.flickr.com/photos/llauren/)"
image_name: "[Poster boy](https://flic.kr/p/2nbzAux)"
tags:
  - MacADUK
  - public speaking
  - talks
---

Woo-hoo! I have just stepped off the stage at the Mac Admins' Conference 2022 in
Brighton, UK, and i have survived! My presentation on _Security for humans_ went
without much catastrophy and nearly within the allotted time. I received rather
enthusiastic applause and several people actually came up to me and told me the
the presentation was good! Even more importantly, many people have talked with
me that it indeed seems important to involve humans in the security loop.
Success!

If you were there, [here are my slides](/macaduk-2022-Lauren.pdf). If you were
not, they probably are quite useless.

If there's anything i need to learn, it is to start working ahead of time. And i
mean _radically_ ahead of time, like one COVID pandemic ahead of time ... which
technically is what i had but pretty cleanly managed to ignore. I had enough
foresight to arrive in UK on Sunday evening so i could have the whole Monday to
practice on my talk and then go for pre-conference get togethers. Little did i
know.

Little did i plan that on Monday, all of it, i'd be putting my talk together,
rehearsing it, compacting it, creating slides, creating speaker notes, missing
beer with my conference-chums, rehearsing it again, and falling over in bed at
02:30 in the morning-night, then waking up at 07 to rehearse again. Yeah, i
managed to take a walk during Monday for lunch and a coffee, but it would have
been way nicer to spend some time looking at the city or meet-and-greeting folks
from the conference. Still, i'm happy i arrived as early as i did. Otherwise it
would have been a disaster.

Another flip side which comes from not doing things in time is that i don't get
to re-hash my presentation. I had an original idea which of talking about _The
Human Sysadmin_ which was transformed in to _Security for humans_, and i was
able to slightly fix the content between the first (and only) draft and the talk
that went into production. But really what i missed out on was to have a Version
Zero, giving it to a test audience, gathering feedback, looking at the video
recording, and then creating a production release. Not sure if that's how the
pros do, but based on the book [Resonate by Nancy
Duarte](https://www.duarte.com/resonate/), this is the least what a pro should
do.
