---
type: "post"
title: "Manfluenza"
date: 2018-04-03T19:51:00+03:00
lastmod: 2018-04-03T19:54:00+03:00
description: "Being sick is terrible."
draft: false
keywords: ["influenza", "manfluenza", "sick"]
topics: ["me"]
tags: ["influenza", "manfluenza", "me"]
---

I've been sick in a way that can only truly be appreciated by experiencing it.
I've had _manfluenza_, which is the male version of any disease and is by
definition worse just by the virtue of you being male. We're not as good at
handling pain. And yes, i'm joking. A bit.

I was about to write a long technical post about my illness, but i'll just lay
down the short and sweet.

<!--more-->

Day zero, at a family gathering. Though nobody would know it at the time,
several other would get the same plague as i received, so that's where it all
started. No blame though. These diseases are smarter than us.

Day one, some coughs on the way from work. Didn't think anything of it.

Second day, felt tired. Some more coughs. Had a half hour nap and felt okay
again.

Third day, my hands and feet were cold, i was exhausted and shivering. Took an
ibuprofen, which took an hour to kick in, then felt hot. More coughing.

This cold-ibuprofen-hot cycle went on for about a week. Really exhausted.
Coughing like my body wanted to eject my larynx along with a hefty dose of
slime. Thirsty, but not very hungry. Then hungry but too tired to do anything
about it. My voice was that of a giant toad or an oaken door. Thankfully, my
throat wasn't sore and my fever was really moderate. Thank you modern medicine
for that.

The bad bits were waking up during the night thinking that i've forgotten how
to breathe, then willing it to fill my lungs for a while, then falling into some
sort of half coma, and waking up in terror that i can't breathe. Really sucks
for resting.

Tried altering ibuprofen with paracetamol. Didn't help. Tried with antihistamine
if it'd be hay fever. Wasn't. The only thing that helped was rest. Much of the
time, i was just laying in bed with my eyes closed. Getting up made me dizzy.
It was impossible to read, i was not in condition to watch videos, not listening
to podcasts, not learning anything new from any kind of medium, and certainly
not blogging about it.

It's been two weeks and a bit now and i'm 85% back. Nothing hurts, but i don't
care for taking the stairs at work and the occasional cough sounds kinda nasty.
But at least i can read, watch videos again, learn a bit, and blog. More about
those things in my next post.
