+++
type = "post"
title = "The more Separator"
date = 2018-02-24T10:18:31+02:00
draft = false
tags = [ "hugo" ]
categories = [ "meta" ]
topics = [ "meta" ]
+++

**TL;DR:** To preserve html styling in summaries on hugo, use the tag `<!--amore-->` (removing the letter `a`)
in your text. <!--more-->

I was looking at the summary page, which included my previous [Hello, World!](hello)
post and thought, _well this looks crummy._ Turns out that the hugo rendering
engine strips all html from [summaries](https://gohugo.io/content-management/summaries/).
But there's a (manual) way around it, and that is to insert the html comment
`<!--more-->`, all lower case, no spaces, where you want the summary divider.
The nice side effect of this is that your html will be preserved.

I can think of a technical reason to this -- mainly that preserving well-formed
html is hard when cutting a summary -- but it feels a bit surprising anyway.
But then again, being surprised is a nice side effect of learning things.

On a self-deprecating side note, i know this isn't much of a discovery, but i
really needed something to write about.
