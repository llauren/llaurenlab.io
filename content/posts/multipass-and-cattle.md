---
title: "Multipass and Cattle"
type: post
date: 2022-02-12T17:21:12+02:00
featured_image: "https://live.staticflickr.com/6211/6258240517_a685450c0f_c_d.jpg"
image_name: "[Cows](https://flic.kr/p/ax27Sz)"
image_by: "[Oli](https://flickr.com/photos/27702659@N00/)"
tags:
  - virtualisation
  - devops
  - immutable
---

There's a way of thinking in devops, to treat your servers as cattle, not pets.
There's also a related idea of immutable servers. For us old school people who
love our servers like our children, all this sounds very weird.

But before going to why this is weird, let's take a look at those thoughts. A
pet is an animal you keep and cuddle, maintain and take to the vet, take out for
walkies and love like a family member. A cattle animal is no different from
another cattle animal. You don't get too attached to a single individual and you
can replace one with another. Now while i don't agree with this rather
respectless view on cattle --i think all animals should receive respect-- but
just replace cattle with a cubicle worker and i'm sure you can sympathise.

Immutability then, means that you don't change a server once it's installed. You
don't update it, you don't tweak it. It's read-only. Only the data storage is
written to. In an ideal world, you don't even log in to an immutable server.
I've heard of projects where a server is automatically taken down and replaced
an hour after someone `ssh`s into it. That's Next Level Immutability, but it's
also good for security. A server should do its servery things, not be a guest
house. You log in to it for debugging purposes, you write a fix ... and then you
create a _new_ server with the fix. If somebody else logs into it, it is to be
considered as tainted and should be taken down. Just in case.

This is where multipass can be handy. Once a server image has been downloaded,
spinning up a new server is quick. To configure the server -- without logging in
to it -- there are two techniques to use: `cloud-init` and configuration
management. Minimally, configuration management is scripting, but typically,
you'd use Ansible, Puppet, or something comparable. Most things could be
accomplished with either or, and to make things more convoluted, it's possible
to run Ansible, Puppet, scripts, or a bunch of other things by cloud-init. I've
just scraped the surface so the only thing i know so far is that there's a whole
lot to learn.

# A webserver out of the box

To install a (mostly) unconfigured web server, you could write something along
these lines to a config file, say, `webserver.yaml`:

```
#cloud-init

# Update packages
package_update: true
package_upgrade: true

packages:
  - nginx

write_files:
  - path: /etc/nginx/conf.d/example.com.conf
    content: |
      server {
          server_name example.com;
          listen 80;
          root /var/www;
          location / {
              try_files $uri $uri/ $uri.html =404;
          }
      }
    owner: 'nginx:nginx'
    permissions: '0640'
    defer: true

# Reboot, just in case
power_state:
  mode: reboot
  message: "Back in a while"
```

There are loads of ways to configure the server itself and that's for another
post somewhere else on the web. After all, you do want to get some files to
serve, some ways to get logs out of the server, configure TLS, the biz. But this
is squarely outside the scope of this little post.

To spin up your web server, just type `multipass launch --cloud-init
webserver.yaml`.

# Getting real

To actually be able to access your server from your computer (and your local
area network for that part), install VirtualBox on your Mac and issue the
command `sudo multipass set local.driver=virtualbox` to use VirtualBox. Using
`ifconfig`, figure out the name of your active network interface (it's the one
with an IP address -- say, it's `en0`), then launch your multipass server
instances with something like `multipass launch --network en0 --cloud-init
webserver.yaml`.

As a reminder, Multipass isn't something you want to run your production
infrastructure on. It's for development. But you can use those very same
`cloud-init` files on production virtual machines once your development
environment looks good. And i guess somebody has already thought about how to
deploy a multipass fleet to a production platform.
