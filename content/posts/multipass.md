---
title: "Multipass"
type: post
date: 2022-02-06T21:05:14+02:00
tags:
  - Linux
  - Ubuntu
  - virtualisation
  - development
featured_image: https://live.staticflickr.com/4365/36312101253_f631ca52cc_c_d.jpg
image_name: "[DSC_0903](https://flic.kr/p/XjM1Bg)"
image_by: "[Steven Lam](https://flickr.com/photos/slamto/)"
summary: |
  Multipass is a technology for spinning up Linux virtual machines on your
  desktop using simple commands on the command line.
---

So what did i learn this week? In review, it feels like nothing, but i did
actually stumble over a technology that was new to me, namely
[multipass](https://multipass.run/). Multipass is a virtualisation platform
which has a docker-like feeling. It actually runs on other virtualisation
systems, so i guess it's fair to call it _virtualisation middleware_.

Multipass makes it really easy to spin up a Linux server on your computer, using
the `multipass` binary. Fetch it from the link above. There are binaries for
Mac, Windows and (whoa!) Linux.

If you're on a Mac, type `sudo multipass set local.driver=hyperkit` to use the
native virtualisation layer. Or if you want to go deeper, fetch and install
VirtualBox (which is free to use if you don't install the extension pack), then
use the spell `sudo multipass set local.driver=virtualbox`.

Launching your first multipass server is a breeze. Type

```sh
multipass launch
```

then wait a while. This will fetch an image then launch a server with the name
`primary`, because each multipass fleet needs a primary server (i'll probably
learn later why). Each next server will get launched from that same image and
get an arbitraty name. If you want to name your server, run `multipass launch
--name deepthought`.

To access run commands in the multipass instance, your friend is `multipass exec
--name marvin ip address`. To get a command line on the machine, use `multipass
shell --name eddie`. To list your fleet, type `multipass list`. To shut down a
machine, use `multipass stop` to stop your primary machine, or `multipass stop
--name skynet` to stop a given one. And to throw away your stopped computers,
first use `multipass delete --name jarvis`, then `multipass purge`. I've yet to
learn why you first delete then purge machines, as i don't relly know what you
should do with a deleted instance.

Multipass also accepts [cloud-init](https://cloudinit.readthedocs.io/en/latest/)
files to configure your virtual machines. If, for instance, you want to create a
new user on your virtual box, create a yaml file like such, adding _your_ public
ssh key there instead of mine, and run it with something like `multipass launch
--name dallas --cloud-init dallas.yaml`

```yaml
# dallas.yaml
---
users:
  - default
  - name: leeloo
    sudo: ALL=(ALL) NOPASSWD:ALL
    ssh_authorized_keys: ssh-rsa AAAAB3NzaC1yc2EAAAAD...uuWg7NM= leeloo
```

If you wanted, you could make sure your packages are apt to date, install,
harden, or whatever suits your fancy.

There are tons more, i'm sure, based on the command `multipass help`, which also
gives context sentitive help, so `multipass help launch` will tell me how i can
fetch another image than the latest stable Ubuntu Linux LTS server. Or how i can
bind the virtual machine's network interface [to my local network](https://multipass.run/docs/networks-command/19542)
(which seems to require installing VirtualBox and setting up a virtual switch,
familiar from my vmWare days).

Next, i think i'm going to try Ansible on these multipass virtual machines.
