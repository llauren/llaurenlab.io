---
title: "Now i need images"
type: post
date: 2022-01-13T22:38:15+02:00
draft: false
tags: ["hugo", "meta", "pictures"]
---

Updated the [theme](https://gohugo-ananke-theme-demo.netlify.app/) for my blog
and oh goodness, now i need to have an image for each post or you'll get my
solemn figure for each entry unless i can find something nicer. I've already
patched my site's theme so that i can give credit to any creator who isn't me.
Now i just need to think of a nifty way to include the license of the image too.
