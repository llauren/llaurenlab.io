---
title: "On Hold"
type: post
date: 2020-03-12T09:52:18+02:00
draft: false
tags:
  - Macaduk
  - Public speaking
  - covid19
---

The Macaduk conference has been moved to May, in hopes that the COVID-19 pandemic
has passed (into) Stage 3 by then. I'm still planning on giving my _Security for
humans_ talk, but in two months, not two weeks. The upshot is that i've been given
a second chance to actually make this a good talk :)

There's a discussion group on the Macaduk Whova (it's an app for events) for my
session and if you feel like pitching in with your questions and ideas, please
join! My many thanks to the people who've already participated! It has helped me
give much needed clarity for my presentation.
