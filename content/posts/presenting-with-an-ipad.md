---
title: "Presenting with an iPad"
type: post
date: 2022-04-13T20:22:41+03:00
featured_image: /humans-are-weird.jpeg
image_by: "Janne Lehikoinen"
image_name: "Humans are weird"
tags:
  - public speaking
  - iPad
---

I tried something new today. I held a presentation with the slides coming from
an iPad. Specifically, the slides were on Keynote and were beamed wirelessly
using AppleTV. And it was neat.
<!-- more -->

I have an iPad Pro gen 4 (so the one that came out just before the first M1
model) which has caused me much joy. Today it also proved to be useful. It's
possible to connect the iPad to a second display using HDMI (through a USB-C to
HDMI dongle) or by AppleTV, but this second screen will mirror the first one;
you can't have two separate screens on one iPad. Except when you're presenting
slides. It turns out that both Google Slides and Keynote somehow ignore the
mirror-only limitation of the iPad and can show slides on the external display
while showing the speaker notes and/or your current/next slide on the iPad
itself. Woot!

And today, at the Finnish Mac Admins' get-together, i tried it live. The talk
itself was the same _IT security for humans_ that i held a week ago at MacADUK,
with the exception that it probably was even longer this time :)

There are two immediate upsides to presenting with an iPad in your hand. First,
you are mobile, free to move on the stage, if that's your thing. Today's stage
was small enough that there really wasn't much space to move, but on another
stage, this would be relevant to me. And second, there is no wall between me and
the audience. Some may feel the laptop screen is a barrier of security, but
really, this means i can have a wholly different contact with the crowd. The
downside is that the text is pretty small on the iPad for an old man's eyes. And
the thing to remember for next time is to disable auto-rotation for the screen
:)

Are there better solutions? Sure. A giant speaker's monitor placed so that it is
easily readable from the stage. But having the notes on my arm also meant i
don't watch them all the time and instead spend more time talking and making
contact with the crowd. In the end, not the easier path, but surely one i want
to become better with.

Thanks to everybody who made the get-together happen, especially the kind folks
at [Fixably](https://www.fixably.com/) who organised the event!

[Here are the slides](/macaduk-2022-Lauren.pdf), in case you were there and want
to give them another spin.
