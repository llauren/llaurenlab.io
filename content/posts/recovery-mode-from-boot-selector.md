---
title: "TIL: Recovery Mode From Boot Selector"
type: post
date: 2020-03-19T16:15:25+02:00
draft: false
tags:
  - macos
  - sysadmin
  - today i learned
---

Today i learned that you can enter the recovery mode through the Boot Volume
selector when starting up a Mac.

Until today, i've booted the computer, hurried up to hold down `⌘ R` for an
unknown amount of time and hoped i got the timing right. Now, i boot with the
Alt (Option) key down, wait for the boot volume selector and then press `⌘ R`.

Okay, so it's another unnecessary step, but somehow i feel less stressed. Maybe
it's because the boot selector comes up pretty quickly and i can see what's
happening, whereas loading into recovery mode looks precisely like starting the
macOS proper, only that it takes a longer time. So expectations handling,
basically.

Next, i suppose i'll need to check whether you also can boot into Internet
Recovery from the boot selector.
