---
type: post
title: "Refactor My Site"
date: 2019-07-23T20:07:48+03:00
category: website
tags:
- hugo
---

Inactivity can do wonders. Having been on summer vacation for a while and not
having done anything technically creative, my mind decided it's time to
experiment a little. I have a few websites, or blogs, or whatever, and for
reasons of simplicity and incompetence, these have always been separate from
each other. Well, i thought, how about trying to consolidate them? Just for fun?
So that's what i did.

My blogs/sites use [Hugo](https://gohugo.io), a static website generator. Hugo
uses Markdown for the posts, yaml or toml for configuration, configurable
themes, and an engine written in Go to mish-mash everything together. I have all
my articles in version control, living in GitLab. The funky thing with GitLab is
that it comes with a built-in Continuous Integration engine and web pages for
static sites. This means that when i push my articles or changes to GitLab,
magic happens (in the technological sense), and my new pages are on the web in a
minute's time.

While i've been using Hugo for one blog per site, there's really no reason why
you can't host several ones in parallel ... except for the lack of competence.
But as i've said before, incompetence can be cured by learning, and that's what
i decided to do.

I created a brand new hugo site (`hugo new site blog`) and decided to go with
the [Ananke](https://themes.gohugo.io/gohugo-theme-ananke/) theme (because
that's the one mentioned in the Quick Start documents). Ananke's default
directory, or content category, is called `post`, whereas my blog's posts are in
the directory `posts` (plural). I decided that i'd rather retain my URL scheme
in case _anyone_ has linked to any of my articles previously and tinker a bit.
My theatre blog is getting a wholly new URL scheme, which really can't be
helped.[^theatre]

I used the command `git submodule add
https://github.com/budparr/gohugo-theme-ananke.git themes/ananke` to add the
theme while in the root dir of the blog, and i'm a little worried that the CI
won't pick up on the directory name "ananke" i chose instead of the longer
"gohugo-theme-ananke". Pushing it all to GitLab will show the results, and if it
doesn't work, i have work.

Having two blogs on "one Hugo" isn't that complicated. I created a directory
`content/efraim` for my theatre stuff, parallel to `content/posts`.  To make the
whole thing work, you need to create a file called `_index.md` in both
`content/*` directories. The file should contain the metadata for the category
in question and mine look a little bit like this:


```yaml
---
title: "Kalla mej Efraim"
featured_image: '/efraim.jpg'
description: "En aspirerande amatörskådespelare bloggar"
languageCode: sv
menu: main
weight: 50
---
```

The first three linkes are probably self-explanatory. The `languageCode: sv`
means that the content is in Swedish (i'll add proper localisation...
eventually). `menu: main` is so that the category will show at the page-top
menu, and `weight: 50` is for sorting; menu items with a lower weight go to the
left (like "Learning in public") and menu items with a higher weight sort to the
right.

I also have some single-page pages, like the [now](/now) page. There are
probably more ways than one to accomplish this, but the simple one is to create
a markdown file directly inside the `content` directory and add `menu: main`
(and a weight) in the front matter. Thus, my `now.md` starts out with

```yaml
---
title: "Now"
date: 2019-06-24T12:00:42+00:00
menu: "main"
weight: 100
---
```

This could probably also be achieved by adding `SectionPagesMenu = "main"` into
the site's `config.toml`, but what's done is done, and there's only so much
refactoring a man can take on a single run.

To make sure you use the `post` layout, as defined by the theme, you could add
`layout: post` to the front matter. I did this on the [thesis](/thesis) page,
though i can't say i see any difference between that page and the now page.

I have a [talks](/talks) section, containing just one article, which i could
have built like this. In the end, i structured it like the two blogs, just in
case i'll ever give another talk, or if i post another article around the one i
gave :D . Sure, you shouldn't build ahe

## Tweaks

Since i use a directory name `posts` for my posts, and because i want my dates
in a different format than the theme's creator, i did some tweakage on the
back-end by adding a second theme for my overrides. To make Hugo use both the
original theme and my stuff, my site's `config.toml` describes the theme thusly:

```toml
theme = ["my", "ananke"]
```

I've then copied `themes/ananke/layouts/_default/single.html` to
`themes/ananke/layouts/_default/` and changed the `.Date.Format` to my liking
(it's on line 23, currently). I then copied `themes/ananke/layouts/post/*` into
`themes/my/layouts/posts`. The drawback is that if the creator changes the
theme's default single layout, my override won't catch it; it would be way much
better if the date format was configurable in the site's `config.toml` (and i
might just create a feature request for that :).

Finally, i copied some mugshots, lowered the jpeg quality to squeeze down the
file size (150-250kB per pic is still more than i would want to send you --
accept my apologies!) and saved the result in the static directory. To have Hugo
pick these up, add the following line to the `_index.md` files:

```yaml
featured_image: '/mugshot.jpg'
```

(changing the image URL accordingly).

Now i just got to take a deep breath and figure out how to get all of my new
source code to replace the old one on my blog repo. If you're seeing this, i've
probably succeeded.

[^theatre]: If you haven't heard of my theatre blog, you are forgiven. I haven't made much noise about it, and it is written in Swedish.
