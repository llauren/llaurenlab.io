---
title: "Safe Christmas"
type: post
date: 2021-12-22T22:19:54+02:00
draft: false
featured_image: "https://live.staticflickr.com/590/31822646461_5bcce0e267_c_d.jpg"
image_name: "[Dhaka](https://flic.kr/p/Qu4o7i)"
image_by: "[ASaber91](https://www.flickr.com/photos/84891020@N03/)"
categories:
  - security
tags:
  - security
  - Lazarus Heist
  - money
  - crime
---

In February 2016, Bangladesh was robbed of US$101 million by cunning cyber
criminals using fraudulent SWIFT transfer requests. The money was siphoned from
the Bangladesh National Bank's account at the US Federal Reserve to banks in Sri
Lanka and the Philippines. Another US$850M was stopped from being transferred
due to a happenstance. Check [The Lazarus
Heist](https://www.bbc.co.uk/programmes/w13xtvg9) for juicy details.

What made the heist possible? Timing. Careful, thought-out execution.
Ruthlessness. The bank robbery took place during a weekend of Lunar New Year
festivities in South Asia, so there wasn't really anyone on call to check and
stop the transfers. And it was preceded by nearly a year of preparation.

Computer criminals don't take Christmas off (or New year, be it Lunar or
Gregorian). Rather, they take advantage of the fact that IT has a few days of
holiday, because being moral is bad for their kind of business.

Thus, this Christmas, and every Christmas, New year, Wappu[^wappu] and Midsummer
for that sake, stay a little extra vigilant with your IT, or just keep your
hands off your computers, don't install shady apps, click on dodgy links or get
hit by phishing, scams and ransomware, and we'll have a few good days of
relaxing time off!

[^wappu]: The start of Spring festivities, big thing in the Nordics
