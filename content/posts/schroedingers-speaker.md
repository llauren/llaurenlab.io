---
title: "Shrödinger's Speaker"
type: post
date: 2020-02-13T13:12:11+02:00
lstmod: 2020-03-01T19:25:21+02:00
tags:
  - macaduk
  - public speaking
---


Last autumn, i volunteered to speak at the MacAD.uk conference 2020. As i
neither got an acceptance, nor a rejection letter, apart from a two word
response, "great thanks", i'm now _assuming_ i won't be talking there.
Therefore, i haven't prepared a talk.

My nightmare scenario is that i'll show up at the conference and they expect me
to present.

**Update:** [That's pretty much what happened!](/posts/alive-in-the-box) Except
that they did give me four weeks of warning :)

{{< figure src="/img/llauren-macaduk-wallpaper.png" title="Me on the Macaduk wallpaper" >}}
