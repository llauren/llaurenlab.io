---
type: "post"
title: "Sharing your `known_hosts` among your admin peers"
date: 2018-02-28T12:45:30+02:00
description: "You can share your known_hosts file among your admins"
draft: false
keywords: ["ssh", "known_hosts"]
topics: ["sysadmin"]
tags: ["ssh", "known_hosts", "ansible", "security", "hcisec"]
---

The first time you contact an ssh server, you're presented with a fingerprint and asked whether you
want to trust the server to be who it is, based on this fingerprint. Blindly, i might add. If you're
at least a bit concerned about (usable) security, this should send a few shivers down your spine; how
the heck are you supposed to know whether this fingerprint is right or not.

If you're managing your servers with ansible, this query can be inconvenient, as it stops your flow at
some random place and it's not entirely clear where it will commence. So yeah, asking to trust fingerprints
suck, but then again, not checking for those fingerprints is an even worse idea.

One way to alleviate the pain of you and/or your peers is to share those fingerprints amongst yourselves,
and it turns out it's not that hard to do.

First, copy the `~/.ssh/known_hosts` file of a reasonably seasoned sysadmin. `sort` the file. Groom is so 
that it contains only entries for servers that are alive and common. Combine lines which have separate 
entries for a host's IP address and hostname. Put the hostname first, then a comma, then the IP address, 
then the rest. Delete old entries with outdated keys. Sparkle it with love.

Now add the file to your git repo (because you have one for your admins config stuff, right?). For the
sake of this discussion, let's call this location `~/git/admin/config/ssh/ssh_config`. Commit and push.

Now add a line somewhere near the top of your `~/.ssh/config` file which says:

```
GlobalKnownHostsFile ~/git/admin/config/ssh/known_hosts
```

(substituting the path above to whatever yours is)

Delete all entries from your own `~/.ssh/known_hosts` file which are already in the common `known_hosts`
file (or like i did, rename your `known_hosts` into something else, then `grep` the hosts that are just
yours into a new `known_hosts`).

And that's it! Share and enjoy :)

_On a different note, there really should be a way to verify those fingerprints some other way, like
a TXT record on DNS, but that's for another blog post._
