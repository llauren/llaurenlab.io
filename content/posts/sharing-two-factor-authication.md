---
title: "Sharing Two Factor Authentication With Your Admin Colleagues"
type: post
date: 2021-06-30T18:48:27+03:00
draft: false
summary: "It's surprisingly easy to share two factor authentication among your peers (depending on your definition of 'easy'). You'll need just a little work on the command line to prove that any of you are, in fact, you."
tags:
  - sysadmin
  - 2FA
  - MFA
  - multi-factor authentication
  - two-factor authentication
  - security
  - mac
---

Two factor authentication is a way for you to prove that you are you, and for
anybody not you, not being able to prove that they are you. Or so i have been
led to believe.

There are different ways of two factor authentication. You can answer 'Yes' on
your mobile device if you're logging in on your computer. You can enter a secret
sent to you via a text message. You can present your face or fingerprint to your
device. There are physical keys that may look like USB memory sticks and then
there are authenticator apps that roll a new six digit number once or twice a
minute. I use those a lot.

Turns out [the six digit code](https://en.wikipedia.org/wiki/Time-based_One-Time_Password)
is just something calculated from a static secret, otherwise known as a
password. The only difference is that you don't need to know the password and
that the calculation includes the current time, which you also don't need to
know, because the computer takes care of all this remembering and calculation.
This also means that anybody with the shared secret ("password") can
two-factor-authenticate as you. The upside is that you can use 2FA for those
shared admin accounts that you really shouldn't be using, but sometimes do have
to use anyway. Like when you're registering for a service and don't want the
Primary Administrator of that service be any one of you admins.

Let's get to business.

Authentication by Time-Based One-Time Passwords (TOTP), those six digits that
change every so often, usually starts by the system presenting a QR code for you
to scan with a mobile device. But we're not going to use a mobile device, we're
going to use a computer with a command line, because we're Admins. For that,
you're going to need a piece of software to decode QR codes. On a Mac, using
Homebrew, you can `brew install zbar`. Take a cropped screen shot (⌘ + €, or ⌘ +
Shift + 4 if you're on a non-European Mac). Type the incantation `zbarimg
~/Desktop/Screenshot\ <today's date> at <the time a few seconds ago>.png` or the
equivalent for your non-Mac if that's what you're on. The interesting bit is the
string after `secret=`. Copy this one and save it to your encrypted password
vault. Then share the encrypted secret among your admins.

If you're using Gopass, you can just edit the secret for your 2FA user and add a
`totp: ` field and paste the whole shebang starting with `otpauth://`. You can
now get the TOTPs using `gopass totp <name of credential>`. But that would be
just too easy.

It bears repeating: Encrypt, then share. Use GPG or some password manager with
which you can limit who can access the secret. It's just another password, with
the added twist that you may _think_ you're more secure with it, but really,
it's just another password.

Next you're going to need OATH toolkit (`brew install oath-toolkit`). Once this
is installed, it's as simple an incantation as `oathtool --base32 --totp
<secret>`, where `secret` is the string of characters you copied, encrypted and
saved a few paragraphs up. You hopefully got it by using your password managerö
Add a `| pbcopy` to the previous command and you can simply paste the TOTP to
your login form.

You can even write a script which decrypts your TOTP secrets and feeds the
required one into oathtool, then copies the result for you to paste in the 2FA
form.

I used this method successfully to create a shared admin user for the MDM we're
testing. I'll tell more about that in another post. Now go forth and break down
the illusion of added security!
