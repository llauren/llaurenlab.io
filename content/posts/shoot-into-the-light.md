---
title: "Shoot Into the Light"
type: post
date: 2023-01-24T22:03:51+02:00
featured_image: "https://live.staticflickr.com/65535/52609313776_03842fce1a_c_d.jpg"
image_name: "[Winter night wonderland](https://flic.kr/p/2o9Uu75)"
image_by: "[Robin Laurén](https://www.flickr.com/photos/llauren/)"
tags:
tags:
    - photography
    - instagram
---

After years of waiting, i finally got myself a camera. It's been such a long time since i had one. Now i need to start seeing with "my camera eye" again.

To train my camera eye, i've challenged myself to take, edit and publish one picture each day. I don't know how long i'll last, but i'll try.

There are several reasons for this. Consistency is one. If i do something each day, i hope to improve, and i hope to get some routine doing it. But more so, i hope i can challenge myself to see things in my surroundings that are part of my day and that would work as a picture.

I'm on day 24 as i write this and i have to say it isn't easy. Most days look like each other. The sky is gray and i'm out only when it's dark.

If i could link to my hashtags i would do it, but then again, i probably won't publish anything on [my Instagram feed](https://www.instagram.com/robin.lauren) which isn't part of my #dailypic challenge.

And if that wasn't hard enough, i also try to publish [a dog a day on Kelvin's feed](https://www.instagram.com/kelvin_the_lapphund/). Considering how much he looks the same day to day makes this even harder!
