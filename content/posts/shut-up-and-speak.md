---
title: "Shut Up and Speak"
type: post
date: 2019-10-05T12:17:34+02:00
tags:
  - public speaking
  - conference

---

I've spent this wonderful week at the [MacSysAdmin](https://www.macsysadmin.se)
conference, which, as you may be able to deduce from its name, is a conference
for Mac sysadmins.

Being at a conference, and at this conference in particular, can be inspiring,
which is why i've decided to create a follow-up talk for my first one (or last
one, however you want to put it), on the topic of _Humane Security._ Now while
i'd love to give it at MacSysadmin, which i consider the crown jewel of all Mac
sysadmin conferences[^msaconf], i really feel i should first approach Macaduk
with my proposal.

I'm still in Göteborg writing this, and i actually already started gathering my
ideas. For better or for worse, i think they're only the background and
motivation for the whole thing, and i'm really missing both the meat and the
expertise here, but if i just re-frame it to _"the need for humane security"_,
i've nearly nailed it already ;) (with the added bonus that it's way shorter
than last year's marathon presentation)

Now i remember that speaking on stage is bloody frightful, but the loving
atmosphere of MacSysadmin has filled me with such euphoria that for the moment
that fright is all forgotten. I guess this too shall pass.

[^msaconf]: My sum total experience of Mac admin conference is, in all honesty,
            two. Well, three, if you consider our local meetups/miniconfs.
            To say MacAD.uk comes at an honourable second place does sound a bit
            less majestic, but it really is a good conference.
