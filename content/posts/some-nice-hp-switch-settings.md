---
type: "post"
title: "Some Nice HP Switch Settings"
date: 2018-03-02T10:48:51+02:00
description: "Basic, initial and fairly secure settings for a HP 2530 switch"
draft: false
keywords: ["hp", "hpe", "network", "switch", "aruba"]
topics: ["network"]
tags: ["hp", "switch", "network", "config"]
---

I'm kind of fond of the HP (HPE/Aruba) 2530 series switches. They are mostly
no-nonsense and fairly nice to configure from the command line.

<!--more-->

Here are (some of) the settings i like to copy-paste to a HP 2530 console
the first time i fire it up. Please substitute the stuff in `UPPER_CASE` to
correspond to your environment.

If you're a Friend Of Order, you might want to start your initial config with
`reset`. I didn't put this in the code below, in case one of us uses it over
a ssh session (which would be ... inconvenient).

Use a password manager and create (separate) passwords for `manager` and
`operator`. Use the commands `password manager` and `password operator` to
set these. You might want to use `password manager plaintext YOUR_MANAGER_PASS`
if you promise me you don't use the same manager password on more than one
switch :)

So here's a quick breakdown of the settings:

* Give a nice hostname for the switch
* Set all IP management traffic to be routed to your (management VLAN) gateway
* Use encrypted protocols ([i write about SSL certificates here](/posts/hp-switch-certificate/))
* Allow Manager and Operator to log in over ssh using keys (note that the public key must be 'in single quotes' to work)
* Set logging settings; i use facility local0
* Allow read-only snmp access (you may want better security here)
* Use NTP for time synchronisation (you'll need to be on a fairly recent
  firmware for ntp)
* Set up a management VLAN and allow management only from this VLAN
* Set telemetry to come from the management VLAN (or if you really want to,
  from a separate telemetry VLAN)
* Set passwords for the manager and operator users

Add a port for your management VLAN, connect and update the firmware. I'm sure
you could do this too over the command line, but my skills are finite.

And this i think more or less covers it. I'll add to this post if i come to
think of anything neat or necessary. Apart from that certificate stuff.

```
hostname YOUR_HOSTNAME_HERE

ip gateway YOUR_GATEWAY_HERE

no telnet-server
ip ssh
ip ssh filetransfer
ip ssh public-key manager 'ssh-rsa MANAGER_PUBLIC_KEY Manager'
ip ssh public-key operator 'ssh-rsa OPERATAOR_PUBLIC_KEY Operator'
aaa authentication ssh login public-key

no tftp client
no tftp server
no dhcp config-file-update
no dhcp image-file-update
no dhcp tr69-acs-url

logging YOUR_SYSLOG_SERVER_IP udp YOUR_SYSLOG_SERVER_PORT
logging facility YOUR_SYSLOG_FACILITY
logging severity info
logging notify running-config-change
logging origin-id hostname

snmp-server community public restricted
snmp-server location "YOUR_LOCATION_HERE"

timesync ntp
ntp unicast
ntp server YOUR_NTP_SERVER iburst
ntp enable
time daylight-time-rule YOUR_DST_RULE
time timezone YOUR_TIMEZONE_IN_MINUTES

spanning-tree

vlan YOUR_MANAGEMENT_VLAN
  ip address dhcp
exit

management-vlan YOUR_MANAGEMENT_VLAN
ip source-interface all YOUR_MANAGEMENT_VLAN

# password manager
# password operator

save
```
