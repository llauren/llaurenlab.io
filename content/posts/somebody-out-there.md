---
title: "Somebody Out There"
type: post
date: 2019-10-05T13:41:07+02:00
tags:
  - conference
  - audience

---

One really nice thing from the MacSysadmin conference (see the next/previous
article), i got to hear that i have at least one reader of my blog! Yay! Thank
you!

I wish i had a little hand-wavy-thing on this blog, like the one on Medium, so
people could react and say "Yay!" or "I read this", or even "Thanks!", if they
were so inclined. I'd like it to be privacy conscious though.
