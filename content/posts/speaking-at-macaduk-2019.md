---
type: post
title: "Speaking at Macaduk 2019"
date: 2019-01-30T15:59:38+02:00
draft: false
tags:
- "macaduk"
- "speaking"
- "public performance"
categories: "me"
---

Aaaagh! I'm going to speak at a conference! And i haven't even started writing
my talk yet! And i thought i'd start writing it in January and it's practically
already February! Panic now!

On a more serious note, i am indeed going to take a huge step over my insecurity
and talk about something that i do know a bit about, though the more i think
about it, the less i feel i really know, so it's imposters' all around agian.

Breathe.

For some time now, i've been interested in security from an _official_ standpoint.
What does it mean when your government says something is confidential, restricted,
secret or mythically _top_ secret? Assuming these things do exist outside agent
films, what should you do about them? And what should you do about your computer
environment to keep your secret things secret and your confidential things
confidential?

Also, can you get your computer up to government level security and still use it
as a computer? And not break your budget, or lose your sanity, while doing so?

(spoiler: the answer to at least one of these questions is yes)
