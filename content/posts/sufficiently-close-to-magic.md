---
title: "Sufficiently Close to Magic"
type: post
featured_image: https://live.staticflickr.com/65535/40732802473_c9ef8ff690_c_d.jpg
image_name: "[25 April 2019, Matins of Holy Friday](https://flic.kr/p/254qg4P)"
image_by: "[Saint-Petersburg Theological Academy](https://www.flickr.com/photos/spbpda/)"
summary: |
  Any integration done well enough is sufficiently close to magic. With
  apologies to Isaac Asimov. In this article, i'm stunned by the ease of
  creating an ebook from a web page to an iPad with precisely two commands.
date: 2021-06-09T11:39:08+03:00
draft: false
categories:
  - macos
  - integration
  - magic
  - ebooks
---

Sometimes things work so well together that i'm just stunned. Like today when a
colleague suggested me a web site about on-line privacy and anonymity. He also
said it's a pretty long read, the book is presented on-line as a _yuge_ html
file, so i though why not read it on my new brand spanking iPad as an ebook.
Previously i've tended to read my free ebooks on my Android tablet, but this was
way more magical.

With just the two commands on my Mac:

```
pandoc https://anonymousplanet.org/guide.html -o anon-guide.epub
open anon-guide.epub
```

  * The book-as-a-web-page is downloaded
  * Pandoc translates it into an ebook, with chapters, pictures and all
  * The book is opened in the Books app on the Mac
  * The book is also copied to my iPad's Books

With Google Books, i'd have to upload the ebook using the web interface, which
in itself isn't a big hassle, but this is just so automagic!

Now the only thing that bothers me is whether my actions was against the
recommendations in [The Hitch-Hiker's Guide to Online
Anonymity](https://anonymousplanet.org/guide.html), but i guess i'll just have
to read the book. It's a good 635 pages on a comfortable font size. See you in a
while.
