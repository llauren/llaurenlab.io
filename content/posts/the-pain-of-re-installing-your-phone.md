---
type: post
title: "The Pain of Re-installing Your Phone"
date: 2018-10-09T09:25:46+03:00
draft: true
tags:
- "android"
- "phone"
category: "digital life"
---

After installing the latest security update to my phone yesterday,
my phone and i disagreed on what the security pattern was to boot
the phone. And being a phone, it doesn't really care for negotiating.
So after swiping every conceivable iteration of what i am _sure_ is
the right pattern, i decided to wipe the phone and restore it from
cloud backup, which i was equally sure was automagically taken. Small
thing. Few hours, and back to reality. Piece of cake, right? Right.
Or not so much.<!--more-->

The security pattern is a pretty tight lockdown for an Android phone,
as i'm sure it's strong for iPhones. There really is no backup code
as the phone uses the pattern for encrypting much of the phone's data.
And when locked out by the security you've imposed on yoursef, that
actually kind of sucks. Otherwise _maybe_ one could use the backup
authentication codes from Google, or a hardware token (of which i've
written [before](getting-started-with-yubikeys)).

First, it's about wiping your phone. You'd think _that_ would be easy.
I have device management on my phone ("Find my device", and Device
Management on my company account), which has a Remote Wipe function.
But it turns out that this functionality requires a properly booted
up phone to work, which means that Find my device can't find ...my
device.

Time to up the ante. Android phones have a way to factory reset the
phone from firmware. The methods differ a little from phone to phone,
but generally you'll have to power on your phone holding down volume
up (or down) while holding down the power button. I did this a dozen
times before succeeding. And when you're in Diagnostic Mode, you'll
choose to factory reset the phone, get a non-descript "No Command"
screen, press volume up (or was it power?), choose to nuke and pave,
select yes, and it's wipey-by to your data.

You'd think that would suffice.

Uh-uh. Security, baby. Once the phone comes back, it's _still_ locked.
And again, i know that it's great for security and that a stolen phone
is pretty much worthless to the crook, but when it bites _me_ in the
foot, it's really annoying. Yeah. Security. Annoying. Take it from me.

Now i _have_ introduced two Yubikeys to my Google account, so i was
given the option to use one of these to prove i was i. And you think
that would work? The NFC key was readable only on the top of the phone
(makes sense since that bit is not covered with metal) but the phone
just found and dropped the connection in rapid succession, so that
was a lost cause. Second, i tried the USB-C key, but one of thwo things
kept happening: The phone asked whether to allow the use of this new
found key, or it would just not read (because somehow the USB-C port
requires me to stuff any connector in it with some force --
