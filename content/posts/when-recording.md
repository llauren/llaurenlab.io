---
title: "When Recording"
type: post
date: 2019-12-03T16:48:57+02:00
draft: false
tags:
  - music
  - singing
  - learning
  - recording

---

{{< youtube A2UBI03_j-8 >}}

I have a hobby i haven't written much about here. I sing. I started to sing by a
happy happenstance and a fortunate misunderstanding when i was the Nights Watch
at the music camp my kids were at and i thought the commanding _"Everyone must
come to the singing auditions"_ really meant everyone and not just the campers.
I was scared to bits, but after getting over the terror, i started really liking
it! Now i've been taking vocal training for a few years at the Arbis ("Workers'
Institute", kind of like a community thing) where we have a 45 minute solo class
every two weeks and a concert at the end of the semester.

I record my performances for myself to analyse and to force upon my friends and
family. If all goes well, i should see something to improve on the videos, and
some improvement over the years.

A side quest in recording your singing is that you learn how to record your
singing. Sometimes you learn by making mistakes, which is exactly what happened
yesterday. And since it's a one-shot live situation with no re-takes, the only
thing you can do is write it down and try not to repeat the dumb bits next time.
And sometimes, the mistakes are happy mistakes.

So here are the mistakes i made this time, and which i might learn from:

It's not enough to have fresh batteries to the recording device (in my case, an
old Zoom H2). You also need space on the SD card. And if your spare SD card
which you did (ha!) bring with you, needs to be compatible with your recording
gear (duh). In my case, the card had too large a capacity for the recorder to
understand.

Frame your picture so that you and the pianist both fit nicely into the picture.
Moving unnecessarily will look silly on video, and might cut out your head from
the frame. This, i've learned on earlier recordings, but it leads to the next
learning:

When recording picture and audio with a single camera or phone, your sound will
suck. In practice, recording anything from farther than a metre away, especially
in a place with a lot of echo (reverb) area, your sound will sound crap. To hear
the melody, you'll need a microphone closer to your sound emitting hole. An
external microphone, either connected to your camera or phone, or an external
recording device. If you want to fancy things up, you can mix the sound from the
proper microphone with the camera's sound in post[^in-post].

If you just ran out of SD card, you'll need to move the camera/phone closer to
your face. This will make your sound clearer, but will probably make the picture
look weirder, and quite possibly drown out the piano in the background, throwing
off the sound balance. Since it's live, you only get one shot to experiment
with. But given the options, i suggest a closer up picture. In the heat of the
moment, i didn't think of any of this, so my camera/phone stayed where it was.
The framing of the picture was okay, but the sound was crap.

The phone itself was a decent enough recording device, though i'm sure i could
have done better with an actual camera or a real video camera. But a phone is
unobtrusive and is easy to set up. You can even go wild and use several cameras,
either for deciding on the best angle afterwards or to make a multi-camera
recording. I'll talk about the joys of Blackmagic Davinci Resolve in another
post.

Recording involves gear, which you sometimes may forget or not have time to
bring. A camera does not stand by itself. You need a stand. And you need a stand
for your microphone. A stand for music sheets can be repurposed into a camera or
microphone stand. If you're fortunate, you can remove the platform the music
sheets are supposed to lie on and the screw that holds the table in place will
have the exact same pitch as your mic or your gimbal, so you can screw them in
and never fear the mic or phone will fall down. Whoa! Ex tempore mic stand!

A proper camera stand will be robust. It will not be unobtrusive. A repurposed
music sheet stand will be unobtrusive but wobbly. Thus, press record, check that
the camera is indeed recording (the timer is rolling), and then take the stage.
Same goes for the sound recorder. I've managed to "record" something on standby
so many times that it's not even funny. Make sure you see the seconds rolling
before you start performing. You'll beat yourself up, at least mentally, if you
get out of your once in a livetime performance and realise that your gear is
still on standby. It is not a funny feeling. At all.

So what worked well? The combination of a phone, a gimbal (60 € on the Internet)
and a music sheet stand worked surprisingly well. I just need to make sure i
have a makeshift stand like that next time, or i'll have to improvise. What
would i do differently next time? In an ideal world, i'd have several cameras
(phones) on equally unobtrusive places. Record in a resolution higher than
1920x1080 so that i can crop the picture in post without losing quality. Have
several microphones, one for me, one for the piano and one for the hall sound.
At least the piano mic and the hall mic should be stereo to create a nice
ambient sound. And i'd do a small click sound before starting to sing so that i
could more easily line up my audio tracks. Then i'd first mix the different
audio tracks before mixing in the multi-camera video. In a practical world, at
least i'd have that separate microphone.

And i'd practice both my singing and my video recording skillz so i'd get some
routine doing this, rather than filming just twice a year.



[^in-post]: In post is fancy-schancy lingo for "afterwards", when you are
    cutting and mixing your recording. If you aren't cutting and mixing your
    recording, this post (hah) will probably sound like an awful lot of
    unnecessary work, and you wouldn't be all too far from the truth...
