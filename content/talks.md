---
title: "Talks"
origdate: 2019-03-27T08:32:21Z
date: 2022-03-290T16:12:21+01:00
description: "Just shut up and speak"
featured_image: '/macaduk-2019-wallpaper-llauren.jpeg'
keywords:
 - public speaking
 - talks
topics:
  - talks
tags:
  - public speaking
  - macaduk
  - security
meta: false
menu: main
weight: 31
type: post

---

# MacSysadmin 2022: Security for Humans - Revisited

**A completely remastered and mostly re-written version of my MacAD.UK talk.**
Read about all the buzz below.

This one was recorded at the Finns Sommarteater stage in front of a gapingly
empty non-audience, over three days. I was interrupted by incompetence (which is
where the first day went), bad timing (second day) and rain (third day), but in
the end, i got a rather nice multicamera production recorded ... using just one
camera (because that's all i had).

The reason i did it on a stage, in front of rows of empty benches, was to
emphasise that MacSysadmin really should be done in front of actual people (and
i miss you!). There's so much missing with a pre-recorded presentation, like
audience participation, so i decided to mix that in! I had some wonderful
friends who contributed questions (on a minimal schedule) which i mixed in so
that they felt live.

There were a few easter eggs included. Of course, i had to have a banana in the
presentation. There's a blue mug on stage, which is from MacAD.UK, where the
talk was originaly held. And i switched between all my MacSysadmin T-shirts
during the filming (and some of you even found out!).

I even recorded a post-roll extra where i meet myself walking towards Göteborg.
Didn't make it into the cut because as always, i was way out of time. So that's
something to learn for me.

Film editing with BlackMagic DaVinci Resolve. The final edit has nearly 200
elements (=film snippets, transitions, pieces of text, slides and whatnot). The
sound was first fixed in Audacity and then fixed some more in DaVinci Resolve.
Slides done in Keynote and DaVinci Resolve. It took me approximately 120 working
hours to get the whole thing done from first writing draft to final delivery.

# MacAD.UK 2022: Security for Humans

**There's a lot more to IT security than IT. Trying to solve IT security without
taking human behaviour into account will lead to failure. Here's how not to
fail.**

In this talk, i adress how and why humans are weird, lazy, gullible and
wonderful, but how they are so in a more or less predictable manner, and how
this ties into IT security.

[Here are the slides](/macaduk-2022-Lauren.pdf)! Not sure they will help you if
you haven't seen the talk ;) . Maybe i should do a blog series of the contents?

[Here's the blog post](/posts/macaduk-2022-security-for-humans/) about it.

And [here's the video!(https://youtu.be/N6xy91hyPJo)

This talk was also performed at the Macsysadmin.fi meet-up shortly after
MacAD.UK 2022.

## Sources mentioned in the talk

  * Björk: [Human Behaviour](https://www.youtube.com/watch?v=p0mRIhK9seg)
    (see the little hedgehog at the bottom right of the slide? ;)
  * Bruce Schneier: [Secrets and lies](https://www.schneier.com/books/secrets-and-lies/)
  * [FidoNet](https://en.wikipedia.org/wiki/FidoNet), which much to my surprise
    still exists!
  * [Reaktor](https://reaktor.com)
  * Elements of happiness, vaguely based on Maslov's hierarchy of needs. Can't
    remember where i learned this from.
  * Lt Cmdr Data: Generations
  * Newton's laws on [motion](https://en.wikipedia.org/wiki/Newton%27s_laws_of_motion)
    (or mechanics), and on [thermodynamics](https://en.wikipedia.org/wiki/Laws_of_thermodynamics)
  * George Box: [All models are wrong](https://en.wikipedia.org/wiki/All_models_are_wrong)
  * Daniel Kahneman: [Thinking, fast and slow](https://scholar.princeton.edu/kahneman/home/publications-0)
  * Brian Brushwood: [The World's Greatest Con](https://worldsgreatestcon.fireside.fm/)
  * Perry Carpenter: [8th Layer Insights](https://8thlayerinsights.com/) podcast
  * Perry Carpenter: [Transformational security awareness](https://amzn.to/3qKgLSY) (Amazon UK affiliate link)
  * Robert Cialdini: [Seven principles of persuasion](https://www.influenceatwork.com/7-principles-of-persuasion/)
  * Arbinger Institute: [Leadership and self-deception](https://arbinger.com/Landing/LeadershipAndSelfDeception.html) (the Box model)
  * Otto Scharmer: [Listening](https://www.youtube.com/watch?v=eLfXpRkVZaI)
  * Nancy Duarte: [Resonate](https://www.duarte.com/resonate/) (the hero's journey)
  * BJ Fogg: The [Fogg Behavour model](https://behaviormodel.org/)
  * George Edward Woodberry's [corollary on Newton's Second Law on Thermodynamics](https://www.brainyquote.com/quotes/george_edward_woodberry_119569)


# MacADUK ~~2020~~ ~~2021~~ 2022

I'll be at [Macaduk](https://macad.uk) in ~~May~~ ~~November 2020~~ March 2022,
or whenever Macaduk will be held,
[talking about Security for Humans](https://www.macad.uk/speaker-2020-robin-lauren/).

I had, at some stage, thought about the subject _The Human Sysadmin_ but maybe
that's a bit too big of a subject, best saved for when i have some more
experience in flapping my lips in front of a paying audience.

# MacAD.UK 2019: Official Security

My first ever public talk, on security in an official setting, at Macaduk 2019.

* [Video](https://youtu.be/lklZ1IuGpBw)
* [Slides](https://docs.google.com/presentation/d/1441kX4zXr4pw5Me0vydGPLwt7J6cGY339qZ5NZieIcY/)

Before this, i'd actually never stood in front of a paying audience and talked
before now. The whole prospect was both terrifying and something i felt i _should_
do, given all the effort all others have given through their talks. My time to
start giving back.

I've posted some  [shorter cries for help](/tags/macaduk/) on my blog.
