---
title: "Thesis"
date: "2018-08-26"
lastmod: "2018-08-26"
menu: "main"
weight: 40
meta: "false"
layout: post
---

OK, so it was over ten years since i wrote my MSc Thesis, but since i wrote in
it that it shall be available on the Internet under this address, it would be
nasty not to have it published. Though in all honesty, it's a bit crap. You've
been informed.

View or download my Msc Thesis [User Centred Access Control for home networks](/thesis-llauren-100-remastered.pdf) (if you must).

There are, however, a few lessons learned which i might be able to recall even
10 years post fact.

## Have an excellent advisor

Your advisor/instructor is your editor and your coach and, thesistically
speaking, your best friend. Remember this when your thesis is published. S/he
should have a clear view on what a good thesis work looks like and preferrably
have written one, not too insanely long ago. It's not that all theses (uh,
thesises, thesii..?) look the same, but they do share traits. There is a
structure, a tone of word, and most of all, a focus (we'll get to this in the
next paragraph), and your advisor should be well clued about these. S/he should
be able to spend enough time and effort on you and your work without actually
writing it. You should be able to communicate often and effortlessly.

## Write about a well defined subject

This is probably the hardest thing of them all, especially as you've probably
never written a technical paper before this. Well, i never had, but i hope
things have changed a bit since. My thesis ended up a mish-mash of things and
even though i had a fairly precise subject, well over half of my thesis was a
bunch of background on things that were interesting but in no way about the
subject itself.

In the end, most of my time writing the thesis was spent on rewriting bits,
changing word order and in much angst not really producing anything of
substance. Focus. Really. It's hard, but it's the right thing.

If you can, start sketching at a high level. Use a mind map. Then edit and
iterate. Be inspired. Graft out the things that are good but just don't fit in.

If you're like me, you'll find _loads_ of interesting subjects and viewpoints
when reading other articles and you want to include them _all_ into your thesis!
Don't do this. Think of what is important to _your_ thesis. It's okay to change
the direction, even the subject of your thesis (if your advisor and professor
are okay with it!). It's way better to have a deep thesis than a wide one. This
i only understood way after the fact. Don't be me.

I know it's hard to discard stuff you've poured your soul to write. Instead of
just throwing stuff away, leave it as fodder for future writing, be they blog
articles or actual technical articles.

## Have an infrastructure

Version control is your friend. Make a git repo for your thesis from day one.
Don't be too fussy about the actual layout, you'll have plenty of time to muck
about before you're done.

You probably want one directory for the actual thesis, one for ideas, one for
meta and misc. Or something like this. Don't sweat it, just do it. Start version
control before writing anything.

```
git init thesis
cd thesis
echo 'I made this!' > README
git commit -am 'Here we go!'
```

There! You've written something! :)

I needed to accidentally delete my latest thesis source code and manually
rebuild it using old code and the latest pdf before understanding this. Don't be
me. Also do backups
([properly](https://www.hanselman.com/blog/TheComputerBackupRuleOfThree.aspx)),
even if you use git. You can (semi-)automate this; Dropbox + Time Machine is
probably a good enough solution if you're on a Mac. Also, thirteen years ago,
git wasn't really on my radar, but eventually i started using `rcs`, because
it could work on my computer without the need of any other computer. Again,
don't be me. And use git.

I wrote my thesis in LaTeX because it was the Right Thing to do. I also spent
weeks on learning LaTeX (good), tweaking LaTeX (questionable), debuggig LaTeX
(frustrating) and just trying to make the damn thing compile (mmwwwaarrgghh!).
LaTeX is damn frustrating, but writing my thesis with LaTeX was a learning
experience and a tale of much personal growth. A rite of passage. I've written
my thesis, from scratch. Also, it was quite the satisfaction to actually see my
`.tex` files compile and have them update in the dvi viewer. I guess this is
attributable to
[the IKEA Effect](https://www.hbs.edu/faculty/Publication%20Files/11-091.pdf)
(pdf) because there's no rational reason why one would have to pour time, effort,
frustration and pain in something that is inherently inferior but still something
of your ... very own. Still, i feel most of my time writing my thesis went to
try to get my thesis to actually compile and show up as a pdf.

If i'd write my thesis today, i'd write it in Markdown or Restructured Text
(RST; which i don't know, but understand is way easier than LaTeX and way more
powerful than Markdown). If it's mostly text, you can do with Markdown. If your
text contains mathematical formulas -- every good thesis should at minimum
contain a table, a figure and a mathematical formula -- you're probably better
off with RST.

I would not touch Word.

Then, create a workflow to create a beautiful document from your source code.
My suggestion is that you use Pandoc, typesetting your PDF document with LaTeX.
And if you have some downtime, use Gitlab CI to automagically build a new pdf
(and epub!) for each push to your master. If there's any interest, i can try to
find the `Makefile` i've successfully used later to do this.

## Make and maintain a reference list

Most of the time writing my thesis was spent reading others' articles. Articles
are good because they're short and mostly to the point -- reading a thesis as
background material can be valuable but it also takes time!). Don't worry about
finding the "best" article to cite, just search for something
([Google Scholar](https://scholar.google.com) is your friend) and look what
articles are cited there. Keep a well groomed list of references on your
computer. Include _why_ each reference might be interested, whether you read the
article and whether there actually was something worthy within. If you're using
pure LaTeX, this reference list can be used directly in your thesis, _even for
entries you did not use!_ Other formats may also support this, or you may have
to do some coding.

Reading an article on paper is way more ergonomic than reading it from a screen.
Using a highlighter and annotating on paper is satisfactory. Reading a technical
paper on a bus will make you look smart in the eyes of people you'd like to
look smart to.

Sometimes you find something interesting that doesn't actually fit within the
scope of your thesis. Don't throw this away, but also don't "force" it into
your thesis. Save it for a later day.

Many technical papers are behind a paywall, but practically all of them have a
page with the abstract and their author. If you can't reach your article, just
breathe in and send an email to the author of the paper (or authors, but send
one email per author, not a mass email to them all) and ask if you can have a
copy. The worst thing you can get is a no, the best thing you can get is an
academic who is interested in your work! (no, i wouldn't go so far as to say
"an academic who's interested in _you_", but ymmv ;)

## Have a schedule

Most of the time writing my thesis went to reading [Piled Higher and Deeper](http://phdcomics.com/). I would show up at the lab in the morning,
spend all morning until lunch reading PhD, have lunch, maybe read some more Phd,
see that it's already afternoon, join the two-o-clock lab coffee, head back to
my machine, desperately try and find some interesting articles, maybe read some,
change the order of a few sentences and go home feeling miserable.

Don't be me.

Start the day by doing something productive. Save the Piled Higher and Deeper
comic until some time slot before lunch or lab coffee, which'll give your slot
a natural time box. Give yourself sensible days and end the day with some sticky
notes on what you should do the next time you come to your computer. And that is
what the productive thing you should start with the next morning.

Have a schedule when you meet with your advisor. Learning from how i work now,
schedule a five or ten minute meeting _each day_ with your advisor. This will
keep you honest and not make unnecessary performance pressure for a "Daily".
Also schedule a longer meeting _N_ times a week (and one on a recurring schedule
with your professor, if you can). The longer you have between meetings, the
harder it will be to have one.

I didn't meet often enough and in nothing that looked like a schedule. Don't
be me. That's probably a good enough advise to encapsulate all of it :)

## Know when it's ready

You can tweak your thesis forever. There is always one more thing. Still, you,
and your advisor, will have to know when to say it's ready. For it's never
_done_ -- and with digital technology, it doesn't have to be. Instead of having
the pressure of "is this well enough?", just call a well-enough version 1.00
and put it in print. You can always write a 1.0.1 and a 1.1 and so on with your
online version. You can even provide a "patch" on print to your near and dear.

And finally, for now, know that there really are just two people on Earth that
actually read you whole thesis from cover to cover: your advisor and you. Your
professor will thumb through the first few pages, make sure his/her name is in
the Foreword, estimate the length of the References at the back, and finally
ask your instructor what s/he thinks. Your parents will look and probably even
read the abstract and then look at the thankyou notes in the foreword (an be
really happy if they find themselves there -- they did after all finance most of
your life up to this). Your friends will browse through the book and look for
pictures. How do i know? Because i hid an easter egg somewhere inside the thesis
and nobody has called me on it yet. But for you, it is the world and it's your
soul between those covers. Value it. You're worth it.
