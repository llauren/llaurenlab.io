---
title: "Talks ... eventually"
date: 2019-03-27T08:32:21Z
lastmod: 2019-03-27T08:32:21Z
description: "Sometimes i talk. A lot."
#menu: "main"
weight: 30
keywords: ["speaking", "talks"]
topics: ["talks"]
tags: ["speaking", "public speaking", "macaduk", "humane security"]
meta: false
featured_image: '/talks.jpg'
---

Calling this section talk**s** is a bit of an overstatement. Basically, i've
just given two iterations of the same talk and that's it. So a talk-and-a-half.
But it's a start, and i hope to give another one one day, because it's scary and
it's a thrill (once it's done!).
