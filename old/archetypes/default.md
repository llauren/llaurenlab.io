---
type: "post"
title: "{{ replace .TranslationBaseName "-" " " | title }}"
date: {{ .Date }}
lastmod: {{ .Date }}
description: "description"
draft: false
keywords: ["key", "words"]
topics: ["topic 1"]
tags: ["one", "two"]
---
