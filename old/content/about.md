---
title: "About"
date: "2018-02-24"
lastmod: "2018-03-10"
menu: "main"
weight: 10
draft: false
meta: "false"
---

Hi. I'm Robin. On the internets, i'm known as llaurén (which you can pronounce like "Robin" if you wish).

I work in the intersection of humans and technology. I used to study Human-Computer Interaction (HCI, related to Usability), later forked on to HCIsec ("Secure Usability"), and eventually became a Systems Administrator. Now i work at Reaktor, and i can't think of a better place to work.

My weapons of choice are Mac, Linux and hugs. On the server side of things, i subscribe to the _automate everything_ ethos, though i can't say i automate everything with elegance. I do my best to write Puppet scripts and Ansible. I just discovered
Terraform and i want to learn about Docker and Kubernetes.

When i'm not swamped in work, i tend to get Imposter Syndrome. Those two extremes aren't the best to live with, so i'm working on both. I've come to the decision that everybody's good at something -- even me --, that nobody knows everything, and that if i want to become better at something, i can. And that it's okay to admit it, and to ask questions. I will also blog about it, as an explorer, not as the authority.

While someone will always know more of something i write about, someone will also
always know less, even if that someone just is old-me. If someone knows more than
me, there's also the chance that i too can learn more. This is what happened to
me when writing about [link aggregates](/posts/link-aggregates-and-virtual-lans)
(or "trunks"). I try not to be ashamed of my lesser knowledge, but rather try to
enjoy that i'm learning.

I've overcome my fear of singing in public. I enjoy progressive music, tinkering with computers and excellent beverages such as coffee, beer and rum. I used to do radio, which i long back to from time to time. I'm allergic to furry animals, which is a shame, since i like furry animals (i have high hopes for gene therapy).

I am not [Robin Lauren Janssen](https://www.robinlauren.com/about). She has much more taste than i. Looking the way i do, i would love to work together with her.

These pages are served on [Gitlab Pages](https://about.gitlab.com/features/pages/) using the [Hugo](https://gohugo.io) static web site generator and [Gitlab Runner](https://docs.gitlab.com/runner/) automation. This means that i edit stuff on any of my computers, push the changes to Gitlab, and if all goes well, the pages are updated. Boom. Near-magic. The layout is a bit uneven to my tastes. I'm working on fixing that.
