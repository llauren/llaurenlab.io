---
title: "Now"
#date: 2018-02-24T11:06:14+02:00
#date: 2018-07-30T22:12:20+03:00
#date: 2018-12-16T14:21:40+02:00
#date: 2019-01-12T14:52:40+02:00
#date: 2019-03-31T09:51:42+00:00
date: 2019-06-24T12:00:42+00:00
draft: false
menu: "main"
weight: 100
---

I'm in an amateur theatre project, playing Pippi's father Efraim Longstocking, in
the _Finns Sommarteater_ production Pippi Långstrump (that's right, Pippi
Longstocking). And i have a small role as Starke Adolf in that same play. If
you're fluent in Swedish, you can read all about my experiences in [Efraims
Logg](https://llauren.gitlab.io/kalla-mej-efraim/).

Theatre has taught me to get over another threshold of public performance fear,
and how to dance and sing (simultaneously) on stage while actually having fun. I
was never much to learn things by heart, so learning my lines was also a bit of a
challenge. These days, Pippi and i ad-lib most of our lines and it all works
wonders. And i truly never grokked dancing. I still don't _dance_, but at least i
move in some kind of synchronisation with the rest of the people on stage, so i
guess that's something.

We play through June, which means it's only this week left, after which i'll be
faced with vacation and a gaping social vacuum.

## What Now?

Derek Sivers suggested one should have [a now page](https://sivers.org/nowff) on
one's blog to tell the world what one is doing _now_ (as to remind oneself what
one has decided to do, and to reduce unnecessary contacts from strangers ... among
other good reasons).
