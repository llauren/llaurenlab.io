---
title: "Doctor Who and the Krikkitmen"
date: 2018-07-09T10:02:01+03:00
lastmod: 2018-07-09T10:02:01+03:00
description: "Douglas Adams lives again"
draft: true
keywords: ["book", "words"]
topics: ["reading"]
tags: ["dr who", "reading", "book", "42", "dna"]
---

I've read a book! ... which for me actually is kind of an achievement. These
days i mostly read stuff from a screen and regardless of the medium, it tends
to be either technical or trivial, not prose. I really should read prose more.
Fiction. Books. Not technical books ... well, not only technical books at least.
<!--more-->

_Doctor Who and the Krikkitmen_ is a book written by James Goss, based on a
_nearly_ finished story by the late, great and dearly missed Douglas Adams.
It ties in very cleverly with the Hitch-Hiker lore in a way that nearly and
neatly explains the Krikkitmen (who in the HHG universe saw all other life in
the Universe as undesirable and as a solution, wanted it eradicated) and their
back story. The book is chock-full of "adamsisms" and while some could see this
as plagiarism, i just felt the presence of Adams on every page. It's clear that
Goss has done his homework, of which there was an abundance, and that he has
treated it with respect and love.

Unbeknownst to me, the Krikketmen were actually first imagined in the
