+++
title = "Google Wifi Is Nearly Great"
date = 2018-09-23T20:48:29+03:00
draft = true
tags = ["network", "wifi", "dns"]
categories = ["networking"]
+++

Having received an unending amount of biting criticism about the crap wifi
in our house, i finally caved in and solved the problem by throwing money
at it. Quite a bit of money, actually, and in a way i'm not incredibly
proud of. I bought us a Google WiFi.

First things first. Google WiFi is an access point (boring) or three
access points (or just "points" in Google Wifi parlance) which covers your
whole home with uninterrupted wireless fidelity. The system is pleasingly
easy to set up and the app is great. I'd recommend it to almost any normal
person out there, including my mother. Not only is it easy to set up, but it
can be monitored and maintained remotely, out of the box. Still, for a geek
like me, Google WiFi does come with some limitations, and i do feel like i'm
not the one in charge at times.
<!--more-->

Google Wifi is really two things: a router and a wifi infrastructure. Each
point has two Ethernet ports (WAN and LAN). You connect one "point" straight
to your Internet connection and it'll become a router. The others you can either
just power up and they will become part of a mesh network, or you can connect
them through the wired Ethernet (WAN) connection, whereby they uplink over your
wired network. You can also use a Point to wirelessly bridge two segments of
your wired network by connecting a switch to the LAN port of the point. Just
make sure you don't create network loops while building your network. Though i
haven't tried it, i wouldn't be too surprised if you can do these two things
simultaneously; both uplinking your Point through your in-house LAN, and
wirelessly extending your LAN, though in all fairness, i don't see why you would
want that.

{{< note msg="High-geekity content for the next few paragraphs. You are allowed
to skip or zone out." >}}

Actually, you don't _have_ to connect the first Point straight into your router
if you know what you're doing. I've connected my router to a managed switch,
like so:

```
 \  /         _________
 _\/_        | Managed |      ________        _______          _______
| 4G |       | switch  |     | Switch |      / Point \        / Point \
|____|-------|_________|-----|________|      \_______/        \_______/
                |  |                             |
                |  |                             |
               _|__|__                       ____|___
              / Point \                     | Switch |
              \_______/                     |________|
```

The architecture was a bit unintentional. I was supposed to connect the second
GWifi Point to the wired LAN, but i was a bit hasty when building the network
and i've been a bit lazy fixing something that, in essence, works. It's just a
bit ugly. For instance, the 4G Internet link already has NAT, so my network has
double NAT at the client side. Not that i've experienced any problems with that,
but as said, it's a bit ugly. The switch in the middle of the picture connects
to my home automation stuff, the switch at the lower right connects to Apple TV
and things like that. Which means i'm streaming Apple TV over WiFi, which isn't
optimal.

So why's the managed switch? Well, i still want a way to configure the 4G. And
i thought i wanted to create a transparent firewall, or a dual Internet uplink.
I just haven't got around to actually _doing_ it ... yet. Google WiFi doesn't
expose its firewall settings at all. I just assume it's there and doing its job.
Whatever happened to _Trust but verify?_ Still, there is no way that i can
firewall those cheap webcams i got from AliExpress from connecting back home.

Anyways, i'll try to rewire my net and update the picture above.

End of geekity bit. You can zone back in again.

So far so good? I'll come to the bad bits in a minute.

Google WiFi also has a very nice app with which you can check the status of
your net (on-site or anywhere on the Internet). You can identify the things
on your network, give them names, group them together and if needed, block
devices or groups from the Internet... or more to the point, you can mark
which of your things belong to your kids and then give your kids some off
the net -time. You can even give the whole family some Internet Pause, which
actually sounds like a rather fair idea.

Now i told you that Google WiFi is a bit limited. Not a lot, but if you're
_really_ into networking. You can manually set your subnet if you don't like
192.168.86.0/24. You can set which DNS server to use if you want to use an
internal server or if you just like some other server more than Google's.
This becomes relevant if you have servers or home automation stuff on your
network and would like to address them by name rather than IP address. And
you can reserve "static" addresses for any device you like.

Google WiFi comes with rudimentary DNS on the LAN, though i haven't seen this
documented anywhere. My devices get a `${hostname}.lan` address, which sometimes
resolves and sometimes requires me first to ping the device before it does, so
it't not really useful yet, but maybe something Google will improve. If you in
the meantime _really_ want robuster name resolution, i suggest you look into
[http://www.thekelleys.org.uk/dnsmasq/docs/dnsmasq-man.html](dnsmasq), but in
reality, this can be a bit dodgy.[^](I haven't found a way to switch off Google
WiFi's dhcp server, so you won't be getting automatic/dynamic hostname
registration; rather, you'll have to reserve IP addresses on the GWifi and
manually configure these into dnsmasq ... which in the end probably has a lousy
ROI.)

Google WiFi has a guest network. While i did configure it, i haven't tried
it yet, so i don't know if it's on a separate LAN. What the Guest LAN does provide
is access to given services on your private network, meaning that your guests
could be allowed play music or show photos on your Chromecasts and Apple TVs.

Speaking of separate LANs, what i'd _really_ like is a separate "IoT LAN" where
i could hide my dodgy home automation and allow my private network users
access to the home automation server in the very same vein the Guest network
devices can reach given private network services. Can't use the Guest network
for that, as i have wired home automation devices.

And i can't create a home automation network outside the Google WiFi either, as
the GWifi doesn't support static routing. Yes i know, i'm in the minority to
want these features, but these are also the reasons why GWifi feels like a bit
of a cop-out to me.

One feature i haven't used at all is port forwarding. If i'd have a home
automation LAN or a VPN server on my net, i might use it.

In an ideal world, Google WiFi would support at least one more LAN for IoT and
home automation (using VLANs for all i care), have better local DNS, static
routing, VPN, the ability to switch off the DHCP server ... and a reset button
for when i configure myself out of my network, and version control for my
configs. But the truth of the matter is of course that if i want features like
these, i'm not really in the Google WiFi customer demographic :)
