---
title: "Hello, World"
date: "2018-02-21T18:02:38Z"
draft: false
tags: [ "hugo", "gitlab" ]
categories: [ "meta" ]
topics: [ "meta" ]
---

Well would look at that -- it's a blog post!

I haven't really toyed around with this thing for a while, but i somehow got
the inspiration now, so why stop a good inspiration when you have it?

So what is this thing? <!--more--> Well, it's a web site, or more specifically
a blog, which is generated and hosted on [Gitlab](https://gitlab.com) and is
built on a system called [Hugo](https://gohugo.io/) with the
[Kiera](https://themes.gohugo.io/hugo-kiera/) theme. Hugo eats markdown files.
When i push these to to Gitlab, a little robot (a _Runner_ in Gitlab parlance)
translates it to a bunch of web pages and that's what you're seeing now (if you
aren't watching [the markdown source](https://gitlab.com/llauren/llauren.gitlab.io/)).

I would actually want to have the pages end up on my web server rather than on
Gitlab, but maybe i'll be able to do something about that too once i learn some
more. I might even write up something about that.

I wish i could write exactly what i did to get this thing running, but it's
based on efforts i did a year back and then refreshed today. But the much of
the process is described [here](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/)
