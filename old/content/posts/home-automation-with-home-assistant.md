+++
title = "Home Automation With Home Assistant"
date = 2018-12-16T13:49:00+02:00
draft = false
tags = ['home assistant', 'mqtt', 'esphomelib', 'IKEA', 'Trådfri']
categories = ['home automation']
+++

I've been interested in home automation for ten or fifteen years now, probably
since writing my thesis on home networks access control, quite some time away.
My view of a well made home automation is a home that responds to its inhabitants,
in some cases even _anticipates_ their wills and wants. Well, anticipate may be
a bit of a stretch, but even "infers from" is already sufficiently close to magic.<!--more-->

My first home automation thingy was far from the magic of inference (and to set
the expectation, it still isn't anything magical). In effect, it knew to turn on
the garden lights at sunset and turn them off at 23, then turn them on the next
morning at 06 and turn them off at sunrise. To make this compatible with the
time of year when the sun set after 23 or rose before 06 required some thinking
:)

It was, however, a distributed, multi-tenant thingy which switched the garden lights
on and off with the solar schedule. It was built using [homA](https://github.com/binarybucks/homA),
essentially a glorified MQTT[^mqtt] framework, running on my home server[^server],
an [iTead iBoard](https://www.itead.cc/iboard.html) and a set of relays bought
on the cheap from the Internet. It worked, to some extent, fine. It was in no
way a refined home automation, but it was mine. And i even think i wrote my first
Python script to twiddle some MQTT messages down the bus.

Eventually, i realised that HomA was going to be a bit too unextendable for my
skills, so i started looking for some "proper" home automation software. I found
some Java thingy ([openHAB](https://www.openhab.org/), i believe) but it was way
too heavy. I found [MisterHouse](http://misterhouse.sourceforge.net/) but it
seemed like an unpenetrable pile of Perl to me, so i let it be.

And finally, i came across Home Assistant. It knew MQTT and it was everything i
needed. Well, almost. Home Assistant came with "some assembly required", which
means it did exactly nothing when installed, had documentation that didn't help
me much, and was _very_ finicky about how to spell things out. But i got it running
and i got it broken in several interesting ways. I also learned to love git as a
way to recover from my configuration updates. It was a wonderfully convoluted
system, it knew about who was home from the DHCP reservations from my MikroTik
router and it could switch off the car heaters using that information when my
wife or i left for work.

For the longest time, i didn't really extend my system. I did exchange mechanical
relays to solid state ones and the iBoard for a Raspberry Pi since i couldn't find
the source code for the iBoard and i _know_ it was hairy enough that i didn't want
to re-write it, but these were incremental updates.

For the longest time, i wanted to add some DIY stuff, or at least, hacked
commercial gear, but i just didn't have the time. That changed a little while
back when my wife wanted to buy a LED roof lamp with adjustable intensity and
colour temperature. The lamp did not have Home Assistant support, so that was a
big no-no. And off i go, the same evening, to IKEA, and _finally_ buy a small bag
of Trådfri devices, dimmable, with adjustable colour temperature, one motion
sensor and one controllable power socket (shoulda bought more, they ran out just
after i bought them and they haven't been in stock since). The Trådfri lights
and gateway were a delight to integrate with Home Assistant and the only thing
i'm missing these days is the presence detection my router provided, as i
updated my network infrastructure to Google Wifi. More about that in another post.

The entrance lights now turn on in the morning and go out when the home alarm
is rigged. The car heater now also takes outside temperature into consideration.
And soon, the IKEA lights will change colour temperature with the clock. Sure,
it's not a lot, but it's something.

I still have hopes to integrate those [Sonoff](https://www.itead.cc/smart-home.html)
relays and thingies i bought. Otto Winter has built [esphomelib](https://github.com/OttoWinter/esphomelib)
(on Tasmota, which i never could get running) with which i have managed to flash
two of the simplest relay boxes, but for the more complicated ones, i'm still
clueless. **Update:** I see there's a module for the
[4CH](https://esphomelib.com/esphomeyaml/devices/sonoff_4ch.html) relay box, so
maybe there's one coming for the
[4CH Pro](https://www.itead.cc/smart-home/sonoff-4ch-pro.html) that i've got too.
The Pro has some really weird internal serial protocol to manage the relays, which
is just too strange for me to properly comprehend.

Every year, i'd like the christmas tree to have a sensor to warn if the
water is running out, and a pump to add some water to it (and a sensor to warn
if the water container with the pump is running dry!), but each year nothing gets
done. It's kinda disheartening. At least the christmas lights work on timer (though
not connected to Home Assistant yet, since the iTead Wifi to 433 MHz bridge it
really opaque to me).

In any case, home automation is still a fascinating topic, and with Home Assistant,
i can extend it on my pace, with my skills, on my gear. When or if ever i have the
time :)

[^mqtt]: Ramble down memory lane: I first found out about MQTT on Phil Windley's
Technometria podcast, when it still was a podcast on IT Conversations (and when
IT Conversations still existed). When [Andy Piper](https://andypiper.co.uk/)
presented MQTT, i was enthused by the clarity. I still like MQTT for its
simplicity. None of those RabbitMQ complexities. Or (unnecessary) features.

[^server]: The server crashed badly on a power outage, taking much of my music
and some of my photographs with it. Thankfully, it crashed during the summer
when there was little need for garden lights, so i had a few months to make'm
blink again. On a lighter note, i soon after started paying for the privilege to
stream music and films i already owned. The server was eventually replaced with
two or three Raspberry Pies and an UPS.
