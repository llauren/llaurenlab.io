---
title: "Humane Security"
date: 2019-07-23T14:19:07+03:00
tags: ['humane', 'security']
categories: ['humane security']
---

I would like to explore the idea of _Humane security_ as an alternative to
_Mandated security_. None of these terms exist, or, at least, are established
terms from before, so allow me to explain what i mean.

In traditional computer security, corporate security, public sector security,
and basically any high security environment, there's a power structure where the
user is told what to do and what not to do and that's the end of it. There's
very little wiggle room or room for discussion. Do this or face the reprimand.
No need to understand why. Follow the rules.

In what i would like to coin Humane security, i would like to turn the top-down
security imperative on its head. The job of the security responsible is to
enable a secure but desirable environment and foster a secure culture. In this
secure culture, security is not a threat or a weapon, but the foundation on
which to build a safe and secure environment where everybody understands why
security is important.

Trying to parse this out will be my challenge.

Consider for a while, what kind of environment you would like to work in. You
don't need a separate work persona and off-work persona. You can talk with
people. You can tell people that you have a bad day or that you want to leave
early because your kid is having a concert. You are trusted and you are valued.
You are listened to and you listen to others. You don't dread or fear. Things
just work. You feel like you can do your work. You do the right thing, because
that's the right thing to do.

These are markers of _safety_, the subjective experience of security. In a
_safe_ working environment, it's okay, even encouraged, to tell if you found, or
did, something that is against security. Doing so, will not get you reprimanded.
Discussing security problems and even omissions will be a learning experience
for you and for your org, and in the end, your org and the people within will be
_more_ secure. Allowing people to be human sounds like a fundamental component
for humane security.

Of course you need to operate with security in mind. You need to understand that
you, and what you do, is part of everyone's security, but doing so shouldn't
feel like an unnecessary chore. It's something you do because you understand why
you do it, and doing things the secure way isn't overly complicated. It's a
balance that tips towards "it's worth it".

Security can actually be a relief. Once you have a system in place where it's
easy to do the right thing and hard to do the wrong thing, you know that you can
do your stuff without much risk of causing a catastrophe. It's calming, and you
can concentrate on doing your work. Good security is an enabler, bad security a
hindrance. Thus, humane security, done right, is good security.

Still, as i started this post, this whole thing of humane security is still just
a little inchling of an idea which i'd like to see grow. If somebody steals it
and develops it as their own, well, tough, but in the end, maybe everyone will
be more safe and secure, and i shouldn't let my ego stand in the way of
something that i feel is truly important.
