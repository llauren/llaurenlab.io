+++
title = "Macaduk Speaker Imposter Syndrome"
date = 2019-02-07T10:48:31+02:00
draft = false
tags = ["macaduk", "speaking", "imposter syndrome"]
categories = ["public speaking"]
+++

Aaaaaagh! Looking at the [MacADUK 2019 speaker list](https://www.macad.uk/2019speakers/),
i'm now _completely_ overwhelmed by imposter syndrome. All these geniuses ... and me?! 

Halp!
