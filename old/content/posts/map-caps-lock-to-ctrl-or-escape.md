---
title: "Mac tip: Map Caps Lock to Ctrl or Escape"
date: 2018-08-29T13:26:05+03:00
draft: false
tags: ['mac', 'osx', 'keyboard']
categories: ['mac tips']
---

Caps lock? Show me a programmer or a system administrator that actually uses
caps lock for producing CAPITAL LETTERS. Actually, don't. But the point is,
CAPS LOCK as a toggle for typing in SCREAM CASE versus normal case is really
an outdated concept. That's why i mapped my Caps Lock key to be a control key
instead.<!--more-->

To do this -- on a Mac -- open System Preferences, then the Keyboard prefs,
then, on the Keyboard tab, press the Modifier Keys button. Change Caps Lock to
map to Control. Or Escape, if you're a vim person.

Ah! A dilemma!

What if you _are_ a vim person, but use Emacs key bindings in the shell (like
any normal person :) ? And you're mighty tired that the Escape key has been
relegated to the Touch Bar? Then you need
[Karabiner Elements](https://pqrs.org/osx/karabiner/index.html). Karabiner
Elements is a re-write of the good old Mac keyboard modifier app Karabiner,
re-written because the security model of modern macOS was incompatible with the
app as it was.

Install Karabiner Elements and open up its Preferences. The Preferences window
will probably pop up the first time you run Karabiner, but if can also be
summoned through the box shaped icon on your menu bar at the top of the screen.
Open the "Complex Modifications" tab, choose <i class="fa fa-plus-circle
fa-small"></i> Add Rule and press
<i class="fa fa-cloud-download fa-small"></i> Import more rules from the
Internet. A web brower will open and a page will appear. Import the
`Change caps_lock (rev 2)` rule set. Then activate the rule `Change caps_lock to
control if pressed with other keys, to escape if pressed alone` and you're done!

If you _really_ want a Caps Lock to activate SCREAM CASE on your keyboard, you
could always try importing the `Change shift key (rev 2)` rule set which has
the rule `Change left_shift to caps_lock if pressed alone (rev 2)`, which works
totally fine with the caps lock to esc/ctrl rule, _and_ lights up the Caps Lock
LED. Because you did miss that one, didn't you ;)
