---
type: "post"
title: "Been singing"
date: 2018-07-06T21:51:20+03:00
lastmod: 2018-07-06T21:51:20+03:00
description: "The last ten days, i've been singing"
draft: false
keywords: ["mwi", "music", "singing"]
topics: ["music"]
tags: ["music", "singing"]
---

The last ten or eleven days, i've been on a music camp with my kids.
Well, mostly it's been the kids doing the music and me looking out
during the night. But i was in the "adult ensemble", where we practiced
these two songs, _Alta Trinità beata_ and _I skovens dybe stille ro_.
The first of these is an old Italian religious song, the other a Danish
folk song.

Since i'm not much of a singer and because i don't read sheet music
very well, i try to teach myself my stuff by transcribing the notes
into a notation software (called [MuseScore](https://musescore.org/en)).
To make my job easier, i also purchased a 50€ mini keyboard, and oh
boy entering notes is faster with a musical keyboard than the one on
a laptop.

I then set the other voices to pianissimo and export the song as a sound
file.

Because sharing is caring, here are the notes:

* [I skovens dybe stille ro](/skoven.pdf)
* [Alta Trinità beata](/trinita.pdf)

...and the different voices for "Skoven"

* [Soprano](/skoven-sopr.mp3)
* [Alt](/skoven-alt.mp3)
* [Bass](/skoven-bas.mp3)

I can create the voices for "Alta" if anyone needs.

Oh, and i sang Lazarus and Life on Mars with [Folke Gräsbeck](https://www.wikiwand.com/fi/Folke_Gr%C3%A4sbeck)! If anyone got this on video on the
Night Concert, i'd be delighted! :D

Share and enjoy!
