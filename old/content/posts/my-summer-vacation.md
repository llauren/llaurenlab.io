+++
title = "My Summer Vacation"
date = 2018-07-30T22:10:00+03:00
draft = false
tags = ['self', 'me', 'summer']
categories = ['self']
+++

{{< note msg="This is a totally untechnical, unapologetically self-absorbed post." >}}

A month back, i wrote a post "[Help, it's summer](/posts/help-its-summer/)",
discussing (or rather, lamenting) how hard it can be to wind down and how i
should "work" at allowing myself not to do anything work related -- if i could
only think of what _that_ would be.

Well, all in all, i think i've done a pretty good job.<!--more-->

While i _could_ write about the importance of having some spin-down time and its
effects on the society as whole, i'll just leave it as an exercise to the reader
and jot a quick note on myself. I need to reboot my writing and this is an
easy start.

First, i spent a week and a half at music camp with my kids. I even did some
[singing](/posts/mwi-camp/) (sorry, terrible post). I then spent a week and a
half at the cottage. The summer has been insanely warm this year, so being at
the countryside is a plus. We're next to the sea, which cools things down a bit.
And then i went to Madrid with my son for the Speedcubing [European
Championships](https://euro2018.cubecomps.com/en/), which was even more insanely
warm. And then another few days at the countryside before heading back to work.

And i've read a book, which is sadly unusual to me. I started blogging about it
but haven't finished the article just yet, which is way more usual to me :)

Sure, there were things that needed to be done back at the job, but frankly, i
don't mind. It's been good to be on a break and it's good do interesting things
at work. Like untangling recursive dependencies with the CI after i updated a
TLS certificate which only reached the CI server but not the git server it was
connected to, after which it wouldn't fetch more jobs and update the puppet
server to eventually update the git server ... but hey, who needs simple
problems anyway? :)

_(eventually, i solved it by copy-pasting between terminals, which didn't make
me proud but did make the CI server happy, so it's a case of "whatever solves
the problem")_
