---
title: "Renew Your Cert (before it expires)"
date: 2019-06-01T03:07:45+01:00
draft: true
tags: ['ssl', 'tls', 'certificate']
categories: ['sysadm']
---

Here's a free tip. If your Let's Encrypt SSL certificate expires on the last of
May, don't renew it on the last of May. It might just be that it expired
_earlier_ during the last of May. So renew it no later than the day before it
expires. And put that day into your calendar. Otherwise you may need to update
your certificate [†he hard way](/posts/renew-gitlab-cert-the-hard-way/).

This is exactly what i had to do, again, despite my previous warnings to myself.
And doing so, i also found out a few gotchas. Gitlab Pages will keep forcing
your pages to use https (or more correctly, forward your http requests to an
https url) for a while after you've unchecked the box for that very action. And
the `certbot` thing with which you might want to update your certificate with,
_might_ have a teeny little bug which confused the heck out of me. When renewing
the cert, it shows the verification data and corresponding Python code to
generate it, and asks me to press Enter. Then it shows what seems to be the same
thing again. Except it isn't. The verification code is shown that _second_ time.
Ignore the first verification code information if you, like me are repeatedly
hitting your head against the SSL wall because the verification process didn't
go through (as GitLab will forward all http requests to https, as mentioned
above, until it doesn't or until GitLab fixes that bug).

Now let's add the _28th_ of August into that calendar!
