---
title: "Running Presenter Decks"
date: 2018-05-26T23:28:28+03:00
tags: ["conference", "slides", "breakpoint"]
topics: ["conferences"]
keywords:
draft: false
---

Last week, i was involved with [Reaktor Breakpoint](https://www.reaktorbreakpoint.com), a tech, design,
business and future conference with about 600 attendees and twenty-odd speakers.  

Unlike with last
Breakpoint in 2015, i wasn't involved in the planning bits, i just worked with the execution. On the
surface, my job sounds trivial. I was to make sure all presenters' slides were delivered to the right
stages, in time, before each presenter's show, and looking *awesome*.

I like to emphasize the last word of the previous paragraph, because _that_ is your job and the job of
nearly everyone working backstage or in production: making the presenter look, sound, and feel awesome.

While delivering slides may sound like the job of a glorified runner, it was actually quite a bit of
logistics than i'd originally expected.
<!--more-->

At Reaktor, we produce a lot more tech and design than we produce tech and design events, especially of this size. We had 
one main stage, three side stages, one "fireside stage" and two workshop areas. We had 600 people in 
the audience, we had 36 speakers -- about of the third of whom reaktorians -- a staff of about twenty 
reaktorians and quite a few externals for sound, light, pictures, feeding, imbibing, building and the such.
All stages had pro audio, video and streaming crew and equipment and all presentations from the stages 
were streamed live on the Internet and will (probably, or to some extent) be released later.  The "sensible
default" was to show the speaker's slides (or _decks,_ as we like to call them) on presentation machines, 
managed by the stage A/V crew, for reasons i'll outline in a minute. There was no local area network 
connection between our production office and the stage A/V crew, though we did have Internet. This means that 
the presentations would have to be delivered to the A/V crew by Sneakernet, ie. by running.  Sometimes 
walking, a few times by kick-bike, but usually by running.

The suggestion that the speaker slides would best be presented from the stage computers actually came from 
the stage techs themselves (though they were immensely proffesional and flexible and would do anything to 
show anything using any tech available, acquirable or, if needed, createable).  There are a few reasons
for this:

* Presentation computers are set up right for the occasion. No fussing with screen resolution,
  orientation, or which screen is which.
* They have these industrial grade clickers that don't get messed up by all interference in the hall
    (and they have wicked lasers which work in halls that size -- and could be used to quickly obliterate
    inconvenient people from the audience if one were so inclined)
* If presentations contain sound, sound levels and output are set right. Our techs had the audio
    routed over 3.5mm audio cable, not embedded into the HDMI (see first bullet above), because after all,
    their audio mixer eats audio, not HDMI (which goes into the camera mixer).
* The "comfort monitor" will be connected, and it _will_ show the speaker notes and the next slide
    without the speaker having to fuss around with the Switch Monitors icon.
* The hall techs will (should) have a Mac and a Windows box. The Mac needs both Keynote and PowerPoint,
    the Windows box obviously only PowerPoint. And both need Adobe Reader or some other PDF thingy which
    can go into Presentation Mode (full screen, where arrow keys will advance a slide or go back a slide,
    not scroll almost a slide). Also they both need a standards compliant web browser that goes into
    Presentation Mode. 

Before you consider pushing decks over the Internet, let me just say that conference networks _can_ be
unreliable, and that decks that include videos can be really large, which you'll curse when trying to
download or upload them over that flaky connection. Or the networks might be reliable but overcrowded.
These are the times Sneakernet really can be faster than Internet. This we realised first hand when
downloading the presentations we already had from our Google Drive to local storage.  Now if you _had_ a
(bullet proof, wired) LAN between your production office and the stages, that might be different, but
you'd _still_ want to make copies, and backup copies, of all decks. Just in case. In an ideal
world, all stages and the production office would be connected, the decks would be in a repo and be
synchronised to the stages and everything would be beautiful. One fine day, i'll laugh at this backward
use of technology, but until this, i copy and i run.

With that out of the way, here are ome things you might want to consider if *running* a presentation
(you know, with your feet). Take them as suggestions, not gospel. These are our experiences. Your may
vary. Here we go.

In good time before the conference, inform the presenters that unless there's a good reason not to, you'd
rather show the slides from dedicated presentation machines in the halls. Good reasons not to include
stuff like demos -- especially ones that require more than one physical machine --, presentations that run
on HTML and, well, just _weird_ things like reverse engineered Gameboys with engineering jigs that just
need to be shown with camera because they can't be connected to HDMI ind even if they did, it would make
no sense (yeah, we had one of those, _and_ he had a HTML presentaton).

Tell your speakers that they may not have reliable Internet at the stage and that the presentations need 
to be able to run 100% offline.  Live Internet is a plus, and if they have something nifty that uses the 
net, consider it a bonus. This means, ask presenters to make local copies of any videos they intend to 
show off the Youtubes and have a contingency plan B if something on the Internet they intended to show ...
won't.

_Sidenote from the speaker's point of view: in a situation like this, it's better not to lose your thread, 
apologise and stammer.  Your audience hasn't seen the presentation before and does not know that they're 
missing a bit. Or even if they have, **this** version does not include the bits that didn't work. Smile and 
wave to the audience.  Keep yer calm and carry yer on._

Really, really, really try hard to have the presentations delivered and "set in stone" at least 24 hours
before showtime. Yeah, i know, this isn't fully realistic, but really, try. It'll make your job just so
much easier. On a related note, be kind with your speakers and if they find an error in their slides,
help them get it fixed and into production.

Remind or inform your speakers that you also want copies of all non-standard fonts used in the 
presentation. The stage techs may not have them on their machines and neither may you.

_Nuther sidenote: If you're a speaker, you can read the above as "Your conference crew will love you and
remember you fondly if you have your presentation slides finalised and delivered before the conference."
You are the star, but it is our job to make you that._

Our Production Office was staffed by our Speaker Coordinator, who miraculously knew exactly which
speakers had arrived, received all speakers with a smile and a welcome, and connected them with their 
Speaker Hosts ... and answered a hundred questions (an hour) without breaking down, or even a sweat. I 
don't know how she did it, but Iina, you are a hero. In your Production Office, you should have have a 
schedule, where you mark which presentations you have ready. Paper is better than on a computer, 
especially since this is a communications tool for you and your Speaker Coordinator.

_(Terminology sidenote: A Speaker Host is a person who is the speaker's first point of contact with the 
conference when at the conference. The Speaker Host will make sure the speaker knows when and where to be,
shows them around the venue, takes them to the Speakers' Backstage and to the stage when it's time for
that, and just makes sure the speaker is welcome, equipped, sufficiently fed and caffienated, aware and
just generally comfortable.)_

OK. Let's get tehcnical. As i mentioned, presentation decks were distributed using USB stick and sneaker
technology.  Have one USB stick per stage, plus one stick for backup, plus one stick which you use for
copying the presentations from the speakers to your system. Attach a luggage tag to each USB memory stick
with the stage it's for in letters large and legible enough to be read backstage where it's dark.  The
stick you use to copy speakers' presentations to your infrastructure would do well to be one with both
USB-A and USB-C connectors. You know, for those hipsters with new Macs.

On each stick, have a folder for each stage. Thus, your "stage sticks" have one folder with the name
of the stage and your backup and transport/work sticks will have as many folders as there are stages.
Save each presentation as files or in folders inside the "stage folder". Be pedantic with file (or folder)
names. I  used one letter for the stage, the time when the presentation begins, the name of the presenter
and the name of the presentation, so something like `M1500 Joe Strummer - Blockchain as a tool for the
revolution.key`. Use 24h time notation for proper sorting. Use some character in front of the time if
it's a multi day conference (M, T and W sort, but if your conference goes over Wednesday into Thursday,
you're better off using numbers). A little redundancy in file naming can go a long way when you've
suddenly saved a file in the wrong folder or with the wrong speaker's name.

You will not be able to run sticks to the stages yourself. We had two admins with the job of making sure 
the backstage tech was working and that the slides were collected and distributed, ie Jani and me, and 
even that wasn't enough when things got really busy. There will be times when you have two shows starting 
in a few minutes and somehow you haven't run the slides there yet _and_ a new speaker just arrived on 
site. Thankfully we had two reaktorian staffers who happened to be available at the time, because when a 
speaker arrives, you want to get their deck and fonts and not have them wait. 

Also, you want to get the USB stick back from the Stage to the Production Office rightaway.

Remember what i said about making the speaker awesome? While some speakers may be heroes and superstars,
some can be pretty tense and nervous and you are part of the chain to make the speaker feel that
everything is alright, under control and at ease. Copy the deck and have a look at it on your machine
together. Most presenters will have nifty fonts in their presentation that you don't have, and that the
presentation computers won't have. Caveat: Even though you may be a font geek, your this machine should
not be filled with every available font, so that you'll see if something's off.  If you see your speaker's
slides with Times New Roman Bold font, _very_ diplomatically enquire if this looks like it should, because
while most speakers we had are typographically inclined and would never use Times Bold in their
presentations, someone might. Rather, select the text and check from your own Keynote or PowerPoint that
it's actually meant to be Times. At least Keynote on the Mac will tell you if there are missing fonts.
PowerPoint on Windows should be able to save the presentation in a "Pack and go"-format, which will
include everything needed for a presentation to run on another box. And to have copies of the appropriate
fonts.  Install these on your computer, then check again.  If you want to be truly pedantic, _remove_ the
fonts from your computer after you're done, so you can catch the next special font going to another stage,
even though you just got it from a previous speaker.

If the presentation contains video, ask for a copy of these too, if at all possible. Some streaming pros 
like to show videos straight to the stream without going through the video chain. It'll just look and 
sound better. In fact, depending on their setup, it might sound at all.

If your presenter is using Google Slides, you've got a special case. Cherish this person, get the link to
stage crew, make sure the presentation works there, and make an offline (PowerPoint) copy of the
presentation just in case. Check with the presenter if the Google Slides presentation contains live
Q-and-A, and make sure the stage crew and the presenter are in synch about this if they need to run the
offline copy that isn't interactive. You don't want to create a nasty surprise for the speaker when the
presentation is already happening.

If the presenter has provided you with only PDF slides, create a Keynote or PowerPoint deck of these. Your
speaker will then be able to see the next slide from the speaker monitor (or "comfort monitor").

When it's time for the show, the speaker host will lead the speaker to the appropriate backstage, where 
the speaker will be miked. This is a good moment to triple-check with the stage crew that the slides are 
really-really in order. Since you've seen the font choices at this time, you can decide whether you want 
to involve the speaker here or just do it discretely.

A couple of tips:

- PowerPoint on Mac will look _a little_ different than PowerPoint on Windows. You'll be best off in
  making your presenter look awesome by showing PowerPoint slides on the platform it was created.
- To make Google Chrome go into "chromeless" presentation mode, switch off the option "Always Show Toolbar
  In Full Screen" from the View menu (Shift-Cmd-F), and then go to full screen by "Enter Full Screen" from
  the View menu (Ctrl-Cmd-F). Google Slides will have speaker notes on a separate tab when you're not in
  presentation mode. Test before you go live.

All in all, this seems like an awfully long post just for showing slides, and it is. Most of it is really
just advanced common sense, and i'm sure you'll be alright whether you read this or not, or whether you
heed these tips or not. In the end, it'll be okay. You won't see many of the actual presentations, which
can be a bit of a bummer, but you get to hang out with the magic makers and see how the magic is made. Just
remember what you're here for _("For making the speaker look, and feel, awesome!")_ and have an awesome
conference!
