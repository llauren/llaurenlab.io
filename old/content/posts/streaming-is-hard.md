+++
title = "Streaming Is Hard"
date = 2019-03-02T10:46:06+02:00
draft = false
tags = ["streaming", "video streaming", "youtube"]
categories = ["streaming"]
+++

You'd think that creating a live video stream these days would be uncomplicated,
a "solved problem". Sure, things might be complicated by the fact that the
production has multiple remote controlled cameras, slides/video with possible
embedded audio, two audience screens (thankfully showing the same content, and
no speaker screen), live dubbing, a separate stage on another floor to show the
dubbed version of the presentation, streaming to YouTube in 1080/50p and
record the thing on local storage, oh, and keep an eye on the Slack channel for
questions and feedback from those on the other side of the stream. And you need
to monitor the main programme, the stream, and the signal that goes to that
other stage so that the video and audio is smooth. Thankfully, there are two of
us running the tech stack. I wouldn't have the cycles to run both audio and
video mixing.

But still, this should not be a _hard_ problem. Complicated maybe, but not hard.
Still, it is. We've been working on improving the technology for transmitting and
streaming our monthly meeting at work for years, and i feel we're getting closer
to the goal. Last Thursday, we had a nearly working setup. From my standpoint,
the central unit was a [Roland V-60HD](https://proav.roland.com/global/products/v-60hd/)
video mixer with four SDI video inputs, two HDMI video inputs and two XLR audio
inputs. We had two remote controlled PTZ Optics "robot" cameras connected to a
control panel. The cameras send video to the Roland mixer over SDI and they're
controlled over Ethernet from the PTZ Optics controller. I would have liked two
additional SDI cameras, but this time we only had a third HDMI camera. The
second HDMI input was from the presentation laptop.

The Sound Man (for that he is) has an IP controlled mixer, don't know what kind,
four wireless mics, one wired mic and one hall microphone so that the stream
doesn't sound dead. We also have live translation, so there's a guy in a meeting
room a few doors away, who has his own microphone and gets the video programme
on a wall display.

The Roland has two SDI outs and two HDMI outs, plus HDMI for multi-viewer. One
HDMI with the main programme goes to the streamer (which has a HDMI out, which i
pulled to the interpreter -- bit of a hack), one HDMI output goes to the two
audience screens. One SDI goes to the stage on the other floor and one SDI was
meant to be for the translator but i was one SDI to HDMI converter short, so we
had to draw this with a 15 m amplified HDMI cable. If you're counting, that
means four HDMI signals using two ports. This we solved with a distinct lack of
elegance by using a splitter and more amplified HDMI cables between the two
audience displays[^splitter] and the streamer's HDMI output for the interpreter.

It's possible to show only the slides on one output and the stream, which
sometimes contain the slides, on another. The stream is the final mix or
"prorgamme" (PGM) bus; the slides are on the "auxillary" (AUX) bus. You can
easily change which input is the AUX bus by pressing a button, so you could in
practice have two parallel shows if you wanted, except you can't do nifty
transitions between the picture sources. Still, it wouldn't matter, because i
only needed to show the slides on the AUX bus = audience displays.

And this is where it gets jiffy. The stage for the dubbed presentation. For
that, i used an SDI output to a really cute little
[Roland V-1SDI](https://proav.roland.com/global/products/v-1sdi/) video mixer
_(kawaii!)_ that has four video inputs, two SDI, one HDMI, one dual personality
SDI-or-HDMI, and RCA audio inputs. The interpreter's audio was fed to the RCA
audio in, all other video channels had audio muted (something i realised only
during the sound check). The V-1SDI has two SDI outputs, one of which was drawn
over a 70 m (!) SDI cable to the other stage, with the dubbed audio embedded!

At the other end of the 70 m SDI cable (we didn't use all 70 metres, so i don't
really know how long the actual cable pull was) was a fricken awesome
[Blackmagic Teranex Converter](https://www.blackmagicdesign.com/products/teranex)
which converts anything to anything at all, and it's kind of sad that we only
used it for receiving SDI and outputting it to HDMI for the big screen there.
Had i had more of these babies, i would probably have ended up with less problems
than i got.

So, most everything worked nice and dandy, _except_ the streamer, and i can't
really say if that was due to the tech or to my incompetence. The streamer was a
really nice [Magewell Ultra Stream](https://www.magewell.com/products/ultra-stream-hdmi)
box which takes HDMI on one side and spits out a stream on the other. There are
ready made profiles for YouTube, Twitch and Facebook, and you can create a
custom RTMP server profile if you want. The streamer is controlled with a mobile
device over Bluetooth, with an app that doesn't totally suck (only slightly).
The Magewell also has a HDMI out, which i mentioned above, a USB port for
recording, and 3.5mm connectors for microphone in and headphone out. But it
didn't work. At least, it didn't stream. Prepare for a rant.

Setting the device up was fairly painless though not perfectly obvious. You need
to install the Magewell Ultra Stream app on your mobile, which will discover the
streamer (oddly enough, it required location permission to work, but i was in
too much stress to question it at the time). You can stream over WLAN or wired
Ethernet. Setting up a streaming server is where it all went wrong for me. Since
this is a company event, i stream from my company account. The app allowed me to
choose my either of my personal accounts or an "other account", which sounded
correct. So i entered the company account info, which caused my (Android) phone
to wipe its current Work Profile and replace it with an empty work profile for
the same company. Bye bye to my shortcuts and settings, though in all fairness,
it did warn me that it was going to do something in that respect, but in all
honesty, i did not expext it to _wipe_ all my current Work settings. And in the
end, it was not able to log me in, because the work account did not "stick" to
my log-in-able accounts. Yeah, the Magewell app uses built in Android
authentication, which makes all the sense, but because the user interface looks
nothing like the Android interface, i didn't see it coming.

So i re-installed the Magewell Ultra Stream app on my Work profile, added
YouTube as a streaming service and authenticated without a problem. I even found
the well hidden setting to stream at 1080/50p instead of the default 720/30p.
Testing the stream from the app's user interface showed that the streamer would
be able to send data at 3-4 Mbps. Good bananas.

What the Magewell seemed to lack was the possibility to stream to a scheduled
event, so for that, i created a Custom RTMP stream with my scheduled stream's
key and tested it. From there, it was all downhill.

Starting the stream on the Magewell produced a green "starting light" on YouTube
telling that the incoming stream was in good health and if i'd just wait a
little, good things would happen. Which they didn't. So i tried again, with all
combinations of default and custom YouTube settings, and the stream key to the
default "Stream Now" session. Whatever i tried, i would after the initial waity
bit either get that my data rate was too low (while the streamer still claims
it's sending at ~3 Mbps) or that YouTube's receiving twice the data rate (so
_both_ the default and the custom stream to the same stream key?).

In the end, there was no YouTube stream, and i haven't checked yet if it
recorded either, but some of me is not holding my breath.

In recapture, the mixers, cameras and controllers worked well, but the streamer
didn't, and while i've tried to appear composed in this post, i'm actually
rather miffed (not to use a stronger expletive) that it wouldn't work.
Previously, i've used
[Blackmagic Ultra Studio Mini](https://www.blackmagicdesign.com/products/ultrastudio)
and [Elgato Cam Link](https://www.elgato.com/en/gaming/cam-link-4k) (although
the older non-4k version) capture devices and streamed using a Mac and
[Wirecast Play](https://www.telestream.net/wirecastplay/) (and dabbled a bit
with [OBS](https://obsproject.com/)), but with the old Mac i used for streaming,
i could never quite get the frame rate going. I've also tried the
[Blackmagic TV Studio](https://www.blackmagicdesign.com/products/atemtelevisionstudio)
which i really liked, except that it was really finicky about video formats and
created an unbearable lag to computers we connected to it. I really wished it
had worked better, because the Blackmagic cameras were really neat.

So in the end, this whole streaming thing isn't really a Solved Problem for me
yet. Please reach out on the social networks if you have a better solution :)

In an ideal world, i would want a video mixer with at least four SDI and four
HDMI video inputs and an equal number of outputs, four XLR audio in and out, two
stereo RCA audio in and out just for good measure, at least two video AUX buses
(or just two more independent "PGM" outputs) and two audio AUX buses, all of
which can be routed however i wish. This way, i could mix in the interpreted
sound into another feed and send it to the other stage, and whenever YouTube
starts supporting it, cast a multi-audio stream. The
[Roland V-1200HD](https://proav.roland.com/global/products/v-1200hd/) comes
close, but i doubt it is in our price range.

Ideally, the video mixer would also work as a proper audio mixer (Blackmagic
almost nails this, but their TV Studio mixers don't have proper XLR inputs).
Then we would be able to get a video call as input, route the video call audio
out to the hall and to the stream _and_ get local audio back to the person on
the other side of the video call without mixing in their own audio. Oh, and a
streamer that actually works.

Also, i would like the flexibility of software mixers like CamTwist and OBS
which think in layers, not keyers, and the stability of hardware gear. I would
like cameras that require one cable, not separate cables for signal and control.
I would like a hardware console that groks any video standard i throw at it,
then scales it to a common format. I would like the console to have built in
camera remote operation (PTZ, focus, iris). I would like the remote operation
console to warn me (with force feedback) if i move a camera which is on air, so
that my live picture won't wiggle. And i would like all that under 10k€
(including lights and cables -- the lights could also be controllable using the
video mixer console!).

Is that too much to ask?

[^splitter]: actually, i think we ended up connecting the splitter right after
    the laptop, so that half the signal went to the _other_ audience display and
    the other half to the Roland video mixer. Another bit-of-a-hack.
