---
type: "post"
title: "The Q currency"
date: 2019-01-12T13:53:30+02:00
description: "There's a new currency in town and it's called Q"
draft: false
keywords: ["currency", "alternative currency"]
topics: ["sysadmin"]
tags: ["money", "Q", "currency"]
---

![My proposed Q currency symbol](/img/q-currency-symbol.png#center)

There's a new currency in town and it's called Q. Or more exactly, it's an 
_initiative_ in the making, by some ex-PayPal guys, to create a an alternative
currency which they call Q. They're kickstarting it by taking registrations
from people. If you want in, you know me and i know you ...

**here's my invite link: [https://initiativeq.com/invite/PQWHnZBFX](https://initiativeq.com/invite/PQWHnZBFX).**

The Q initiative is handing out a bunch of free Q currency and the quicker you
react, the more Q you and i get for you signing up and me accepting you. Let me
know after you registered, because I need to verify you on my end.

To keep up with the spirit of Q, please only follow the invite link if we know
each other. The link will stop working once i'm out of invites. 

<!--more-->

And with that out of the way, here's my thoughts (two cents, if you wish,
though i don't know if there'll be a separate name for a hQ) on the currency
initiative. It's based on a rather lengthy comment i made on my Facebook stream.

So, some guys (gender-unspecific guys) decided that our current currency system
is outdated and needs to be rebooted. Nothing new with that. There are at least
2600 parallel or alternative currencies that have been created, according to 
[a post on the Christiania currency Løn](http://www.utopiskehorisonter.dk/engelsk/comcur.htm).

This whole Q thing is really interesting. Who in their right mind would issue "a
new money"? Or, why didn't i think of that and become a millionaire? Or ... who
in their right mind would accept this currency as payment? Ah, these questions!

Money, more specifically Fiat currency, really is one of humanity's great invention.
Money in itself has no value, or very little value. It's intentionally hard to
burn to keep you warm and it doesn't work well on the toilet. Metal money is
even more worthless in itself.

Money as it is only has value because those who use it chooses to believe that 
it has a value. Think about it. Bills and coins in your pocket are really just 
paper/plastic/cotton/mix and metal that you and the person you're having a 
transaction with choose to consider a token of value. Or worse, money is just 
numbers on your bank account(s), on your credit card, in your cryptocoin wallet.  
But it's a fantastic invention. We don't have to carry around goods to trade to 
get wares and services; instead we trade tokens. And we get tokens from working,
or stealing, inheriting, selling, trading and so on.

The Q initiative really wants to launch a parallel currency and while this 
certainly isn't the first time some have launched parallel, independent currencies, 
few have survived. On the other hand, there are also success stories. 
[Cities](https://en.wikipedia.org/wiki/W%C3%B6rgl#The_W%C3%B6rgl_Experiment) and
[countries](https://www.npr.org/sections/money/2010/10/04/130329523/how-fake-money-saved-brazil) 
have rebooted their economy using funny-money.

There are at least two twists here with regards to the Q. Acceptance as a currency, 
valuation of the Q and that people actually use the Q (when it's 
[launched in 2020/2021](https://initiativeq.com/timeline).[^terminology] Also,
i'd really like to see the 

People, both the initial ones who have Q and those who might take Q as payment, 
need to "believe" in the Q currency and accept it as payment.  Then there's how 
much one Q is worth. The Q Initiative aims for `1 Q` = `US$ 1`. But as we know, 
the value of Bitcoin has been just insane. At launch, you would pay five bitcoins 
for a pizza. These days five bitcoins would buy you a car. Or Q might take the 
route of a failing share, losing its value after the initial hype.

This is related to how people will be using it. If speculative owners hold on to 
their Qs in hope that their value will once go through the roof, the Q will not 
be exchanged and the whole currency will fade.

Finally, you might want to say that you can't just print your own money. Sure 
you can. See the earlier point, that a currency works iff people using it 
believes in it. There are  over 2600 complementary currencies created around the 
globe, many of them thriving.

Still after all these questions, i joined the Q Initiative. Q was free to join, 
and i welcome anyone who likes to try to disrupt a huge existing institution. 
While it might not succeed, it might at least shake the foundations.



[^terminology]: Terminology footnote: I am not an economist, so the use of words like _valuation_, _acceptance_ and _speculative_ may be totally random here. I hope the context of my writing makes it clear what i'm trying to say.

