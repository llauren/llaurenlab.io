+++
title = "Theses on a Thesis"
date = 2018-08-26T12:29:40+03:00
draft = false
tags = ['thesis', 'msc', 'git', 'markdown']
categories = ['academic']
+++

For some strange reason, i realized that there's a spot in my old MSc Thesis
which says it's available on line. So i thought i'd follow up on that promise
and make sure it is. And while i was at it, i wrote [a whole page](/thesis)
about how to (not) write your thesis. So there.

Now for some truly unfathomable reason, the link to the thesis page doesn't
show if you're reading the preview of this. You need to click into the post.
Or just click the [Thesis](/thesis) link at the nav bar up there on the right.
