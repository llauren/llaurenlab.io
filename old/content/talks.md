---
type: "post"
title: "Talks"
date: 2019-03-27T08:32:21Z
lastmod: 2019-03-27T08:32:21Z
description: "Sometimes i talk"
draft: false
menu: "main"
weight: 30
keywords: ["speaking", "talks"]
topics: ["talks"]
tags: ["speaking", "public speaking", "macaduk", "humane security"]
meta: false
---

I've actually never stood in front of a paying audience and talked before now.
The whole prospect is both terrifying and something i feel i _should_ do, given
all the effort all others have given through their talks. It's my time to start
giving back.

To remedy, i decided to give a talk at the [MacAD.uk](https://www.macad.uk)
conference on the 27th of March 2019) about, well, two things really.
First, and this is where my topic started, on security requirements for working
with "official" or "government level" security. The second topic is what i'd
like to coin _humane security_ and while it's the shorter bit of my talk, i
actually think it's the more relevant one and something i'd like to explore
further.

While you wait for a proper write-up about my talk, you can always check out my
[shorter cries for help](/tags/macaduk/).

The fact that i'm naming this section talk**S**, in plural, is for possible
future expansion that i'll maybe give another talk, if i don't die trying today.

The ever-evolving slides to my talk can be found [here](https://docs.google.com/presentation/d/1441kX4zXr4pw5Me0vydGPLwt7J6cGY339qZ5NZieIcY/edit?usp=sharing).

Wish me luck, cos' skill is something i don't have. Yet.
