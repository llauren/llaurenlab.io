==================================================================
https://keybase.io/llauren
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://robin.lauren.fi
  * I am llauren (https://keybase.io/llauren) on keybase.
  * I have a public key ASCO5otbynXDMLv16KUxLbZtwK7qg21i47-iGAkOTkio4Ao

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "01208ee68b5bca75c330bbf5e8a5312db66dc0aeea836d62e3bfa218090e4e48a8e00a",
      "host": "keybase.io",
      "kid": "01208ee68b5bca75c330bbf5e8a5312db66dc0aeea836d62e3bfa218090e4e48a8e00a",
      "uid": "3427ea1442b2ca61d4bb5fcc312fe019",
      "username": "llauren"
    },
    "merkle_root": {
      "ctime": 1520010091,
      "hash": "9f807d08a1236f24cfa34f853fddf3fb057134222ac5382414aabcb72eb1469683fe2216bafb37fbee9fd3c0a018bd9e29f724083a4a1d6514e9c9695749ebe8",
      "hash_meta": "58e210f176890b5ee4b7421fb34ec809d133331bceb6a1b4cdd99accf655351b",
      "seqno": 2176498
    },
    "service": {
      "hostname": "robin.lauren.fi",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "client": {
    "name": "keybase.io go client",
    "version": "1.0.44"
  },
  "ctime": 1520010111,
  "expire_in": 504576000,
  "prev": "9457506d16ee0de28f14b972780f90d623f4ccf60a9d9652824782659147fc25",
  "seqno": 38,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgjuaLW8p1wzC79eilMS22bcCu6oNtYuO/ohgJDk5IqOAKp3BheWxvYWTFA0d7ImJvZHkiOnsia2V5Ijp7ImVsZGVzdF9raWQiOiIwMTIwOGVlNjhiNWJjYTc1YzMzMGJiZjVlOGE1MzEyZGI2NmRjMGFlZWE4MzZkNjJlM2JmYTIxODA5MGU0ZTQ4YThlMDBhIiwiaG9zdCI6ImtleWJhc2UuaW8iLCJraWQiOiIwMTIwOGVlNjhiNWJjYTc1YzMzMGJiZjVlOGE1MzEyZGI2NmRjMGFlZWE4MzZkNjJlM2JmYTIxODA5MGU0ZTQ4YThlMDBhIiwidWlkIjoiMzQyN2VhMTQ0MmIyY2E2MWQ0YmI1ZmNjMzEyZmUwMTkiLCJ1c2VybmFtZSI6ImxsYXVyZW4ifSwibWVya2xlX3Jvb3QiOnsiY3RpbWUiOjE1MjAwMTAwOTEsImhhc2giOiI5ZjgwN2QwOGExMjM2ZjI0Y2ZhMzRmODUzZmRkZjNmYjA1NzEzNDIyMmFjNTM4MjQxNGFhYmNiNzJlYjE0Njk2ODNmZTIyMTZiYWZiMzdmYmVlOWZkM2MwYTAxOGJkOWUyOWY3MjQwODNhNGExZDY1MTRlOWM5Njk1NzQ5ZWJlOCIsImhhc2hfbWV0YSI6IjU4ZTIxMGYxNzY4OTBiNWVlNGI3NDIxZmIzNGVjODA5ZDEzMzMzMWJjZWI2YTFiNGNkZDk5YWNjZjY1NTM1MWIiLCJzZXFubyI6MjE3NjQ5OH0sInNlcnZpY2UiOnsiaG9zdG5hbWUiOiJyb2Jpbi5sYXVyZW4uZmkiLCJwcm90b2NvbCI6Imh0dHBzOiJ9LCJ0eXBlIjoid2ViX3NlcnZpY2VfYmluZGluZyIsInZlcnNpb24iOjF9LCJjbGllbnQiOnsibmFtZSI6ImtleWJhc2UuaW8gZ28gY2xpZW50IiwidmVyc2lvbiI6IjEuMC40NCJ9LCJjdGltZSI6MTUyMDAxMDExMSwiZXhwaXJlX2luIjo1MDQ1NzYwMDAsInByZXYiOiI5NDU3NTA2ZDE2ZWUwZGUyOGYxNGI5NzI3ODBmOTBkNjIzZjRjY2Y2MGE5ZDk2NTI4MjQ3ODI2NTkxNDdmYzI1Iiwic2Vxbm8iOjM4LCJ0YWciOiJzaWduYXR1cmUifaNzaWfEQPB5IsAFlRA6dhObhYYhCRncNz+fxT0AYyHP1NTDJ/ikvo5iq5uLNmS+SYxu5/vuiAvU5P1pp2Cbe+7xOrNIpgyoc2lnX3R5cGUgpGhhc2iCpHR5cGUIpXZhbHVlxCD75lB4KlIQvUVm3qwnq91WsDsn3mkyaHSojWqehVNarqN0YWfNAgKndmVyc2lvbgE=

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/llauren

==================================================================
